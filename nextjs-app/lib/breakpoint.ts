export enum Breakpoint {
  sm = 'sm',
  md = 'md',
  lg = 'lg',
  xlg = 'xlg'
}

const breakpoint = (breakpoint: Breakpoint) => {
  return {
    sm: '600px',
    md: '768px',
    lg: '992px',
    xlg: '1200px',
  }[breakpoint];
};

export default breakpoint;