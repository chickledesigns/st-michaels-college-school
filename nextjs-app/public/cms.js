// For all code running frontend after load

$(function() {
   function checkLoaded() {
      window.setTimeout(function() {
         if ($('main').length > 0) {
            $(document).trigger('nextjs-loaded');
            return;
         }
         checkLoaded();
      }, 300);
   }

   $(document).on('nextjs-loaded', function() {
      $('.carousel').not('.slick-slider').slick({
         dots: true,
         arrows: true,
         infinite: false,
         autoplay: false,
         fade: true,
         slide: '.carousel-item'
      });
   });

   checkLoaded();
});