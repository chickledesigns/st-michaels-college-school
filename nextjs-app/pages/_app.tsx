import '../styles/globals.scss'

import { ApolloProvider } from "@apollo/client";
import { useApollo } from "../lib/apolloClient";
import { useEffect } from "react";
import { useRouter } from 'next/router';

export default function App({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps.initialApolloState);
  const router = useRouter();

  useEffect(() => {
    const slick = document.createElement('script');
    const cms = document.createElement('script');
    slick.src = '/vendor/slick.min.js';
    cms.src = '/cms.js';
    document.body.appendChild(slick);
    document.body.appendChild(cms);

    router.events.on('routeChangeComplete', () => {
      const event = new Event('nextjs-loaded');
      document.dispatchEvent(event);
    })
  }, []);

  return (
    <ApolloProvider client={apolloClient}>
      <div>
        <Component {...pageProps} />
      </div>
    </ApolloProvider>
  );
}