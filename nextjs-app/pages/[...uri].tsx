import { useRouter } from "next/router";
import Page from "../features/Page";
import {GetStaticPaths, GetStaticProps, NextPage} from "next";
import {Maybe, Scalars} from "../gql.generated";
import { initializeApollo } from "../lib/apolloClient";
import GET_ALL_PAGE_URI from "../components/Page/queries/GET_ALL_PAGE_URI";
import GET_PAGE_BY_URI from "../components/Page/queries/GET_PAGE_BY_URI";
import extractPageProps from "../features/Page/hooks/extractPageProps";

type RoutedPageProps = {
  data: Maybe<Scalars['JSON']>;
}

const RoutedPage:NextPage<RoutedPageProps> = ({ data }) => {
  const props = extractPageProps(data);
  return <Page {...props} />;
}

export const getStaticPaths: GetStaticPaths = async(ctx) => {
  const client = await initializeApollo();
  const response = await client.query({
    query: GET_ALL_PAGE_URI
  });

  const uris = response.data.pages.nodes.map((page) => page.uri.split('/').filter(x => x)).filter(x => x.length > 0);

  const paths = uris.map(uri => ({
    params: { uri: uri },
  }));

  return { paths, fallback: false };
}

//Forces the router.query to load
export const getStaticProps: GetStaticProps = async ({ params }) => {
  const client = await initializeApollo();
  const response = await client.query({
    query: GET_PAGE_BY_URI,
    variables: { uri: `${params.uri}` }
  });

  return {
    props: {
      data: response.data
    }
  }
}

export default RoutedPage;
