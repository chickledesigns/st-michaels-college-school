import {GetStaticProps, NextPage} from "next";
import Home from "../features/Page/homepage";
import GET_HOMEPAGE from "../components/Page/queries/GET_HOMEPAGE";
import extractHomepageProps from "../features/Page/hooks/extractHomepageProps";
import { initializeApollo } from "../lib/apolloClient";

type HomepageProps = {
  data: JSON;
}

const Homepage:NextPage<HomepageProps> = ({ data }) => {
  const props = extractHomepageProps(data);
  return <Home {...props} />;
}


export const getStaticProps: GetStaticProps = async () => {
  const client = await initializeApollo();
  const response = await client.query({
    query: GET_HOMEPAGE,
    variables: {uri: 'home'}
  });

  return {
    props: {
      data: response.data
    },
  };
}

export default Homepage;