export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  JSON: any;
};

/** The root entry point into the Graph */
export type GQLRootQuery = {
  /** Entry point to get all settings for the site */
  allSettings?: Maybe<GQLSettings>;
  /** Connection between the RootQuery type and the category type */
  categories?: Maybe<GQLRootQueryToCategoryConnection>;
  /** A 0bject */
  category?: Maybe<GQLCategory>;
  /** Returns a Comment */
  comment?: Maybe<GQLComment>;
  /** Connection between the RootQuery type and the Comment type */
  comments?: Maybe<GQLRootQueryToCommentConnection>;
  /** A node used to manage content */
  contentNode?: Maybe<GQLContentNode>;
  /** Connection between the RootQuery type and the ContentNode type */
  contentNodes?: Maybe<GQLRootQueryToContentNodeConnection>;
  /** Fetch a Content Type node by unique Identifier */
  contentType?: Maybe<GQLContentType>;
  /** Connection between the RootQuery type and the ContentType type */
  contentTypes?: Maybe<GQLRootQueryToContentTypeConnection>;
  discussionSettings?: Maybe<GQLDiscussionSettings>;
  generalSettings?: Maybe<GQLGeneralSettings>;
  /** An object of the mediaItem Type.  */
  mediaItem?: Maybe<GQLMediaItem>;
  /**
   * A mediaItem object
   * @deprecated Deprecated in favor of using the single entry point for this type with ID and IDType fields. For example, instead of postBy( id: &quot;&quot; ), use post(id: &quot;&quot; idType: &quot;&quot;)
   */
  mediaItemBy?: Maybe<GQLMediaItem>;
  /** Connection between the RootQuery type and the mediaItem type */
  mediaItems?: Maybe<GQLRootQueryToMediaItemConnection>;
  /** A WordPress navigation menu */
  menu?: Maybe<GQLMenu>;
  /** A WordPress navigation menu item */
  menuItem?: Maybe<GQLMenuItem>;
  /** Connection between the RootQuery type and the MenuItem type */
  menuItems?: Maybe<GQLRootQueryToMenuItemConnection>;
  /** Connection between the RootQuery type and the Menu type */
  menus?: Maybe<GQLRootQueryToMenuConnection>;
  /** Fetches an object given its ID */
  node?: Maybe<GQLNode>;
  nodeByUri?: Maybe<GQLUniformResourceIdentifiable>;
  /** An object of the page Type.  */
  page?: Maybe<GQLPage>;
  /**
   * A page object
   * @deprecated Deprecated in favor of using the single entry point for this type with ID and IDType fields. For example, instead of postBy( id: &quot;&quot; ), use post(id: &quot;&quot; idType: &quot;&quot;)
   */
  pageBy?: Maybe<GQLPage>;
  /** Connection between the RootQuery type and the page type */
  pages?: Maybe<GQLRootQueryToPageConnection>;
  /** A WordPress plugin */
  plugin?: Maybe<GQLPlugin>;
  /** Connection between the RootQuery type and the Plugin type */
  plugins?: Maybe<GQLRootQueryToPluginConnection>;
  /** An object of the post Type.  */
  post?: Maybe<GQLPost>;
  /**
   * A post object
   * @deprecated Deprecated in favor of using the single entry point for this type with ID and IDType fields. For example, instead of postBy( id: &quot;&quot; ), use post(id: &quot;&quot; idType: &quot;&quot;)
   */
  postBy?: Maybe<GQLPost>;
  /** A 0bject */
  postFormat?: Maybe<GQLPostFormat>;
  /** Connection between the RootQuery type and the postFormat type */
  postFormats?: Maybe<GQLRootQueryToPostFormatConnection>;
  /** Connection between the RootQuery type and the post type */
  posts?: Maybe<GQLRootQueryToPostConnection>;
  readingSettings?: Maybe<GQLReadingSettings>;
  /** Connection between the RootQuery type and the EnqueuedScript type */
  registeredScripts?: Maybe<GQLRootQueryToEnqueuedScriptConnection>;
  /** Connection between the RootQuery type and the EnqueuedStylesheet type */
  registeredStylesheets?: Maybe<GQLRootQueryToEnqueuedStylesheetConnection>;
  /** Connection between the RootQuery type and the ContentRevisionUnion type */
  revisions?: Maybe<GQLRootQueryToContentRevisionUnionConnection>;
  /** An object of the socialMediaIcon Type.  */
  socialMediaIcon?: Maybe<GQLSocialMediaIcon>;
  /**
   * A socialMediaIcon object
   * @deprecated Deprecated in favor of using the single entry point for this type with ID and IDType fields. For example, instead of postBy( id: &quot;&quot; ), use post(id: &quot;&quot; idType: &quot;&quot;)
   */
  socialMediaIconBy?: Maybe<GQLSocialMediaIcon>;
  /** Connection between the RootQuery type and the socialMediaIcon type */
  socialMediaIcons?: Maybe<GQLRootQueryToSocialMediaIconConnection>;
  /** A 0bject */
  tag?: Maybe<GQLTag>;
  /** Connection between the RootQuery type and the tag type */
  tags?: Maybe<GQLRootQueryToTagConnection>;
  /** Connection between the RootQuery type and the Taxonomy type */
  taxonomies?: Maybe<GQLRootQueryToTaxonomyConnection>;
  /** Fetch a Taxonomy node by unique Identifier */
  taxonomy?: Maybe<GQLTaxonomy>;
  /** A node in a taxonomy used to group and relate content nodes */
  termNode?: Maybe<GQLTermNode>;
  /** Connection between the RootQuery type and the TermNode type */
  terms?: Maybe<GQLRootQueryToTermNodeConnection>;
  /** A Theme object */
  theme?: Maybe<GQLTheme>;
  /** Connection between the RootQuery type and the Theme type */
  themes?: Maybe<GQLRootQueryToThemeConnection>;
  /** Returns a user */
  user?: Maybe<GQLUser>;
  /** Returns a user role */
  userRole?: Maybe<GQLUserRole>;
  /** Connection between the RootQuery type and the UserRole type */
  userRoles?: Maybe<GQLRootQueryToUserRoleConnection>;
  /** Connection between the RootQuery type and the User type */
  users?: Maybe<GQLRootQueryToUserConnection>;
  /** Returns the current user */
  viewer?: Maybe<GQLUser>;
  writingSettings?: Maybe<GQLWritingSettings>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycategoriesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToCategoryConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycategoryArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLCategoryIdType>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycommentArgs = {
  id: Scalars['ID'];
};


/** The root entry point into the Graph */
export type GQLRootQuerycommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToCommentConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycontentNodeArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLContentNodeIdTypeEnum>;
  contentType?: Maybe<GQLContentTypeEnum>;
  asPreview?: Maybe<Scalars['Boolean']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycontentNodesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToContentNodeConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycontentTypeArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLContentTypeIdTypeEnum>;
};


/** The root entry point into the Graph */
export type GQLRootQuerycontentTypesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymediaItemArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLMediaItemIdType>;
  asPreview?: Maybe<Scalars['Boolean']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymediaItemByArgs = {
  id?: Maybe<Scalars['ID']>;
  mediaItemId?: Maybe<Scalars['Int']>;
  uri?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymediaItemsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToMediaItemConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymenuArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLMenuNodeIdTypeEnum>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymenuItemArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLMenuItemNodeIdTypeEnum>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymenuItemsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToMenuItemConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerymenusArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToMenuConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerynodeArgs = {
  id?: Maybe<Scalars['ID']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerynodeByUriArgs = {
  uri: Scalars['String'];
};


/** The root entry point into the Graph */
export type GQLRootQuerypageArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLPageIdType>;
  asPreview?: Maybe<Scalars['Boolean']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypageByArgs = {
  id?: Maybe<Scalars['ID']>;
  pageId?: Maybe<Scalars['Int']>;
  uri?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypagesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToPageConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypluginArgs = {
  id: Scalars['ID'];
};


/** The root entry point into the Graph */
export type GQLRootQuerypluginsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypostArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLPostIdType>;
  asPreview?: Maybe<Scalars['Boolean']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypostByArgs = {
  id?: Maybe<Scalars['ID']>;
  postId?: Maybe<Scalars['Int']>;
  uri?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypostFormatArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLPostFormatIdType>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypostFormatsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToPostFormatConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerypostsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToPostConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQueryregisteredScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQueryregisteredStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQueryrevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToContentRevisionUnionConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerysocialMediaIconArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLSocialMediaIconIdType>;
  asPreview?: Maybe<Scalars['Boolean']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerysocialMediaIconByArgs = {
  id?: Maybe<Scalars['ID']>;
  socialMediaIconId?: Maybe<Scalars['Int']>;
  uri?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerysocialMediaIconsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToSocialMediaIconConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerytagArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLTagIdType>;
};


/** The root entry point into the Graph */
export type GQLRootQuerytagsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToTagConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerytaxonomiesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQuerytaxonomyArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLTaxonomyIdTypeEnum>;
};


/** The root entry point into the Graph */
export type GQLRootQuerytermNodeArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLTermNodeIdTypeEnum>;
  taxonomy?: Maybe<GQLTaxonomyEnum>;
};


/** The root entry point into the Graph */
export type GQLRootQuerytermsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToTermNodeConnectionWhereArgs>;
};


/** The root entry point into the Graph */
export type GQLRootQuerythemeArgs = {
  id: Scalars['ID'];
};


/** The root entry point into the Graph */
export type GQLRootQuerythemesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQueryuserArgs = {
  id: Scalars['ID'];
  idType?: Maybe<GQLUserNodeIdTypeEnum>;
};


/** The root entry point into the Graph */
export type GQLRootQueryuserRoleArgs = {
  id: Scalars['ID'];
};


/** The root entry point into the Graph */
export type GQLRootQueryuserRolesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The root entry point into the Graph */
export type GQLRootQueryusersArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLRootQueryToUserConnectionWhereArgs>;
};

/** All of the registered settings */
export type GQLSettings = {
  /** Allow people to submit comments on new posts. */
  discussionSettingsDefaultCommentStatus?: Maybe<Scalars['String']>;
  /** Allow link notifications from other blogs (pingbacks and trackbacks) on new articles. */
  discussionSettingsDefaultPingStatus?: Maybe<Scalars['String']>;
  /** A date format for all date strings. */
  generalSettingsDateFormat?: Maybe<Scalars['String']>;
  /** Site tagline. */
  generalSettingsDescription?: Maybe<Scalars['String']>;
  /** This address is used for admin purposes, like new user notification. */
  generalSettingsEmail?: Maybe<Scalars['String']>;
  /** WordPress locale code. */
  generalSettingsLanguage?: Maybe<Scalars['String']>;
  /** A day number of the week that the week should start on. */
  generalSettingsStartOfWeek?: Maybe<Scalars['Int']>;
  /** A time format for all time strings. */
  generalSettingsTimeFormat?: Maybe<Scalars['String']>;
  /** A city in the same timezone as you. */
  generalSettingsTimezone?: Maybe<Scalars['String']>;
  /** Site title. */
  generalSettingsTitle?: Maybe<Scalars['String']>;
  /** Site URL. */
  generalSettingsUrl?: Maybe<Scalars['String']>;
  /** Blog pages show at most. */
  readingSettingsPostsPerPage?: Maybe<Scalars['Int']>;
  /** Default post category. */
  writingSettingsDefaultCategory?: Maybe<Scalars['Int']>;
  /** Default post format. */
  writingSettingsDefaultPostFormat?: Maybe<Scalars['String']>;
  /** Convert emoticons like :-) and :-P to graphics on display. */
  writingSettingsUseSmilies?: Maybe<Scalars['Boolean']>;
};

/** Arguments for filtering the RootQueryToCategoryConnection connection */
export type GQLRootQueryToCategoryConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Options for ordering the connection by */
export enum GQLTermObjectsConnectionOrderbyEnum {
  COUNT = 'COUNT',
  DESCRIPTION = 'DESCRIPTION',
  NAME = 'NAME',
  SLUG = 'SLUG',
  TERM_GROUP = 'TERM_GROUP',
  TERM_ID = 'TERM_ID',
  TERM_ORDER = 'TERM_ORDER'
}

/** Connection between the RootQuery type and the category type */
export type GQLRootQueryToCategoryConnection = {
  /** Edges for the RootQueryToCategoryConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToCategoryConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLCategory>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToCategoryConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLCategory>;
};

/** The category type */
export type GQLCategory = GQLNode & GQLTermNode & GQLDatabaseIdentifier & GQLUniformResourceIdentifiable & GQLHierarchicalTermNode & GQLMenuItemLinkable & {
  /** The ancestors of the node. Default ordered as lowest (closest to the child) to highest (closest to the root). */
  ancestors?: Maybe<GQLCategoryToAncestorsCategoryConnection>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of databaseId
   */
  categoryId?: Maybe<Scalars['Int']>;
  /** Connection between the category type and the category type */
  children?: Maybe<GQLCategoryToCategoryConnection>;
  /** Connection between the category type and the ContentNode type */
  contentNodes?: Maybe<GQLCategoryToContentNodeConnection>;
  /** The number of objects connected to the object */
  count?: Maybe<Scalars['Int']>;
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** The description of the object */
  description?: Maybe<Scalars['String']>;
  /** Connection between the TermNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLTermNodeToEnqueuedScriptConnection>;
  /** Connection between the TermNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLTermNodeToEnqueuedStylesheetConnection>;
  /** The globally unique ID for the object */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The link to the term */
  link?: Maybe<Scalars['String']>;
  /** The human friendly name of the object. */
  name?: Maybe<Scalars['String']>;
  /** Connection between the category type and the category type */
  parent?: Maybe<GQLCategoryToParentCategoryConnectionEdge>;
  /** Database id of the parent node */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent node. */
  parentId?: Maybe<Scalars['ID']>;
  /** Connection between the category type and the post type */
  posts?: Maybe<GQLCategoryToPostConnection>;
  /** An alphanumeric identifier for the object unique to its type. */
  slug?: Maybe<Scalars['String']>;
  /** Connection between the category type and the Taxonomy type */
  taxonomy?: Maybe<GQLCategoryToTaxonomyConnectionEdge>;
  /** The ID of the term group that this term object belongs to */
  termGroupId?: Maybe<Scalars['Int']>;
  /** The taxonomy ID that the object is associated with */
  termTaxonomyId?: Maybe<Scalars['Int']>;
  /** The unique resource identifier path */
  uri: Scalars['String'];
};


/** The category type */
export type GQLCategoryancestorsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The category type */
export type GQLCategorychildrenArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLCategoryToCategoryConnectionWhereArgs>;
};


/** The category type */
export type GQLCategorycontentNodesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLCategoryToContentNodeConnectionWhereArgs>;
};


/** The category type */
export type GQLCategoryenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The category type */
export type GQLCategoryenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The category type */
export type GQLCategorypostsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLCategoryToPostConnectionWhereArgs>;
};

/** An object with an ID */
export type GQLNode = {
  /** The globally unique ID for the object */
  id: Scalars['ID'];
};

/** Terms are nodes within a Taxonomy, used to group and relate other nodes. */
export type GQLTermNode = {
  /** The number of objects connected to the object */
  count?: Maybe<Scalars['Int']>;
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** The description of the object */
  description?: Maybe<Scalars['String']>;
  /** Connection between the TermNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLTermNodeToEnqueuedScriptConnection>;
  /** Connection between the TermNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLTermNodeToEnqueuedStylesheetConnection>;
  /** Unique identifier for the term */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The link to the term */
  link?: Maybe<Scalars['String']>;
  /** The human friendly name of the object. */
  name?: Maybe<Scalars['String']>;
  /** An alphanumeric identifier for the object unique to its type. */
  slug?: Maybe<Scalars['String']>;
  /** The ID of the term group that this term object belongs to */
  termGroupId?: Maybe<Scalars['Int']>;
  /** The taxonomy ID that the object is associated with */
  termTaxonomyId?: Maybe<Scalars['Int']>;
  /** The unique resource identifier path */
  uri: Scalars['String'];
};


/** Terms are nodes within a Taxonomy, used to group and relate other nodes. */
export type GQLTermNodeenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** Terms are nodes within a Taxonomy, used to group and relate other nodes. */
export type GQLTermNodeenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};

/** Connection between the TermNode type and the EnqueuedScript type */
export type GQLTermNodeToEnqueuedScriptConnection = {
  /** Edges for the TermNodeToEnqueuedScriptConnection connection */
  edges?: Maybe<Array<Maybe<GQLTermNodeToEnqueuedScriptConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLTermNodeToEnqueuedScriptConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedScript>;
};

/** Script enqueued by the CMS */
export type GQLEnqueuedScript = GQLNode & GQLEnqueuedAsset & {
  /** @todo */
  args?: Maybe<Scalars['Boolean']>;
  /** Dependencies needed to use this asset */
  dependencies?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Extra information needed for the script */
  extra?: Maybe<Scalars['String']>;
  /** The handle of the enqueued asset */
  handle?: Maybe<Scalars['String']>;
  /** The globally unique ID for the object */
  id: Scalars['ID'];
  /** The source of the asset */
  src?: Maybe<Scalars['String']>;
  /** The version of the enqueued asset */
  version?: Maybe<Scalars['String']>;
};

/** Asset enqueued by the CMS */
export type GQLEnqueuedAsset = {
  /** @todo */
  args?: Maybe<Scalars['Boolean']>;
  /** Dependencies needed to use this asset */
  dependencies?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Extra information needed for the script */
  extra?: Maybe<Scalars['String']>;
  /** The handle of the enqueued asset */
  handle?: Maybe<Scalars['String']>;
  /** The ID of the enqueued asset */
  id: Scalars['ID'];
  /** The source of the asset */
  src?: Maybe<Scalars['String']>;
  /** The version of the enqueued asset */
  version?: Maybe<Scalars['String']>;
};

/** Information about pagination in a connection. */
export type GQLWPPageInfo = {
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']>;
};

/** Connection between the TermNode type and the EnqueuedStylesheet type */
export type GQLTermNodeToEnqueuedStylesheetConnection = {
  /** Edges for the TermNodeToEnqueuedStylesheetConnection connection */
  edges?: Maybe<Array<Maybe<GQLTermNodeToEnqueuedStylesheetConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedStylesheet>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLTermNodeToEnqueuedStylesheetConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedStylesheet>;
};

/** Stylesheet enqueued by the CMS */
export type GQLEnqueuedStylesheet = GQLNode & GQLEnqueuedAsset & {
  /** @todo */
  args?: Maybe<Scalars['Boolean']>;
  /** Dependencies needed to use this asset */
  dependencies?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Extra information needed for the script */
  extra?: Maybe<Scalars['String']>;
  /** The handle of the enqueued asset */
  handle?: Maybe<Scalars['String']>;
  /** The globally unique ID for the object */
  id: Scalars['ID'];
  /** The source of the asset */
  src?: Maybe<Scalars['String']>;
  /** The version of the enqueued asset */
  version?: Maybe<Scalars['String']>;
};

/** Object that can be identified with a Database ID */
export type GQLDatabaseIdentifier = {
  /** The unique identifier stored in the database */
  databaseId: Scalars['Int'];
};

/** Any node that has a URI */
export type GQLUniformResourceIdentifiable = {
  /** The unique resource identifier path */
  id: Scalars['ID'];
  /** The unique resource identifier path */
  uri?: Maybe<Scalars['String']>;
};

/** Term node with hierarchical (parent/child) relationships */
export type GQLHierarchicalTermNode = {
  /** Database id of the parent node */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent node. */
  parentId?: Maybe<Scalars['ID']>;
};

/** Nodes that can be linked to as Menu Items */
export type GQLMenuItemLinkable = {
  /** The unique resource identifier path */
  databaseId: Scalars['Int'];
  /** The unique resource identifier path */
  id: Scalars['ID'];
  /** The unique resource identifier path */
  uri: Scalars['String'];
};

/** Connection between the category type and the category type */
export type GQLCategoryToAncestorsCategoryConnection = {
  /** Edges for the CategoryToAncestorsCategoryConnection connection */
  edges?: Maybe<Array<Maybe<GQLCategoryToAncestorsCategoryConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLCategory>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLCategoryToAncestorsCategoryConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLCategory>;
};

/** Arguments for filtering the CategoryToCategoryConnection connection */
export type GQLCategoryToCategoryConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the category type and the category type */
export type GQLCategoryToCategoryConnection = {
  /** Edges for the CategoryToCategoryConnection connection */
  edges?: Maybe<Array<Maybe<GQLCategoryToCategoryConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLCategory>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLCategoryToCategoryConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLCategory>;
};

/** Arguments for filtering the CategoryToContentNodeConnection connection */
export type GQLCategoryToContentNodeConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Filter the connection based on input */
export type GQLDateQueryInput = {
  /** Nodes should be returned after this date */
  after?: Maybe<GQLDateInput>;
  /** Nodes should be returned before this date */
  before?: Maybe<GQLDateInput>;
  /** Column to query against */
  column?: Maybe<GQLPostObjectsConnectionDateColumnEnum>;
  /** For after/before, whether exact value should be matched or not */
  compare?: Maybe<Scalars['String']>;
  /** Day of the month (from 1 to 31) */
  day?: Maybe<Scalars['Int']>;
  /** Hour (from 0 to 23) */
  hour?: Maybe<Scalars['Int']>;
  /** For after/before, whether exact value should be matched or not */
  inclusive?: Maybe<Scalars['Boolean']>;
  /** Minute (from 0 to 59) */
  minute?: Maybe<Scalars['Int']>;
  /** Month number (from 1 to 12) */
  month?: Maybe<Scalars['Int']>;
  /** OR or AND, how the sub-arrays should be compared */
  relation?: Maybe<GQLRelationEnum>;
  /** Second (0 to 59) */
  second?: Maybe<Scalars['Int']>;
  /** Week of the year (from 0 to 53) */
  week?: Maybe<Scalars['Int']>;
  /** 4 digit year (e.g. 2017) */
  year?: Maybe<Scalars['Int']>;
};

/** Date values */
export type GQLDateInput = {
  /** Day of the month (from 1 to 31) */
  day?: Maybe<Scalars['Int']>;
  /** Month number (from 1 to 12) */
  month?: Maybe<Scalars['Int']>;
  /** 4 digit year (e.g. 2017) */
  year?: Maybe<Scalars['Int']>;
};

/** The column to use when filtering by date */
export enum GQLPostObjectsConnectionDateColumnEnum {
  DATE = 'DATE',
  MODIFIED = 'MODIFIED'
}

/** The logical relation between each item in the array when there are more than one. */
export enum GQLRelationEnum {
  AND = 'AND',
  OR = 'OR'
}

/** The MimeType of the object */
export enum GQLMimeTypeEnum {
  APPLICATION_JAVA = 'APPLICATION_JAVA',
  APPLICATION_MSWORD = 'APPLICATION_MSWORD',
  APPLICATION_OCTET_STREAM = 'APPLICATION_OCTET_STREAM',
  APPLICATION_ONENOTE = 'APPLICATION_ONENOTE',
  APPLICATION_OXPS = 'APPLICATION_OXPS',
  APPLICATION_PDF = 'APPLICATION_PDF',
  APPLICATION_RAR = 'APPLICATION_RAR',
  APPLICATION_RTF = 'APPLICATION_RTF',
  APPLICATION_TTAF_XML = 'APPLICATION_TTAF_XML',
  APPLICATION_VND_APPLE_KEYNOTE = 'APPLICATION_VND_APPLE_KEYNOTE',
  APPLICATION_VND_APPLE_NUMBERS = 'APPLICATION_VND_APPLE_NUMBERS',
  APPLICATION_VND_APPLE_PAGES = 'APPLICATION_VND_APPLE_PAGES',
  APPLICATION_VND_MS_ACCESS = 'APPLICATION_VND_MS_ACCESS',
  APPLICATION_VND_MS_EXCEL = 'APPLICATION_VND_MS_EXCEL',
  APPLICATION_VND_MS_EXCEL_ADDIN_MACROENABLED_12 = 'APPLICATION_VND_MS_EXCEL_ADDIN_MACROENABLED_12',
  APPLICATION_VND_MS_EXCEL_SHEET_BINARY_MACROENABLED_12 = 'APPLICATION_VND_MS_EXCEL_SHEET_BINARY_MACROENABLED_12',
  APPLICATION_VND_MS_EXCEL_SHEET_MACROENABLED_12 = 'APPLICATION_VND_MS_EXCEL_SHEET_MACROENABLED_12',
  APPLICATION_VND_MS_EXCEL_TEMPLATE_MACROENABLED_12 = 'APPLICATION_VND_MS_EXCEL_TEMPLATE_MACROENABLED_12',
  APPLICATION_VND_MS_POWERPOINT = 'APPLICATION_VND_MS_POWERPOINT',
  APPLICATION_VND_MS_POWERPOINT_ADDIN_MACROENABLED_12 = 'APPLICATION_VND_MS_POWERPOINT_ADDIN_MACROENABLED_12',
  APPLICATION_VND_MS_POWERPOINT_PRESENTATION_MACROENABLED_12 = 'APPLICATION_VND_MS_POWERPOINT_PRESENTATION_MACROENABLED_12',
  APPLICATION_VND_MS_POWERPOINT_SLIDESHOW_MACROENABLED_12 = 'APPLICATION_VND_MS_POWERPOINT_SLIDESHOW_MACROENABLED_12',
  APPLICATION_VND_MS_POWERPOINT_SLIDE_MACROENABLED_12 = 'APPLICATION_VND_MS_POWERPOINT_SLIDE_MACROENABLED_12',
  APPLICATION_VND_MS_POWERPOINT_TEMPLATE_MACROENABLED_12 = 'APPLICATION_VND_MS_POWERPOINT_TEMPLATE_MACROENABLED_12',
  APPLICATION_VND_MS_PROJECT = 'APPLICATION_VND_MS_PROJECT',
  APPLICATION_VND_MS_WORD_DOCUMENT_MACROENABLED_12 = 'APPLICATION_VND_MS_WORD_DOCUMENT_MACROENABLED_12',
  APPLICATION_VND_MS_WORD_TEMPLATE_MACROENABLED_12 = 'APPLICATION_VND_MS_WORD_TEMPLATE_MACROENABLED_12',
  APPLICATION_VND_MS_WRITE = 'APPLICATION_VND_MS_WRITE',
  APPLICATION_VND_MS_XPSDOCUMENT = 'APPLICATION_VND_MS_XPSDOCUMENT',
  APPLICATION_VND_OASIS_OPENDOCUMENT_CHART = 'APPLICATION_VND_OASIS_OPENDOCUMENT_CHART',
  APPLICATION_VND_OASIS_OPENDOCUMENT_DATABASE = 'APPLICATION_VND_OASIS_OPENDOCUMENT_DATABASE',
  APPLICATION_VND_OASIS_OPENDOCUMENT_FORMULA = 'APPLICATION_VND_OASIS_OPENDOCUMENT_FORMULA',
  APPLICATION_VND_OASIS_OPENDOCUMENT_GRAPHICS = 'APPLICATION_VND_OASIS_OPENDOCUMENT_GRAPHICS',
  APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION = 'APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION',
  APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET = 'APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET',
  APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT = 'APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDE = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDE',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDESHOW = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDESHOW',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TEMPLATE = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TEMPLATE',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TEMPLATE = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TEMPLATE',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT',
  APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_TEMPLATE = 'APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_TEMPLATE',
  APPLICATION_WORDPERFECT = 'APPLICATION_WORDPERFECT',
  APPLICATION_X_7Z_COMPRESSED = 'APPLICATION_X_7Z_COMPRESSED',
  APPLICATION_X_GZIP = 'APPLICATION_X_GZIP',
  APPLICATION_X_TAR = 'APPLICATION_X_TAR',
  APPLICATION_ZIP = 'APPLICATION_ZIP',
  AUDIO_AAC = 'AUDIO_AAC',
  AUDIO_FLAC = 'AUDIO_FLAC',
  AUDIO_MIDI = 'AUDIO_MIDI',
  AUDIO_MPEG = 'AUDIO_MPEG',
  AUDIO_OGG = 'AUDIO_OGG',
  AUDIO_WAV = 'AUDIO_WAV',
  AUDIO_X_MATROSKA = 'AUDIO_X_MATROSKA',
  AUDIO_X_MS_WAX = 'AUDIO_X_MS_WAX',
  AUDIO_X_MS_WMA = 'AUDIO_X_MS_WMA',
  AUDIO_X_REALAUDIO = 'AUDIO_X_REALAUDIO',
  IMAGE_BMP = 'IMAGE_BMP',
  IMAGE_GIF = 'IMAGE_GIF',
  IMAGE_HEIC = 'IMAGE_HEIC',
  IMAGE_JPEG = 'IMAGE_JPEG',
  IMAGE_PNG = 'IMAGE_PNG',
  IMAGE_TIFF = 'IMAGE_TIFF',
  IMAGE_X_ICON = 'IMAGE_X_ICON',
  TEXT_CALENDAR = 'TEXT_CALENDAR',
  TEXT_CSS = 'TEXT_CSS',
  TEXT_CSV = 'TEXT_CSV',
  TEXT_PLAIN = 'TEXT_PLAIN',
  TEXT_RICHTEXT = 'TEXT_RICHTEXT',
  TEXT_TAB_SEPARATED_VALUES = 'TEXT_TAB_SEPARATED_VALUES',
  TEXT_VTT = 'TEXT_VTT',
  VIDEO_3GPP = 'VIDEO_3GPP',
  VIDEO_3GPP2 = 'VIDEO_3GPP2',
  VIDEO_AVI = 'VIDEO_AVI',
  VIDEO_DIVX = 'VIDEO_DIVX',
  VIDEO_MP4 = 'VIDEO_MP4',
  VIDEO_MPEG = 'VIDEO_MPEG',
  VIDEO_OGG = 'VIDEO_OGG',
  VIDEO_QUICKTIME = 'VIDEO_QUICKTIME',
  VIDEO_WEBM = 'VIDEO_WEBM',
  VIDEO_X_FLV = 'VIDEO_X_FLV',
  VIDEO_X_MATROSKA = 'VIDEO_X_MATROSKA',
  VIDEO_X_MS_ASF = 'VIDEO_X_MS_ASF',
  VIDEO_X_MS_WM = 'VIDEO_X_MS_WM',
  VIDEO_X_MS_WMV = 'VIDEO_X_MS_WMV',
  VIDEO_X_MS_WMX = 'VIDEO_X_MS_WMX'
}

/** Options for ordering the connection */
export type GQLPostObjectsConnectionOrderbyInput = {
  /** The field to order the connection by */
  field: GQLPostObjectsConnectionOrderbyEnum;
  /** Possible directions in which to order a list of items */
  order: GQLOrderEnum;
};

/** Field to order the connection by */
export enum GQLPostObjectsConnectionOrderbyEnum {
  /** Order by author */
  AUTHOR = 'AUTHOR',
  /** Order by the number of comments it has acquired */
  COMMENT_COUNT = 'COMMENT_COUNT',
  /** Order by publish date */
  DATE = 'DATE',
  /** Preserve the ID order given in the IN array */
  IN = 'IN',
  /** Order by the menu order value */
  MENU_ORDER = 'MENU_ORDER',
  /** Order by last modified date */
  MODIFIED = 'MODIFIED',
  /** Preserve slug order given in the NAME_IN array */
  NAME_IN = 'NAME_IN',
  /** Order by parent ID */
  PARENT = 'PARENT',
  /** Order by slug */
  SLUG = 'SLUG',
  /** Order by title */
  TITLE = 'TITLE'
}

/** The cardinality of the connection order */
export enum GQLOrderEnum {
  ASC = 'ASC',
  DESC = 'DESC'
}

/** The status of the object. */
export enum GQLPostStatusEnum {
  /** Objects with the acf-disabled status */
  ACF_DISABLED = 'ACF_DISABLED',
  /** Objects with the auto-draft status */
  AUTO_DRAFT = 'AUTO_DRAFT',
  /** Objects with the draft status */
  DRAFT = 'DRAFT',
  /** Objects with the future status */
  FUTURE = 'FUTURE',
  /** Objects with the inherit status */
  INHERIT = 'INHERIT',
  /** Objects with the pending status */
  PENDING = 'PENDING',
  /** Objects with the private status */
  PRIVATE = 'PRIVATE',
  /** Objects with the publish status */
  PUBLISH = 'PUBLISH',
  /** Objects with the request-completed status */
  REQUEST_COMPLETED = 'REQUEST_COMPLETED',
  /** Objects with the request-confirmed status */
  REQUEST_CONFIRMED = 'REQUEST_CONFIRMED',
  /** Objects with the request-failed status */
  REQUEST_FAILED = 'REQUEST_FAILED',
  /** Objects with the request-pending status */
  REQUEST_PENDING = 'REQUEST_PENDING',
  /** Objects with the trash status */
  TRASH = 'TRASH'
}

/** Connection between the category type and the ContentNode type */
export type GQLCategoryToContentNodeConnection = {
  /** Edges for the CategoryToContentNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLCategoryToContentNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLCategoryToContentNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** Nodes used to manage content */
export type GQLContentNode = {
  /** Connection between the ContentNode type and the ContentType type */
  contentType?: Maybe<GQLContentNodeToContentTypeConnectionEdge>;
  /** The ID of the node in the database. */
  databaseId: Scalars['Int'];
  /** Post publishing date. */
  date?: Maybe<Scalars['String']>;
  /** The publishing date set in GMT. */
  dateGmt?: Maybe<Scalars['String']>;
  /** The desired slug of the post */
  desiredSlug?: Maybe<Scalars['String']>;
  /** If a user has edited the node within the past 15 seconds, this will return the user that last edited. Null if the edit lock doesn't exist or is greater than 15 seconds */
  editingLockedBy?: Maybe<GQLContentNodeToEditLockConnectionEdge>;
  /** The RSS enclosure for the object */
  enclosure?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLContentNodeToEnqueuedScriptConnection>;
  /** Connection between the ContentNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLContentNodeToEnqueuedStylesheetConnection>;
  /** The global unique identifier for this post. This currently matches the value stored in WP_Post->guid and the guid column in the "post_objects" database table. */
  guid?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the node. */
  id: Scalars['ID'];
  /** Whether the object is a node in the preview state */
  isPreview?: Maybe<Scalars['Boolean']>;
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The user that most recently edited the node */
  lastEditedBy?: Maybe<GQLContentNodeToEditLastConnectionEdge>;
  /** The permalink of the post */
  link?: Maybe<Scalars['String']>;
  /** The local modified time for a post. If a post was recently updated the modified field will change to match the corresponding time. */
  modified?: Maybe<Scalars['String']>;
  /** The GMT modified time for a post. If a post was recently updated the modified field will change to match the corresponding time in GMT. */
  modifiedGmt?: Maybe<Scalars['String']>;
  /** The database id of the preview node */
  previewRevisionDatabaseId?: Maybe<Scalars['Int']>;
  /** Whether the object is a node in the preview state */
  previewRevisionId?: Maybe<Scalars['ID']>;
  /** The uri slug for the post. This is equivalent to the WP_Post->post_name field and the post_name column in the database for the "post_objects" table. */
  slug?: Maybe<Scalars['String']>;
  /** The current status of the object */
  status?: Maybe<Scalars['String']>;
  /** The template assigned to a node of content */
  template?: Maybe<GQLContentTemplate>;
  /** URI path for the resource */
  uri: Scalars['String'];
};


/** Nodes used to manage content */
export type GQLContentNodeenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** Nodes used to manage content */
export type GQLContentNodeenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};

/** Connection between the ContentNode type and the ContentType type */
export type GQLContentNodeToContentTypeConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLContentType>;
};

/** An Post Type object */
export type GQLContentType = GQLNode & GQLUniformResourceIdentifiable & {
  /** Whether this content type should can be exported. */
  canExport?: Maybe<Scalars['Boolean']>;
  /** Connection between the ContentType type and the Taxonomy type */
  connectedTaxonomies?: Maybe<GQLContentTypeToTaxonomyConnection>;
  /** Connection between the ContentType type and the ContentNode type */
  contentNodes?: Maybe<GQLContentTypeToContentNodeConnection>;
  /** Whether content of this type should be deleted when the author of it is deleted from the system. */
  deleteWithUser?: Maybe<Scalars['Boolean']>;
  /** Description of the content type. */
  description?: Maybe<Scalars['String']>;
  /** Whether to exclude nodes of this content type from front end search results. */
  excludeFromSearch?: Maybe<Scalars['Boolean']>;
  /** The plural name of the content type within the GraphQL Schema. */
  graphqlPluralName?: Maybe<Scalars['String']>;
  /** The singular name of the content type within the GraphQL Schema. */
  graphqlSingleName?: Maybe<Scalars['String']>;
  /** Whether this content type should have archives. Content archives are generated by type and by date. */
  hasArchive?: Maybe<Scalars['Boolean']>;
  /** Whether the content type is hierarchical, for example pages. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** The globally unique identifier of the post-type object. */
  id: Scalars['ID'];
  /** Whether this page is set to the static front page. */
  isFrontPage: Scalars['Boolean'];
  /** Whether this page is set to the blog posts page. */
  isPostsPage: Scalars['Boolean'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** Display name of the content type. */
  label?: Maybe<Scalars['String']>;
  /** Details about the content type labels. */
  labels?: Maybe<GQLPostTypeLabelDetails>;
  /** The name of the icon file to display as a menu icon. */
  menuIcon?: Maybe<Scalars['String']>;
  /** The position of this post type in the menu. Only applies if show_in_menu is true. */
  menuPosition?: Maybe<Scalars['Int']>;
  /** The internal name of the post type. This should not be used for display purposes. */
  name?: Maybe<Scalars['String']>;
  /** Whether a content type is intended for use publicly either via the admin interface or by front-end users. While the default settings of exclude_from_search, publicly_queryable, show_ui, and show_in_nav_menus are inherited from public, each does not rely on this relationship and controls a very specific intention. */
  public?: Maybe<Scalars['Boolean']>;
  /** Whether queries can be performed on the front end for the content type as part of parse_request(). */
  publiclyQueryable?: Maybe<Scalars['Boolean']>;
  /** Name of content type to display in REST API &quot;wp/v2&quot; namespace. */
  restBase?: Maybe<Scalars['String']>;
  /** The REST Controller class assigned to handling this content type. */
  restControllerClass?: Maybe<Scalars['String']>;
  /** Makes this content type available via the admin bar. */
  showInAdminBar?: Maybe<Scalars['Boolean']>;
  /** Whether to add the content type to the GraphQL Schema. */
  showInGraphql?: Maybe<Scalars['Boolean']>;
  /** Where to show the content type in the admin menu. To work, $show_ui must be true. If true, the post type is shown in its own top level menu. If false, no menu is shown. If a string of an existing top level menu (eg. &quot;tools.php&quot; or &quot;edit.php?post_type=page&quot;), the post type will be placed as a sub-menu of that. */
  showInMenu?: Maybe<Scalars['Boolean']>;
  /** Makes this content type available for selection in navigation menus. */
  showInNavMenus?: Maybe<Scalars['Boolean']>;
  /** Whether the content type is associated with a route under the the REST API &quot;wp/v2&quot; namespace. */
  showInRest?: Maybe<Scalars['Boolean']>;
  /** Whether to generate and allow a UI for managing this content type in the admin. */
  showUi?: Maybe<Scalars['Boolean']>;
  /** The unique resource identifier path */
  uri?: Maybe<Scalars['String']>;
};


/** An Post Type object */
export type GQLContentTypeconnectedTaxonomiesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** An Post Type object */
export type GQLContentTypecontentNodesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLContentTypeToContentNodeConnectionWhereArgs>;
};

/** Connection between the ContentType type and the Taxonomy type */
export type GQLContentTypeToTaxonomyConnection = {
  /** Edges for the ContentTypeToTaxonomyConnection connection */
  edges?: Maybe<Array<Maybe<GQLContentTypeToTaxonomyConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTaxonomy>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLContentTypeToTaxonomyConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTaxonomy>;
};

/** A taxonomy object */
export type GQLTaxonomy = GQLNode & {
  /** List of Content Types associated with the Taxonomy */
  connectedContentTypes?: Maybe<GQLTaxonomyToContentTypeConnection>;
  /** Description of the taxonomy. This field is equivalent to WP_Taxonomy-&gt;description */
  description?: Maybe<Scalars['String']>;
  /** The plural name of the post type within the GraphQL Schema. */
  graphqlPluralName?: Maybe<Scalars['String']>;
  /** The singular name of the post type within the GraphQL Schema. */
  graphqlSingleName?: Maybe<Scalars['String']>;
  /** Whether the taxonomy is hierarchical */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** The globally unique identifier of the taxonomy object. */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** Name of the taxonomy shown in the menu. Usually plural. */
  label?: Maybe<Scalars['String']>;
  /** The display name of the taxonomy. This field is equivalent to WP_Taxonomy-&gt;label */
  name?: Maybe<Scalars['String']>;
  /** Whether the taxonomy is publicly queryable */
  public?: Maybe<Scalars['Boolean']>;
  /** Name of content type to diplay in REST API &quot;wp/v2&quot; namespace. */
  restBase?: Maybe<Scalars['String']>;
  /** The REST Controller class assigned to handling this content type. */
  restControllerClass?: Maybe<Scalars['String']>;
  /** Whether to show the taxonomy as part of a tag cloud widget. This field is equivalent to WP_Taxonomy-&gt;show_tagcloud */
  showCloud?: Maybe<Scalars['Boolean']>;
  /** Whether to display a column for the taxonomy on its post type listing screens. */
  showInAdminColumn?: Maybe<Scalars['Boolean']>;
  /** Whether to add the post type to the GraphQL Schema. */
  showInGraphql?: Maybe<Scalars['Boolean']>;
  /** Whether to show the taxonomy in the admin menu */
  showInMenu?: Maybe<Scalars['Boolean']>;
  /** Whether the taxonomy is available for selection in navigation menus. */
  showInNavMenus?: Maybe<Scalars['Boolean']>;
  /** Whether to show the taxonomy in the quick/bulk edit panel. */
  showInQuickEdit?: Maybe<Scalars['Boolean']>;
  /** Whether to add the post type route in the REST API &quot;wp/v2&quot; namespace. */
  showInRest?: Maybe<Scalars['Boolean']>;
  /** Whether to generate and allow a UI for managing terms in this taxonomy in the admin */
  showUi?: Maybe<Scalars['Boolean']>;
};


/** A taxonomy object */
export type GQLTaxonomyconnectedContentTypesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};

/** Connection between the Taxonomy type and the ContentType type */
export type GQLTaxonomyToContentTypeConnection = {
  /** Edges for the TaxonomyToContentTypeConnection connection */
  edges?: Maybe<Array<Maybe<GQLTaxonomyToContentTypeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentType>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLTaxonomyToContentTypeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentType>;
};

/** Arguments for filtering the ContentTypeToContentNodeConnection connection */
export type GQLContentTypeToContentNodeConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the ContentType type and the ContentNode type */
export type GQLContentTypeToContentNodeConnection = {
  /** Edges for the ContentTypeToContentNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLContentTypeToContentNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLContentTypeToContentNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** Details for labels of the PostType */
export type GQLPostTypeLabelDetails = {
  /** Default is ‘Add New’ for both hierarchical and non-hierarchical types. */
  addNew?: Maybe<Scalars['String']>;
  /** Label for adding a new singular item. */
  addNewItem?: Maybe<Scalars['String']>;
  /** Label to signify all items in a submenu link. */
  allItems?: Maybe<Scalars['String']>;
  /** Label for archives in nav menus */
  archives?: Maybe<Scalars['String']>;
  /** Label for the attributes meta box. */
  attributes?: Maybe<Scalars['String']>;
  /** Label for editing a singular item. */
  editItem?: Maybe<Scalars['String']>;
  /** Label for the Featured Image meta box title. */
  featuredImage?: Maybe<Scalars['String']>;
  /** Label for the table views hidden heading. */
  filterItemsList?: Maybe<Scalars['String']>;
  /** Label for the media frame button. */
  insertIntoItem?: Maybe<Scalars['String']>;
  /** Label for the table hidden heading. */
  itemsList?: Maybe<Scalars['String']>;
  /** Label for the table pagination hidden heading. */
  itemsListNavigation?: Maybe<Scalars['String']>;
  /** Label for the menu name. */
  menuName?: Maybe<Scalars['String']>;
  /** General name for the post type, usually plural. */
  name?: Maybe<Scalars['String']>;
  /** Label for the new item page title. */
  newItem?: Maybe<Scalars['String']>;
  /** Label used when no items are found. */
  notFound?: Maybe<Scalars['String']>;
  /** Label used when no items are in the trash. */
  notFoundInTrash?: Maybe<Scalars['String']>;
  /** Label used to prefix parents of hierarchical items. */
  parentItemColon?: Maybe<Scalars['String']>;
  /** Label for removing the featured image. */
  removeFeaturedImage?: Maybe<Scalars['String']>;
  /** Label for searching plural items. */
  searchItems?: Maybe<Scalars['String']>;
  /** Label for setting the featured image. */
  setFeaturedImage?: Maybe<Scalars['String']>;
  /** Name for one object of this post type. */
  singularName?: Maybe<Scalars['String']>;
  /** Label for the media frame filter. */
  uploadedToThisItem?: Maybe<Scalars['String']>;
  /** Label in the media frame for using a featured image. */
  useFeaturedImage?: Maybe<Scalars['String']>;
  /** Label for viewing a singular item. */
  viewItem?: Maybe<Scalars['String']>;
  /** Label for viewing post type archives. */
  viewItems?: Maybe<Scalars['String']>;
};

/** Connection between the ContentNode type and the User type */
export type GQLContentNodeToEditLockConnectionEdge = {
  /** The timestamp for when the node was last edited */
  lockTimestamp?: Maybe<Scalars['String']>;
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLUser>;
};

/** A User object */
export type GQLUser = GQLNode & GQLUniformResourceIdentifiable & GQLCommenter & GQLDatabaseIdentifier & {
  /** Avatar object for user. The avatar object can be retrieved in different sizes by specifying the size argument. */
  avatar?: Maybe<GQLAvatar>;
  /** User metadata option name. Usually it will be &quot;wp_capabilities&quot;. */
  capKey?: Maybe<Scalars['String']>;
  /** A list of capabilities (permissions) granted to the user */
  capabilities?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Connection between the User type and the Comment type */
  comments?: Maybe<GQLUserToCommentConnection>;
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** Description of the user. */
  description?: Maybe<Scalars['String']>;
  /** Email address of the user. This is equivalent to the WP_User-&gt;user_email property. */
  email?: Maybe<Scalars['String']>;
  /** Connection between the User type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLUserToEnqueuedScriptConnection>;
  /** Connection between the User type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLUserToEnqueuedStylesheetConnection>;
  /** A complete list of capabilities including capabilities inherited from a role. This is equivalent to the array keys of WP_User-&gt;allcaps. */
  extraCapabilities?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** First name of the user. This is equivalent to the WP_User-&gt;user_first_name property. */
  firstName?: Maybe<Scalars['String']>;
  /** The globally unique identifier for the user object. */
  id: Scalars['ID'];
  /** Whether the JWT User secret has been revoked. If the secret has been revoked, auth tokens will not be issued until an admin, or user with proper capabilities re-issues a secret for the user. */
  isJwtAuthSecretRevoked: Scalars['Boolean'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The expiration for the JWT Token for the user. If not set custom for the user, it will use the default sitewide expiration setting */
  jwtAuthExpiration?: Maybe<Scalars['String']>;
  /** A JWT token that can be used in future requests for authentication/authorization */
  jwtAuthToken?: Maybe<Scalars['String']>;
  /** A JWT token that can be used in future requests to get a refreshed jwtAuthToken. If the refresh token used in a request is revoked or otherwise invalid, a valid Auth token will NOT be issued in the response headers. */
  jwtRefreshToken?: Maybe<Scalars['String']>;
  /** A unique secret tied to the users JWT token that can be revoked or refreshed. Revoking the secret prevents JWT tokens from being issued to the user. Refreshing the token invalidates previously issued tokens, but allows new tokens to be issued. */
  jwtUserSecret?: Maybe<Scalars['String']>;
  /** Last name of the user. This is equivalent to the WP_User-&gt;user_last_name property. */
  lastName?: Maybe<Scalars['String']>;
  /** The preferred language locale set for the user. Value derived from get_user_locale(). */
  locale?: Maybe<Scalars['String']>;
  /** Connection between the User type and the mediaItem type */
  mediaItems?: Maybe<GQLUserToMediaItemConnection>;
  /** Display name of the user. This is equivalent to the WP_User-&gt;dispaly_name property. */
  name?: Maybe<Scalars['String']>;
  /** The nicename for the user. This field is equivalent to WP_User-&gt;user_nicename */
  nicename?: Maybe<Scalars['String']>;
  /** Nickname of the user. */
  nickname?: Maybe<Scalars['String']>;
  /** Connection between the User type and the page type */
  pages?: Maybe<GQLUserToPageConnection>;
  /** Connection between the User type and the post type */
  posts?: Maybe<GQLUserToPostConnection>;
  /** The date the user registered or was created. The field follows a full ISO8601 date string format. */
  registeredDate?: Maybe<Scalars['String']>;
  /** Connection between the User and Revisions authored by the user */
  revisions?: Maybe<GQLUserToContentRevisionUnionConnection>;
  /** Connection between the User type and the UserRole type */
  roles?: Maybe<GQLUserToUserRoleConnection>;
  /** The slug for the user. This field is equivalent to WP_User-&gt;user_nicename */
  slug?: Maybe<Scalars['String']>;
  /** The unique resource identifier path */
  uri?: Maybe<Scalars['String']>;
  /** A website url that is associated with the user. */
  url?: Maybe<Scalars['String']>;
  /**
   * The Id of the user. Equivalent to WP_User-&gt;ID
   * @deprecated Deprecated in favor of the databaseId field
   */
  userId?: Maybe<Scalars['Int']>;
  /** Username for the user. This field is equivalent to WP_User-&gt;user_login. */
  username?: Maybe<Scalars['String']>;
};


/** A User object */
export type GQLUseravatarArgs = {
  size?: Maybe<Scalars['Int']>;
  forceDefault?: Maybe<Scalars['Boolean']>;
  rating?: Maybe<GQLAvatarRatingEnum>;
};


/** A User object */
export type GQLUsercommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLUserToCommentConnectionWhereArgs>;
};


/** A User object */
export type GQLUserenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** A User object */
export type GQLUserenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** A User object */
export type GQLUsermediaItemsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLUserToMediaItemConnectionWhereArgs>;
};


/** A User object */
export type GQLUserpagesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLUserToPageConnectionWhereArgs>;
};


/** A User object */
export type GQLUserpostsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLUserToPostConnectionWhereArgs>;
};


/** A User object */
export type GQLUserrevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLUserToContentRevisionUnionConnectionWhereArgs>;
};


/** A User object */
export type GQLUserrolesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};

/** The author of a comment */
export type GQLCommenter = {
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** The email address of the author of a comment. */
  email?: Maybe<Scalars['String']>;
  /** The globally unique identifier for the comment author. */
  id: Scalars['ID'];
  /** Whether the author information is considered restricted. (not fully public) */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The name of the author of a comment. */
  name?: Maybe<Scalars['String']>;
  /** The url of the author of a comment. */
  url?: Maybe<Scalars['String']>;
};

/** What rating to display avatars up to. Accepts 'G', 'PG', 'R', 'X', and are judged in that order. Default is the value of the 'avatar_rating' option */
export enum GQLAvatarRatingEnum {
  G = 'G',
  PG = 'PG',
  R = 'R',
  X = 'X'
}

/** Avatars are profile images for users. WordPress by default uses the Gravatar service to host and fetch avatars from. */
export type GQLAvatar = {
  /** URL for the default image or a default type. Accepts &#039;404&#039; (return a 404 instead of a default image), &#039;retro&#039; (8bit), &#039;monsterid&#039; (monster), &#039;wavatar&#039; (cartoon face), &#039;indenticon&#039; (the &#039;quilt&#039;), &#039;mystery&#039;, &#039;mm&#039;, or &#039;mysteryman&#039; (The Oyster Man), &#039;blank&#039; (transparent GIF), or &#039;gravatar_default&#039; (the Gravatar logo). */
  default?: Maybe<Scalars['String']>;
  /** HTML attributes to insert in the IMG element. Is not sanitized. */
  extraAttr?: Maybe<Scalars['String']>;
  /** Whether to always show the default image, never the Gravatar. */
  forceDefault?: Maybe<Scalars['Boolean']>;
  /** Whether the avatar was successfully found. */
  foundAvatar?: Maybe<Scalars['Boolean']>;
  /** Height of the avatar image. */
  height?: Maybe<Scalars['Int']>;
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** What rating to display avatars up to. Accepts &#039;G&#039;, &#039;PG&#039;, &#039;R&#039;, &#039;X&#039;, and are judged in that order. */
  rating?: Maybe<Scalars['String']>;
  /** Type of url scheme to use. Typically HTTP vs. HTTPS. */
  scheme?: Maybe<Scalars['String']>;
  /** The size of the avatar in pixels. A value of 96 will match a 96px x 96px gravatar image. */
  size?: Maybe<Scalars['Int']>;
  /** URL for the gravatar image source. */
  url?: Maybe<Scalars['String']>;
  /** Width of the avatar image. */
  width?: Maybe<Scalars['Int']>;
};

/** Arguments for filtering the UserToCommentConnection connection */
export type GQLUserToCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Allowed Content Types */
export enum GQLContentTypeEnum {
  /** The Type of Content object */
  ATTACHMENT = 'ATTACHMENT',
  /** The Type of Content object */
  PAGE = 'PAGE',
  /** The Type of Content object */
  POST = 'POST',
  /** The Type of Content object */
  SOCIAL_MEDIA = 'SOCIAL_MEDIA'
}

/** Options for ordering the connection */
export enum GQLCommentsConnectionOrderbyEnum {
  COMMENT_AGENT = 'COMMENT_AGENT',
  COMMENT_APPROVED = 'COMMENT_APPROVED',
  COMMENT_AUTHOR = 'COMMENT_AUTHOR',
  COMMENT_AUTHOR_EMAIL = 'COMMENT_AUTHOR_EMAIL',
  COMMENT_AUTHOR_IP = 'COMMENT_AUTHOR_IP',
  COMMENT_AUTHOR_URL = 'COMMENT_AUTHOR_URL',
  COMMENT_CONTENT = 'COMMENT_CONTENT',
  COMMENT_DATE = 'COMMENT_DATE',
  COMMENT_DATE_GMT = 'COMMENT_DATE_GMT',
  COMMENT_ID = 'COMMENT_ID',
  COMMENT_IN = 'COMMENT_IN',
  COMMENT_KARMA = 'COMMENT_KARMA',
  COMMENT_PARENT = 'COMMENT_PARENT',
  COMMENT_POST_ID = 'COMMENT_POST_ID',
  COMMENT_TYPE = 'COMMENT_TYPE',
  USER_ID = 'USER_ID'
}

/** Connection between the User type and the Comment type */
export type GQLUserToCommentConnection = {
  /** Edges for the UserToCommentConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToCommentConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLComment>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToCommentConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLComment>;
};

/** A Comment object */
export type GQLComment = GQLNode & GQLDatabaseIdentifier & {
  /** User agent used to post the comment. This field is equivalent to WP_Comment-&gt;comment_agent and the value matching the &quot;comment_agent&quot; column in SQL. */
  agent?: Maybe<Scalars['String']>;
  /** The approval status of the comment. This field is equivalent to WP_Comment-&gt;comment_approved and the value matching the &quot;comment_approved&quot; column in SQL. */
  approved?: Maybe<Scalars['Boolean']>;
  /** The author of the comment */
  author?: Maybe<GQLCommentToCommenterConnectionEdge>;
  /** IP address for the author. This field is equivalent to WP_Comment-&gt;comment_author_IP and the value matching the &quot;comment_author_IP&quot; column in SQL. */
  authorIp?: Maybe<Scalars['String']>;
  /**
   * ID for the comment, unique among comments.
   * @deprecated Deprecated in favor of databaseId
   */
  commentId?: Maybe<Scalars['Int']>;
  /** Connection between the Comment type and the ContentNode type */
  commentedOn?: Maybe<GQLCommentToContentNodeConnectionEdge>;
  /** Content of the comment. This field is equivalent to WP_Comment-&gt;comment_content and the value matching the &quot;comment_content&quot; column in SQL. */
  content?: Maybe<Scalars['String']>;
  /** The unique identifier stored in the database */
  databaseId: Scalars['Int'];
  /** Date the comment was posted in local time. This field is equivalent to WP_Comment-&gt;date and the value matching the &quot;date&quot; column in SQL. */
  date?: Maybe<Scalars['String']>;
  /** Date the comment was posted in GMT. This field is equivalent to WP_Comment-&gt;date_gmt and the value matching the &quot;date_gmt&quot; column in SQL. */
  dateGmt?: Maybe<Scalars['String']>;
  /** The globally unique identifier for the comment object */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** Karma value for the comment. This field is equivalent to WP_Comment-&gt;comment_karma and the value matching the &quot;comment_karma&quot; column in SQL. */
  karma?: Maybe<Scalars['Int']>;
  /** Connection between the Comment type and the Comment type */
  parent?: Maybe<GQLCommentToParentCommentConnectionEdge>;
  /** The database id of the parent comment node or null if it is the root comment */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent comment node. */
  parentId?: Maybe<Scalars['ID']>;
  /** Connection between the Comment type and the Comment type */
  replies?: Maybe<GQLCommentToCommentConnection>;
  /** Type of comment. This field is equivalent to WP_Comment-&gt;comment_type and the value matching the &quot;comment_type&quot; column in SQL. */
  type?: Maybe<Scalars['String']>;
};


/** A Comment object */
export type GQLCommentcontentArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};


/** A Comment object */
export type GQLCommentparentArgs = {
  where?: Maybe<GQLCommentToParentCommentConnectionWhereArgs>;
};


/** A Comment object */
export type GQLCommentrepliesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLCommentToCommentConnectionWhereArgs>;
};

/** Connection between the Comment type and the Commenter type */
export type GQLCommentToCommenterConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLCommenter>;
};

/** Connection between the Comment type and the ContentNode type */
export type GQLCommentToContentNodeConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLContentNode>;
};

/** The format of post field data. */
export enum GQLPostObjectFieldFormatEnum {
  /** Provide the field value directly from database */
  RAW = 'RAW',
  /** Apply the default WordPress rendering */
  RENDERED = 'RENDERED'
}

/** Arguments for filtering the CommentToParentCommentConnection connection */
export type GQLCommentToParentCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Connection between the Comment type and the Comment type */
export type GQLCommentToParentCommentConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLComment>;
};

/** Arguments for filtering the CommentToCommentConnection connection */
export type GQLCommentToCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Connection between the Comment type and the Comment type */
export type GQLCommentToCommentConnection = {
  /** Edges for the CommentToCommentConnection connection */
  edges?: Maybe<Array<Maybe<GQLCommentToCommentConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLComment>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLCommentToCommentConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLComment>;
};

/** Connection between the User type and the EnqueuedScript type */
export type GQLUserToEnqueuedScriptConnection = {
  /** Edges for the UserToEnqueuedScriptConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToEnqueuedScriptConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToEnqueuedScriptConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedScript>;
};

/** Connection between the User type and the EnqueuedStylesheet type */
export type GQLUserToEnqueuedStylesheetConnection = {
  /** Edges for the UserToEnqueuedStylesheetConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToEnqueuedStylesheetConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedStylesheet>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToEnqueuedStylesheetConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedStylesheet>;
};

/** Arguments for filtering the UserToMediaItemConnection connection */
export type GQLUserToMediaItemConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the User type and the mediaItem type */
export type GQLUserToMediaItemConnection = {
  /** Edges for the UserToMediaItemConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToMediaItemConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLMediaItem>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToMediaItemConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLMediaItem>;
};

/** The mediaItem type */
export type GQLMediaItem = GQLNode & GQLContentNode & GQLDatabaseIdentifier & GQLNodeWithTemplate & GQLUniformResourceIdentifiable & GQLNodeWithTitle & GQLNodeWithAuthor & GQLNodeWithComments & GQLHierarchicalContentNode & {
  /** Alternative text to display when resource is not displayed */
  altText?: Maybe<Scalars['String']>;
  /** Returns ancestors of the node. Default ordered as lowest (closest to the child) to highest (closest to the root). */
  ancestors?: Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnection>;
  /** Connection between the NodeWithAuthor type and the User type */
  author?: Maybe<GQLNodeWithAuthorToUserConnectionEdge>;
  /** The database identifier of the author of the node */
  authorDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the author of the node */
  authorId?: Maybe<Scalars['ID']>;
  /** The caption for the resource */
  caption?: Maybe<Scalars['String']>;
  /** Connection between the HierarchicalContentNode type and the ContentNode type */
  children?: Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnection>;
  /** The number of comments. Even though WPGraphQL denotes this field as an integer, in WordPress this field should be saved as a numeric string for compatibility. */
  commentCount?: Maybe<Scalars['Int']>;
  /** Whether the comments are open or closed for this particular post. */
  commentStatus?: Maybe<Scalars['String']>;
  /** Connection between the mediaItem type and the Comment type */
  comments?: Maybe<GQLMediaItemToCommentConnection>;
  /** Connection between the ContentNode type and the ContentType type */
  contentType?: Maybe<GQLContentNodeToContentTypeConnectionEdge>;
  /** The ID of the node in the database. */
  databaseId: Scalars['Int'];
  /** Post publishing date. */
  date?: Maybe<Scalars['String']>;
  /** The publishing date set in GMT. */
  dateGmt?: Maybe<Scalars['String']>;
  /** Description of the image (stored as post_content) */
  description?: Maybe<Scalars['String']>;
  /** The desired slug of the post */
  desiredSlug?: Maybe<Scalars['String']>;
  /** If a user has edited the node within the past 15 seconds, this will return the user that last edited. Null if the edit lock doesn&#039;t exist or is greater than 15 seconds */
  editingLockedBy?: Maybe<GQLContentNodeToEditLockConnectionEdge>;
  /** The RSS enclosure for the object */
  enclosure?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLContentNodeToEnqueuedScriptConnection>;
  /** Connection between the ContentNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLContentNodeToEnqueuedStylesheetConnection>;
  /** The filesize in bytes of the resource */
  fileSize?: Maybe<Scalars['Int']>;
  /** The global unique identifier for this post. This currently matches the value stored in WP_Post-&gt;guid and the guid column in the &quot;post_objects&quot; database table. */
  guid?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the attachment object. */
  id: Scalars['ID'];
  /** Whether the object is a node in the preview state */
  isPreview?: Maybe<Scalars['Boolean']>;
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The user that most recently edited the node */
  lastEditedBy?: Maybe<GQLContentNodeToEditLastConnectionEdge>;
  /** The permalink of the post */
  link?: Maybe<Scalars['String']>;
  /** Details about the mediaItem */
  mediaDetails?: Maybe<GQLMediaDetails>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of the databaseId field
   */
  mediaItemId: Scalars['Int'];
  /** Url of the mediaItem */
  mediaItemUrl?: Maybe<Scalars['String']>;
  /** Type of resource */
  mediaType?: Maybe<Scalars['String']>;
  /** The mime type of the mediaItem */
  mimeType?: Maybe<Scalars['String']>;
  /** The local modified time for a post. If a post was recently updated the modified field will change to match the corresponding time. */
  modified?: Maybe<Scalars['String']>;
  /** The GMT modified time for a post. If a post was recently updated the modified field will change to match the corresponding time in GMT. */
  modifiedGmt?: Maybe<Scalars['String']>;
  /** The parent of the node. The parent object can be of various types */
  parent?: Maybe<GQLHierarchicalContentNodeToParentContentNodeConnectionEdge>;
  /** Database id of the parent node */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent node. */
  parentId?: Maybe<Scalars['ID']>;
  /** The database id of the preview node */
  previewRevisionDatabaseId?: Maybe<Scalars['Int']>;
  /** Whether the object is a node in the preview state */
  previewRevisionId?: Maybe<Scalars['ID']>;
  /** The sizes attribute value for an image. */
  sizes?: Maybe<Scalars['String']>;
  /** The uri slug for the post. This is equivalent to the WP_Post-&gt;post_name field and the post_name column in the database for the &quot;post_objects&quot; table. */
  slug?: Maybe<Scalars['String']>;
  /** Url of the mediaItem */
  sourceUrl?: Maybe<Scalars['String']>;
  /** The srcset attribute specifies the URL of the image to use in different situations. It is a comma separated string of urls and their widths. */
  srcSet?: Maybe<Scalars['String']>;
  /** The current status of the object */
  status?: Maybe<Scalars['String']>;
  /** The template assigned to a node of content */
  template?: Maybe<GQLContentTemplate>;
  /** The title of the post. This is currently just the raw title. An amendment to support rendered title needs to be made. */
  title?: Maybe<Scalars['String']>;
  /** URI path for the resource */
  uri: Scalars['String'];
};


/** The mediaItem type */
export type GQLMediaItemancestorsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnectionWhereArgs>;
};


/** The mediaItem type */
export type GQLMediaItemcaptionArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};


/** The mediaItem type */
export type GQLMediaItemchildrenArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnectionWhereArgs>;
};


/** The mediaItem type */
export type GQLMediaItemcommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLMediaItemToCommentConnectionWhereArgs>;
};


/** The mediaItem type */
export type GQLMediaItemdescriptionArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};


/** The mediaItem type */
export type GQLMediaItemenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The mediaItem type */
export type GQLMediaItemenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The mediaItem type */
export type GQLMediaItemfileSizeArgs = {
  size?: Maybe<GQLMediaItemSizeEnum>;
};


/** The mediaItem type */
export type GQLMediaItemsizesArgs = {
  size?: Maybe<GQLMediaItemSizeEnum>;
};


/** The mediaItem type */
export type GQLMediaItemsourceUrlArgs = {
  size?: Maybe<GQLMediaItemSizeEnum>;
};


/** The mediaItem type */
export type GQLMediaItemsrcSetArgs = {
  size?: Maybe<GQLMediaItemSizeEnum>;
};


/** The mediaItem type */
export type GQLMediaItemtitleArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** A node that can have a template associated with it */
export type GQLNodeWithTemplate = {
  /** The template assigned to the node */
  template?: Maybe<GQLContentTemplate>;
};

/** The template assigned to a node of content */
export type GQLContentTemplate = {
  /** The name of the template */
  templateName?: Maybe<Scalars['String']>;
};

/** A node that NodeWith a title */
export type GQLNodeWithTitle = {
  /** The title of the post. This is currently just the raw title. An amendment to support rendered title needs to be made. */
  title?: Maybe<Scalars['String']>;
};


/** A node that NodeWith a title */
export type GQLNodeWithTitletitleArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** A node that can have an author assigned to it */
export type GQLNodeWithAuthor = {
  /** Connection between the NodeWithAuthor type and the User type */
  author?: Maybe<GQLNodeWithAuthorToUserConnectionEdge>;
  /** The database identifier of the author of the node */
  authorDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the author of the node */
  authorId?: Maybe<Scalars['ID']>;
};

/** Connection between the NodeWithAuthor type and the User type */
export type GQLNodeWithAuthorToUserConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLUser>;
};

/** A node that can have comments associated with it */
export type GQLNodeWithComments = {
  /** The number of comments. Even though WPGraphQL denotes this field as an integer, in WordPress this field should be saved as a numeric string for compatibility. */
  commentCount?: Maybe<Scalars['Int']>;
  /** Whether the comments are open or closed for this particular post. */
  commentStatus?: Maybe<Scalars['String']>;
};

/** Content node with hierarchical (parent/child) relationships */
export type GQLHierarchicalContentNode = {
  /** Returns ancestors of the node. Default ordered as lowest (closest to the child) to highest (closest to the root). */
  ancestors?: Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnection>;
  /** Connection between the HierarchicalContentNode type and the ContentNode type */
  children?: Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnection>;
  /** The parent of the node. The parent object can be of various types */
  parent?: Maybe<GQLHierarchicalContentNodeToParentContentNodeConnectionEdge>;
  /** Database id of the parent node */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent node. */
  parentId?: Maybe<Scalars['ID']>;
};


/** Content node with hierarchical (parent/child) relationships */
export type GQLHierarchicalContentNodeancestorsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnectionWhereArgs>;
};


/** Content node with hierarchical (parent/child) relationships */
export type GQLHierarchicalContentNodechildrenArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnectionWhereArgs>;
};

/** Arguments for filtering the HierarchicalContentNodeToContentNodeAncestorsConnection connection */
export type GQLHierarchicalContentNodeToContentNodeAncestorsConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the HierarchicalContentNode type and the ContentNode type */
export type GQLHierarchicalContentNodeToContentNodeAncestorsConnection = {
  /** Edges for the HierarchicalContentNodeToContentNodeAncestorsConnection connection */
  edges?: Maybe<Array<Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLHierarchicalContentNodeToContentNodeAncestorsConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** Arguments for filtering the HierarchicalContentNodeToContentNodeChildrenConnection connection */
export type GQLHierarchicalContentNodeToContentNodeChildrenConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the HierarchicalContentNode type and the ContentNode type */
export type GQLHierarchicalContentNodeToContentNodeChildrenConnection = {
  /** Edges for the HierarchicalContentNodeToContentNodeChildrenConnection connection */
  edges?: Maybe<Array<Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLHierarchicalContentNodeToContentNodeChildrenConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** Connection between the HierarchicalContentNode type and the ContentNode type */
export type GQLHierarchicalContentNodeToParentContentNodeConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLContentNode>;
};

/** Arguments for filtering the MediaItemToCommentConnection connection */
export type GQLMediaItemToCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Connection between the mediaItem type and the Comment type */
export type GQLMediaItemToCommentConnection = {
  /** Edges for the MediaItemToCommentConnection connection */
  edges?: Maybe<Array<Maybe<GQLMediaItemToCommentConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLComment>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLMediaItemToCommentConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLComment>;
};

/** Connection between the ContentNode type and the EnqueuedScript type */
export type GQLContentNodeToEnqueuedScriptConnection = {
  /** Edges for the ContentNodeToEnqueuedScriptConnection connection */
  edges?: Maybe<Array<Maybe<GQLContentNodeToEnqueuedScriptConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLContentNodeToEnqueuedScriptConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedScript>;
};

/** Connection between the ContentNode type and the EnqueuedStylesheet type */
export type GQLContentNodeToEnqueuedStylesheetConnection = {
  /** Edges for the ContentNodeToEnqueuedStylesheetConnection connection */
  edges?: Maybe<Array<Maybe<GQLContentNodeToEnqueuedStylesheetConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedStylesheet>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLContentNodeToEnqueuedStylesheetConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedStylesheet>;
};

/** The size of the media item object. */
export enum GQLMediaItemSizeEnum {
  /** MediaItem with the large size */
  LARGE = 'LARGE',
  /** MediaItem with the medium size */
  MEDIUM = 'MEDIUM',
  /** MediaItem with the medium_large size */
  MEDIUM_LARGE = 'MEDIUM_LARGE',
  /** MediaItem with the post-thumbnail size */
  POST_THUMBNAIL = 'POST_THUMBNAIL',
  /** MediaItem with the thumbnail size */
  THUMBNAIL = 'THUMBNAIL',
  /** MediaItem with the 1536x1536 size */
  _1536X1536 = '_1536X1536',
  /** MediaItem with the 2048x2048 size */
  _2048X2048 = '_2048X2048'
}

/** Connection between the ContentNode type and the User type */
export type GQLContentNodeToEditLastConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLUser>;
};

/** File details for a Media Item */
export type GQLMediaDetails = {
  /** The height of the mediaItem */
  file?: Maybe<Scalars['String']>;
  /** The height of the mediaItem */
  height?: Maybe<Scalars['Int']>;
  meta?: Maybe<GQLMediaItemMeta>;
  /** The available sizes of the mediaItem */
  sizes?: Maybe<Array<Maybe<GQLMediaSize>>>;
  /** The width of the mediaItem */
  width?: Maybe<Scalars['Int']>;
};

/** Meta connected to a MediaItem */
export type GQLMediaItemMeta = {
  aperture?: Maybe<Scalars['Float']>;
  camera?: Maybe<Scalars['String']>;
  caption?: Maybe<Scalars['String']>;
  copyright?: Maybe<Scalars['String']>;
  createdTimestamp?: Maybe<Scalars['Int']>;
  credit?: Maybe<Scalars['String']>;
  focalLength?: Maybe<Scalars['Float']>;
  iso?: Maybe<Scalars['Int']>;
  keywords?: Maybe<Array<Maybe<Scalars['String']>>>;
  orientation?: Maybe<Scalars['String']>;
  shutterSpeed?: Maybe<Scalars['Float']>;
  title?: Maybe<Scalars['String']>;
};

/** Details of an available size for a media item */
export type GQLMediaSize = {
  /** The file of the for the referenced size */
  file?: Maybe<Scalars['String']>;
  /** The filesize of the resource */
  fileSize?: Maybe<Scalars['Int']>;
  /** The height of the for the referenced size */
  height?: Maybe<Scalars['String']>;
  /** The mime type of the resource */
  mimeType?: Maybe<Scalars['String']>;
  /** The referenced size name */
  name?: Maybe<Scalars['String']>;
  /** The url of the for the referenced size */
  sourceUrl?: Maybe<Scalars['String']>;
  /** The width of the for the referenced size */
  width?: Maybe<Scalars['String']>;
};

/** Arguments for filtering the UserToPageConnection connection */
export type GQLUserToPageConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the User type and the page type */
export type GQLUserToPageConnection = {
  /** Edges for the UserToPageConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToPageConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPage>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToPageConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPage>;
};

/** The page type */
export type GQLPage = GQLNode & GQLContentNode & GQLDatabaseIdentifier & GQLNodeWithTemplate & GQLUniformResourceIdentifiable & GQLNodeWithTitle & GQLNodeWithContentEditor & GQLNodeWithAuthor & GQLNodeWithFeaturedImage & GQLNodeWithComments & GQLNodeWithRevisions & GQLNodeWithPageAttributes & GQLHierarchicalContentNode & GQLMenuItemLinkable & {
  /** Returns ancestors of the node. Default ordered as lowest (closest to the child) to highest (closest to the root). */
  ancestors?: Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnection>;
  /** Connection between the NodeWithAuthor type and the User type */
  author?: Maybe<GQLNodeWithAuthorToUserConnectionEdge>;
  /** The database identifier of the author of the node */
  authorDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the author of the node */
  authorId?: Maybe<Scalars['ID']>;
  /** Connection between the HierarchicalContentNode type and the ContentNode type */
  children?: Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnection>;
  /** The number of comments. Even though WPGraphQL denotes this field as an integer, in WordPress this field should be saved as a numeric string for compatibility. */
  commentCount?: Maybe<Scalars['Int']>;
  /** Whether the comments are open or closed for this particular post. */
  commentStatus?: Maybe<Scalars['String']>;
  /** Connection between the page type and the Comment type */
  comments?: Maybe<GQLPageToCommentConnection>;
  /** The content of the post. */
  content?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the ContentType type */
  contentType?: Maybe<GQLContentNodeToContentTypeConnectionEdge>;
  /** The ID of the node in the database. */
  databaseId: Scalars['Int'];
  /** Post publishing date. */
  date?: Maybe<Scalars['String']>;
  /** The publishing date set in GMT. */
  dateGmt?: Maybe<Scalars['String']>;
  /** The desired slug of the post */
  desiredSlug?: Maybe<Scalars['String']>;
  /** If a user has edited the node within the past 15 seconds, this will return the user that last edited. Null if the edit lock doesn&#039;t exist or is greater than 15 seconds */
  editingLockedBy?: Maybe<GQLContentNodeToEditLockConnectionEdge>;
  /** The RSS enclosure for the object */
  enclosure?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLContentNodeToEnqueuedScriptConnection>;
  /** Connection between the ContentNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLContentNodeToEnqueuedStylesheetConnection>;
  /** Connection between the NodeWithFeaturedImage type and the MediaItem type */
  featuredImage?: Maybe<GQLNodeWithFeaturedImageToMediaItemConnectionEdge>;
  /** The database identifier for the featured image node assigned to the content node */
  featuredImageDatabaseId?: Maybe<Scalars['Int']>;
  /** Globally unique ID of the featured image assigned to the node */
  featuredImageId?: Maybe<Scalars['ID']>;
  /** The global unique identifier for this post. This currently matches the value stored in WP_Post-&gt;guid and the guid column in the &quot;post_objects&quot; database table. */
  guid?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the page object. */
  id: Scalars['ID'];
  /** Whether this page is set to the static front page. */
  isFrontPage: Scalars['Boolean'];
  /** Whether this page is set to the blog posts page. */
  isPostsPage: Scalars['Boolean'];
  /** Whether the object is a node in the preview state */
  isPreview?: Maybe<Scalars['Boolean']>;
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** True if the node is a revision of another node */
  isRevision?: Maybe<Scalars['Boolean']>;
  /** The user that most recently edited the node */
  lastEditedBy?: Maybe<GQLContentNodeToEditLastConnectionEdge>;
  /** The permalink of the post */
  link?: Maybe<Scalars['String']>;
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The local modified time for a post. If a post was recently updated the modified field will change to match the corresponding time. */
  modified?: Maybe<Scalars['String']>;
  /** The GMT modified time for a post. If a post was recently updated the modified field will change to match the corresponding time in GMT. */
  modifiedGmt?: Maybe<Scalars['String']>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of the databaseId field
   */
  pageId: Scalars['Int'];
  /** The parent of the node. The parent object can be of various types */
  parent?: Maybe<GQLHierarchicalContentNodeToParentContentNodeConnectionEdge>;
  /** Database id of the parent node */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent node. */
  parentId?: Maybe<Scalars['ID']>;
  /** Connection between the page type and the page type */
  preview?: Maybe<GQLPageToPreviewConnectionEdge>;
  /** The database id of the preview node */
  previewRevisionDatabaseId?: Maybe<Scalars['Int']>;
  /** Whether the object is a node in the preview state */
  previewRevisionId?: Maybe<Scalars['ID']>;
  /** If the current node is a revision, this field exposes the node this is a revision of. Returns null if the node is not a revision of another node. */
  revisionOf?: Maybe<GQLNodeWithRevisionsToContentNodeConnectionEdge>;
  /** Connection between the page type and the page type */
  revisions?: Maybe<GQLPageToRevisionConnection>;
  /** The uri slug for the post. This is equivalent to the WP_Post-&gt;post_name field and the post_name column in the database for the &quot;post_objects&quot; table. */
  slug?: Maybe<Scalars['String']>;
  /** The current status of the object */
  status?: Maybe<Scalars['String']>;
  /** The template assigned to a node of content */
  template?: Maybe<GQLContentTemplate>;
  /** The title of the post. This is currently just the raw title. An amendment to support rendered title needs to be made. */
  title?: Maybe<Scalars['String']>;
  /** URI path for the resource */
  uri: Scalars['String'];
};


/** The page type */
export type GQLPageancestorsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLHierarchicalContentNodeToContentNodeAncestorsConnectionWhereArgs>;
};


/** The page type */
export type GQLPagechildrenArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLHierarchicalContentNodeToContentNodeChildrenConnectionWhereArgs>;
};


/** The page type */
export type GQLPagecommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPageToCommentConnectionWhereArgs>;
};


/** The page type */
export type GQLPagecontentArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};


/** The page type */
export type GQLPageenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The page type */
export type GQLPageenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The page type */
export type GQLPagerevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPageToRevisionConnectionWhereArgs>;
};


/** The page type */
export type GQLPagetitleArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** A node that supports the content editor */
export type GQLNodeWithContentEditor = {
  /** The content of the post. */
  content?: Maybe<Scalars['String']>;
};


/** A node that supports the content editor */
export type GQLNodeWithContentEditorcontentArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** A node that can have a featured image set */
export type GQLNodeWithFeaturedImage = {
  /** Connection between the NodeWithFeaturedImage type and the MediaItem type */
  featuredImage?: Maybe<GQLNodeWithFeaturedImageToMediaItemConnectionEdge>;
  /** The database identifier for the featured image node assigned to the content node */
  featuredImageDatabaseId?: Maybe<Scalars['Int']>;
  /** Globally unique ID of the featured image assigned to the node */
  featuredImageId?: Maybe<Scalars['ID']>;
};

/** Connection between the NodeWithFeaturedImage type and the MediaItem type */
export type GQLNodeWithFeaturedImageToMediaItemConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLMediaItem>;
};

/** A node that can have revisions */
export type GQLNodeWithRevisions = {
  /** True if the node is a revision of another node */
  isRevision?: Maybe<Scalars['Boolean']>;
  /** If the current node is a revision, this field exposes the node this is a revision of. Returns null if the node is not a revision of another node. */
  revisionOf?: Maybe<GQLNodeWithRevisionsToContentNodeConnectionEdge>;
};

/** Connection between the NodeWithRevisions type and the ContentNode type */
export type GQLNodeWithRevisionsToContentNodeConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLContentNode>;
};

/** A node that can have page attributes */
export type GQLNodeWithPageAttributes = {
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
};

/** Arguments for filtering the PageToCommentConnection connection */
export type GQLPageToCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Connection between the page type and the Comment type */
export type GQLPageToCommentConnection = {
  /** Edges for the PageToCommentConnection connection */
  edges?: Maybe<Array<Maybe<GQLPageToCommentConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLComment>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPageToCommentConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLComment>;
};

/** Connection between the page type and the page type */
export type GQLPageToPreviewConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLPage>;
};

/** Arguments for filtering the pageToRevisionConnection connection */
export type GQLPageToRevisionConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the page type and the page type */
export type GQLPageToRevisionConnection = {
  /** Edges for the pageToRevisionConnection connection */
  edges?: Maybe<Array<Maybe<GQLPageToRevisionConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPage>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPageToRevisionConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPage>;
};

/** Arguments for filtering the UserToPostConnection connection */
export type GQLUserToPostConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Category ID */
  categoryId?: Maybe<Scalars['Int']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Use Category Slug */
  categoryName?: Maybe<Scalars['String']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Tag Slug */
  tag?: Maybe<Scalars['String']>;
  /** Use Tag ID */
  tagId?: Maybe<Scalars['String']>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag slugs, used to display objects from one tag OR another */
  tagSlugAnd?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of tag slugs, used to exclude objects in specified tags */
  tagSlugIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the User type and the post type */
export type GQLUserToPostConnection = {
  /** Edges for the UserToPostConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToPostConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPost>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToPostConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPost>;
};

/** The post type */
export type GQLPost = GQLNode & GQLContentNode & GQLDatabaseIdentifier & GQLNodeWithTemplate & GQLUniformResourceIdentifiable & GQLNodeWithTitle & GQLNodeWithContentEditor & GQLNodeWithAuthor & GQLNodeWithFeaturedImage & GQLNodeWithExcerpt & GQLNodeWithComments & GQLNodeWithTrackbacks & GQLNodeWithRevisions & GQLMenuItemLinkable & {
  /** Connection between the NodeWithAuthor type and the User type */
  author?: Maybe<GQLNodeWithAuthorToUserConnectionEdge>;
  /** The database identifier of the author of the node */
  authorDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the author of the node */
  authorId?: Maybe<Scalars['ID']>;
  /** Connection between the post type and the category type */
  categories?: Maybe<GQLPostToCategoryConnection>;
  /** The number of comments. Even though WPGraphQL denotes this field as an integer, in WordPress this field should be saved as a numeric string for compatibility. */
  commentCount?: Maybe<Scalars['Int']>;
  /** Whether the comments are open or closed for this particular post. */
  commentStatus?: Maybe<Scalars['String']>;
  /** Connection between the post type and the Comment type */
  comments?: Maybe<GQLPostToCommentConnection>;
  /** The content of the post. */
  content?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the ContentType type */
  contentType?: Maybe<GQLContentNodeToContentTypeConnectionEdge>;
  /** The ID of the node in the database. */
  databaseId: Scalars['Int'];
  /** Post publishing date. */
  date?: Maybe<Scalars['String']>;
  /** The publishing date set in GMT. */
  dateGmt?: Maybe<Scalars['String']>;
  /** The desired slug of the post */
  desiredSlug?: Maybe<Scalars['String']>;
  /** If a user has edited the node within the past 15 seconds, this will return the user that last edited. Null if the edit lock doesn&#039;t exist or is greater than 15 seconds */
  editingLockedBy?: Maybe<GQLContentNodeToEditLockConnectionEdge>;
  /** The RSS enclosure for the object */
  enclosure?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLContentNodeToEnqueuedScriptConnection>;
  /** Connection between the ContentNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLContentNodeToEnqueuedStylesheetConnection>;
  /** The excerpt of the post. */
  excerpt?: Maybe<Scalars['String']>;
  /** Connection between the NodeWithFeaturedImage type and the MediaItem type */
  featuredImage?: Maybe<GQLNodeWithFeaturedImageToMediaItemConnectionEdge>;
  /** The database identifier for the featured image node assigned to the content node */
  featuredImageDatabaseId?: Maybe<Scalars['Int']>;
  /** Globally unique ID of the featured image assigned to the node */
  featuredImageId?: Maybe<Scalars['ID']>;
  /** The global unique identifier for this post. This currently matches the value stored in WP_Post-&gt;guid and the guid column in the &quot;post_objects&quot; database table. */
  guid?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the post object. */
  id: Scalars['ID'];
  /** Whether the object is a node in the preview state */
  isPreview?: Maybe<Scalars['Boolean']>;
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** True if the node is a revision of another node */
  isRevision?: Maybe<Scalars['Boolean']>;
  /** Whether this page is sticky */
  isSticky: Scalars['Boolean'];
  /** The user that most recently edited the node */
  lastEditedBy?: Maybe<GQLContentNodeToEditLastConnectionEdge>;
  /** The permalink of the post */
  link?: Maybe<Scalars['String']>;
  /** The local modified time for a post. If a post was recently updated the modified field will change to match the corresponding time. */
  modified?: Maybe<Scalars['String']>;
  /** The GMT modified time for a post. If a post was recently updated the modified field will change to match the corresponding time in GMT. */
  modifiedGmt?: Maybe<Scalars['String']>;
  /** Whether the pings are open or closed for this particular post. */
  pingStatus?: Maybe<Scalars['String']>;
  /** URLs that have been pinged. */
  pinged?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Connection between the post type and the postFormat type */
  postFormats?: Maybe<GQLPostToPostFormatConnection>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of the databaseId field
   */
  postId: Scalars['Int'];
  /** Connection between the post type and the post type */
  preview?: Maybe<GQLPostToPreviewConnectionEdge>;
  /** The database id of the preview node */
  previewRevisionDatabaseId?: Maybe<Scalars['Int']>;
  /** Whether the object is a node in the preview state */
  previewRevisionId?: Maybe<Scalars['ID']>;
  /** If the current node is a revision, this field exposes the node this is a revision of. Returns null if the node is not a revision of another node. */
  revisionOf?: Maybe<GQLNodeWithRevisionsToContentNodeConnectionEdge>;
  /** Connection between the post type and the post type */
  revisions?: Maybe<GQLPostToRevisionConnection>;
  /** The uri slug for the post. This is equivalent to the WP_Post-&gt;post_name field and the post_name column in the database for the &quot;post_objects&quot; table. */
  slug?: Maybe<Scalars['String']>;
  /** The current status of the object */
  status?: Maybe<Scalars['String']>;
  /** Connection between the post type and the tag type */
  tags?: Maybe<GQLPostToTagConnection>;
  /** The template assigned to a node of content */
  template?: Maybe<GQLContentTemplate>;
  /** Connection between the post type and the TermNode type */
  terms?: Maybe<GQLPostToTermNodeConnection>;
  /** The title of the post. This is currently just the raw title. An amendment to support rendered title needs to be made. */
  title?: Maybe<Scalars['String']>;
  /** URLs queued to be pinged. */
  toPing?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** URI path for the resource */
  uri: Scalars['String'];
};


/** The post type */
export type GQLPostcategoriesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostToCategoryConnectionWhereArgs>;
};


/** The post type */
export type GQLPostcommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostToCommentConnectionWhereArgs>;
};


/** The post type */
export type GQLPostcontentArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};


/** The post type */
export type GQLPostenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The post type */
export type GQLPostenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The post type */
export type GQLPostexcerptArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};


/** The post type */
export type GQLPostpostFormatsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostToPostFormatConnectionWhereArgs>;
};


/** The post type */
export type GQLPostrevisionsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostToRevisionConnectionWhereArgs>;
};


/** The post type */
export type GQLPosttagsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostToTagConnectionWhereArgs>;
};


/** The post type */
export type GQLPosttermsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostToTermNodeConnectionWhereArgs>;
};


/** The post type */
export type GQLPosttitleArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** A node that can have an excerpt */
export type GQLNodeWithExcerpt = {
  /** The excerpt of the post. */
  excerpt?: Maybe<Scalars['String']>;
};


/** A node that can have an excerpt */
export type GQLNodeWithExcerptexcerptArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** A node that can have trackbacks and pingbacks */
export type GQLNodeWithTrackbacks = {
  /** Whether the pings are open or closed for this particular post. */
  pingStatus?: Maybe<Scalars['String']>;
  /** URLs that have been pinged. */
  pinged?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** URLs queued to be pinged. */
  toPing?: Maybe<Array<Maybe<Scalars['String']>>>;
};

/** Arguments for filtering the PostToCategoryConnection connection */
export type GQLPostToCategoryConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the post type and the category type */
export type GQLPostToCategoryConnection = {
  /** Edges for the PostToCategoryConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostToCategoryConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLCategory>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostToCategoryConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLCategory>;
};

/** Arguments for filtering the PostToCommentConnection connection */
export type GQLPostToCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Connection between the post type and the Comment type */
export type GQLPostToCommentConnection = {
  /** Edges for the PostToCommentConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostToCommentConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLComment>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostToCommentConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLComment>;
};

/** Arguments for filtering the PostToPostFormatConnection connection */
export type GQLPostToPostFormatConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the post type and the postFormat type */
export type GQLPostToPostFormatConnection = {
  /** Edges for the PostToPostFormatConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostToPostFormatConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPostFormat>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostToPostFormatConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPostFormat>;
};

/** The postFormat type */
export type GQLPostFormat = GQLNode & GQLTermNode & GQLDatabaseIdentifier & GQLUniformResourceIdentifiable & GQLMenuItemLinkable & {
  /** Connection between the postFormat type and the ContentNode type */
  contentNodes?: Maybe<GQLPostFormatToContentNodeConnection>;
  /** The number of objects connected to the object */
  count?: Maybe<Scalars['Int']>;
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** The description of the object */
  description?: Maybe<Scalars['String']>;
  /** Connection between the TermNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLTermNodeToEnqueuedScriptConnection>;
  /** Connection between the TermNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLTermNodeToEnqueuedStylesheetConnection>;
  /** The globally unique ID for the object */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The link to the term */
  link?: Maybe<Scalars['String']>;
  /** The human friendly name of the object. */
  name?: Maybe<Scalars['String']>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of databaseId
   */
  postFormatId?: Maybe<Scalars['Int']>;
  /** Connection between the postFormat type and the post type */
  posts?: Maybe<GQLPostFormatToPostConnection>;
  /** An alphanumeric identifier for the object unique to its type. */
  slug?: Maybe<Scalars['String']>;
  /** Connection between the postFormat type and the Taxonomy type */
  taxonomy?: Maybe<GQLPostFormatToTaxonomyConnectionEdge>;
  /** The ID of the term group that this term object belongs to */
  termGroupId?: Maybe<Scalars['Int']>;
  /** The taxonomy ID that the object is associated with */
  termTaxonomyId?: Maybe<Scalars['Int']>;
  /** The unique resource identifier path */
  uri: Scalars['String'];
};


/** The postFormat type */
export type GQLPostFormatcontentNodesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostFormatToContentNodeConnectionWhereArgs>;
};


/** The postFormat type */
export type GQLPostFormatenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The postFormat type */
export type GQLPostFormatenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The postFormat type */
export type GQLPostFormatpostsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLPostFormatToPostConnectionWhereArgs>;
};

/** Arguments for filtering the PostFormatToContentNodeConnection connection */
export type GQLPostFormatToContentNodeConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the postFormat type and the ContentNode type */
export type GQLPostFormatToContentNodeConnection = {
  /** Edges for the PostFormatToContentNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostFormatToContentNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostFormatToContentNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** Arguments for filtering the PostFormatToPostConnection connection */
export type GQLPostFormatToPostConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Category ID */
  categoryId?: Maybe<Scalars['Int']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Use Category Slug */
  categoryName?: Maybe<Scalars['String']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Tag Slug */
  tag?: Maybe<Scalars['String']>;
  /** Use Tag ID */
  tagId?: Maybe<Scalars['String']>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag slugs, used to display objects from one tag OR another */
  tagSlugAnd?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of tag slugs, used to exclude objects in specified tags */
  tagSlugIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the postFormat type and the post type */
export type GQLPostFormatToPostConnection = {
  /** Edges for the PostFormatToPostConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostFormatToPostConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPost>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostFormatToPostConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPost>;
};

/** Connection between the postFormat type and the Taxonomy type */
export type GQLPostFormatToTaxonomyConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLTaxonomy>;
};

/** Connection between the post type and the post type */
export type GQLPostToPreviewConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLPost>;
};

/** Arguments for filtering the postToRevisionConnection connection */
export type GQLPostToRevisionConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Category ID */
  categoryId?: Maybe<Scalars['Int']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Use Category Slug */
  categoryName?: Maybe<Scalars['String']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Tag Slug */
  tag?: Maybe<Scalars['String']>;
  /** Use Tag ID */
  tagId?: Maybe<Scalars['String']>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag slugs, used to display objects from one tag OR another */
  tagSlugAnd?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of tag slugs, used to exclude objects in specified tags */
  tagSlugIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the post type and the post type */
export type GQLPostToRevisionConnection = {
  /** Edges for the postToRevisionConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostToRevisionConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPost>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostToRevisionConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPost>;
};

/** Arguments for filtering the PostToTagConnection connection */
export type GQLPostToTagConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the post type and the tag type */
export type GQLPostToTagConnection = {
  /** Edges for the PostToTagConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostToTagConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTag>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostToTagConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTag>;
};

/** The tag type */
export type GQLTag = GQLNode & GQLTermNode & GQLDatabaseIdentifier & GQLUniformResourceIdentifiable & GQLMenuItemLinkable & {
  /** Connection between the tag type and the ContentNode type */
  contentNodes?: Maybe<GQLTagToContentNodeConnection>;
  /** The number of objects connected to the object */
  count?: Maybe<Scalars['Int']>;
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** The description of the object */
  description?: Maybe<Scalars['String']>;
  /** Connection between the TermNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLTermNodeToEnqueuedScriptConnection>;
  /** Connection between the TermNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLTermNodeToEnqueuedStylesheetConnection>;
  /** The globally unique ID for the object */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The link to the term */
  link?: Maybe<Scalars['String']>;
  /** The human friendly name of the object. */
  name?: Maybe<Scalars['String']>;
  /** Connection between the tag type and the post type */
  posts?: Maybe<GQLTagToPostConnection>;
  /** An alphanumeric identifier for the object unique to its type. */
  slug?: Maybe<Scalars['String']>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of databaseId
   */
  tagId?: Maybe<Scalars['Int']>;
  /** Connection between the tag type and the Taxonomy type */
  taxonomy?: Maybe<GQLTagToTaxonomyConnectionEdge>;
  /** The ID of the term group that this term object belongs to */
  termGroupId?: Maybe<Scalars['Int']>;
  /** The taxonomy ID that the object is associated with */
  termTaxonomyId?: Maybe<Scalars['Int']>;
  /** The unique resource identifier path */
  uri: Scalars['String'];
};


/** The tag type */
export type GQLTagcontentNodesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLTagToContentNodeConnectionWhereArgs>;
};


/** The tag type */
export type GQLTagenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The tag type */
export type GQLTagenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The tag type */
export type GQLTagpostsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLTagToPostConnectionWhereArgs>;
};

/** Arguments for filtering the TagToContentNodeConnection connection */
export type GQLTagToContentNodeConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the tag type and the ContentNode type */
export type GQLTagToContentNodeConnection = {
  /** Edges for the TagToContentNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLTagToContentNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLTagToContentNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** Arguments for filtering the TagToPostConnection connection */
export type GQLTagToPostConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Category ID */
  categoryId?: Maybe<Scalars['Int']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Use Category Slug */
  categoryName?: Maybe<Scalars['String']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Tag Slug */
  tag?: Maybe<Scalars['String']>;
  /** Use Tag ID */
  tagId?: Maybe<Scalars['String']>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag slugs, used to display objects from one tag OR another */
  tagSlugAnd?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of tag slugs, used to exclude objects in specified tags */
  tagSlugIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the tag type and the post type */
export type GQLTagToPostConnection = {
  /** Edges for the TagToPostConnection connection */
  edges?: Maybe<Array<Maybe<GQLTagToPostConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPost>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLTagToPostConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPost>;
};

/** Connection between the tag type and the Taxonomy type */
export type GQLTagToTaxonomyConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLTaxonomy>;
};

/** Arguments for filtering the PostToTermNodeConnection connection */
export type GQLPostToTermNodeConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** The Taxonomy to filter terms by */
  taxonomies?: Maybe<Array<Maybe<GQLTaxonomyEnum>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Allowed taxonomies */
export enum GQLTaxonomyEnum {
  CATEGORY = 'CATEGORY',
  POSTFORMAT = 'POSTFORMAT',
  TAG = 'TAG'
}

/** Connection between the post type and the TermNode type */
export type GQLPostToTermNodeConnection = {
  /** Edges for the PostToTermNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLPostToTermNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTermNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLPostToTermNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTermNode>;
};

/** Arguments for filtering the UserToContentRevisionUnionConnection connection */
export type GQLUserToContentRevisionUnionConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the User type and the ContentRevisionUnion type */
export type GQLUserToContentRevisionUnionConnection = {
  /** Edges for the UserToContentRevisionUnionConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToContentRevisionUnionConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentRevisionUnion>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToContentRevisionUnionConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentRevisionUnion>;
};

export type GQLContentRevisionUnion = GQLPost | GQLPage;

/** Connection between the User type and the UserRole type */
export type GQLUserToUserRoleConnection = {
  /** Edges for the UserToUserRoleConnection connection */
  edges?: Maybe<Array<Maybe<GQLUserToUserRoleConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLUserRole>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLUserToUserRoleConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLUserRole>;
};

/** A user role object */
export type GQLUserRole = GQLNode & {
  /** The capabilities that belong to this role */
  capabilities?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** The display name of the role */
  displayName?: Maybe<Scalars['String']>;
  /** The globally unique identifier for the user role object. */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The registered name of the role */
  name?: Maybe<Scalars['String']>;
};

/** Connection between the category type and the category type */
export type GQLCategoryToParentCategoryConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLCategory>;
};

/** Arguments for filtering the CategoryToPostConnection connection */
export type GQLCategoryToPostConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Category ID */
  categoryId?: Maybe<Scalars['Int']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Use Category Slug */
  categoryName?: Maybe<Scalars['String']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Tag Slug */
  tag?: Maybe<Scalars['String']>;
  /** Use Tag ID */
  tagId?: Maybe<Scalars['String']>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag slugs, used to display objects from one tag OR another */
  tagSlugAnd?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of tag slugs, used to exclude objects in specified tags */
  tagSlugIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the category type and the post type */
export type GQLCategoryToPostConnection = {
  /** Edges for the CategoryToPostConnection connection */
  edges?: Maybe<Array<Maybe<GQLCategoryToPostConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPost>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLCategoryToPostConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPost>;
};

/** Connection between the category type and the Taxonomy type */
export type GQLCategoryToTaxonomyConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLTaxonomy>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLCategoryIdType {
  /** The Database ID for the node */
  DATABASE_ID = 'DATABASE_ID',
  /** The hashed Global ID */
  ID = 'ID',
  /** The name of the node */
  NAME = 'NAME',
  /** Url friendly name of the node */
  SLUG = 'SLUG',
  /** The URI for the node */
  URI = 'URI'
}

/** Arguments for filtering the RootQueryToCommentConnection connection */
export type GQLRootQueryToCommentConnectionWhereArgs = {
  /** Comment author email address. */
  authorEmail?: Maybe<Scalars['String']>;
  /** Array of author IDs to include comments for. */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to exclude comments for. */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Comment author URL. */
  authorUrl?: Maybe<Scalars['String']>;
  /** Array of comment IDs to include. */
  commentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of IDs of users whose unapproved comments will be returned by the query regardless of status. */
  commentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Include comments of a given type. */
  commentType?: Maybe<Scalars['String']>;
  /** Include comments from a given array of comment types. */
  commentTypeIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Exclude comments from a given array of comment types. */
  commentTypeNotIn?: Maybe<Scalars['String']>;
  /** Content object author ID to limit results by. */
  contentAuthor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs to retrieve comments for. */
  contentAuthorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of author IDs *not* to retrieve comments for. */
  contentAuthorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Limit results to those affiliated with a given content object ID. */
  contentId?: Maybe<Scalars['ID']>;
  /** Array of content object IDs to include affiliated comments for. */
  contentIdIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of content object IDs to exclude affiliated comments for. */
  contentIdNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Content object name to retrieve affiliated comments for. */
  contentName?: Maybe<Scalars['String']>;
  /** Content Object parent ID to retrieve affiliated comments for. */
  contentParent?: Maybe<Scalars['Int']>;
  /** Array of content object statuses to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentStatus?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  /** Content object type or array of types to retrieve affiliated comments for. Pass 'any' to match any value. */
  contentType?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of IDs or email addresses of users whose unapproved comments will be returned by the query regardless of $status. Default empty */
  includeUnapproved?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Karma score to retrieve matching comments for. */
  karma?: Maybe<Scalars['Int']>;
  /** The cardinality of the order of the connection */
  order?: Maybe<GQLOrderEnum>;
  /** Field to order the comments by. */
  orderby?: Maybe<GQLCommentsConnectionOrderbyEnum>;
  /** Parent ID of comment to retrieve children of. */
  parent?: Maybe<Scalars['Int']>;
  /** Array of parent IDs of comments to retrieve children for. */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of parent IDs of comments *not* to retrieve children for. */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Search term(s) to retrieve matching comments for. */
  search?: Maybe<Scalars['String']>;
  /** Comment status to limit results by. */
  status?: Maybe<Scalars['String']>;
  /** Include comments for a specific user ID. */
  userId?: Maybe<Scalars['ID']>;
};

/** Connection between the RootQuery type and the Comment type */
export type GQLRootQueryToCommentConnection = {
  /** Edges for the RootQueryToCommentConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToCommentConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLComment>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToCommentConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLComment>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLContentNodeIdTypeEnum {
  /** Identify a resource by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a resource by the (hashed) Global ID. */
  ID = 'ID',
  /** Identify a resource by the URI. */
  URI = 'URI'
}

/** Arguments for filtering the RootQueryToContentNodeConnection connection */
export type GQLRootQueryToContentNodeConnectionWhereArgs = {
  /** The Types of content to filter */
  contentTypes?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the ContentNode type */
export type GQLRootQueryToContentNodeConnection = {
  /** Edges for the RootQueryToContentNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToContentNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToContentNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentNode>;
};

/** The Type of Identifier used to fetch a single Content Type node. To be used along with the "id" field. Default is "ID". */
export enum GQLContentTypeIdTypeEnum {
  /** The globally unique ID */
  ID = 'ID',
  /** The name of the content type. */
  NAME = 'NAME'
}

/** Connection between the RootQuery type and the ContentType type */
export type GQLRootQueryToContentTypeConnection = {
  /** Edges for the RootQueryToContentTypeConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToContentTypeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentType>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToContentTypeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentType>;
};

/** The discussion setting type */
export type GQLDiscussionSettings = {
  /** Allow people to submit comments on new posts. */
  defaultCommentStatus?: Maybe<Scalars['String']>;
  /** Allow link notifications from other blogs (pingbacks and trackbacks) on new articles. */
  defaultPingStatus?: Maybe<Scalars['String']>;
};

/** The general setting type */
export type GQLGeneralSettings = {
  /** A date format for all date strings. */
  dateFormat?: Maybe<Scalars['String']>;
  /** Site tagline. */
  description?: Maybe<Scalars['String']>;
  /** This address is used for admin purposes, like new user notification. */
  email?: Maybe<Scalars['String']>;
  /** WordPress locale code. */
  language?: Maybe<Scalars['String']>;
  /** A day number of the week that the week should start on. */
  startOfWeek?: Maybe<Scalars['Int']>;
  /** A time format for all time strings. */
  timeFormat?: Maybe<Scalars['String']>;
  /** A city in the same timezone as you. */
  timezone?: Maybe<Scalars['String']>;
  /** Site title. */
  title?: Maybe<Scalars['String']>;
  /** Site URL. */
  url?: Maybe<Scalars['String']>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLMediaItemIdType {
  /** Identify a resource by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a resource by the (hashed) Global ID. */
  ID = 'ID',
  /** Identify a resource by the URI. */
  URI = 'URI',
  /** Identify a resource by the slug. Available to non-hierarchcial Types where the slug is a unique identifier. */
  SLUG = 'SLUG',
  /** Identify a media item by its source url */
  SOURCE_URL = 'SOURCE_URL'
}

/** Arguments for filtering the RootQueryToMediaItemConnection connection */
export type GQLRootQueryToMediaItemConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the mediaItem type */
export type GQLRootQueryToMediaItemConnection = {
  /** Edges for the RootQueryToMediaItemConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToMediaItemConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLMediaItem>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToMediaItemConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLMediaItem>;
};

/** The Type of Identifier used to fetch a single node. Default is "ID". To be used along with the "id" field. */
export enum GQLMenuNodeIdTypeEnum {
  /** Identify a menu node by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a menu node by the (hashed) Global ID. */
  ID = 'ID',
  /** Identify a menu node by it's name */
  NAME = 'NAME'
}

/** Menus are the containers for navigation items. Menus can be assigned to menu locations, which are typically registered by the active theme. */
export type GQLMenu = GQLNode & GQLDatabaseIdentifier & {
  /** The number of items in the menu */
  count?: Maybe<Scalars['Int']>;
  /** The unique identifier stored in the database */
  databaseId: Scalars['Int'];
  /** The globally unique identifier of the nav menu object. */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  locations?: Maybe<Array<Maybe<GQLMenuLocationEnum>>>;
  /**
   * WP ID of the nav menu.
   * @deprecated Deprecated in favor of the databaseId field
   */
  menuId?: Maybe<Scalars['Int']>;
  /** Connection between the Menu type and the MenuItem type */
  menuItems?: Maybe<GQLMenuToMenuItemConnection>;
  /** Display name of the menu. Equivalent to WP_Term-&gt;name. */
  name?: Maybe<Scalars['String']>;
  /** The url friendly name of the menu. Equivalent to WP_Term-&gt;slug */
  slug?: Maybe<Scalars['String']>;
};


/** Menus are the containers for navigation items. Menus can be assigned to menu locations, which are typically registered by the active theme. */
export type GQLMenumenuItemsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLMenuToMenuItemConnectionWhereArgs>;
};

/** Registered menu locations */
export enum GQLMenuLocationEnum {
  FOOTER = 'FOOTER',
  PRIMARY = 'PRIMARY',
  TOP_NAV = 'TOP_NAV'
}

/** Arguments for filtering the MenuToMenuItemConnection connection */
export type GQLMenuToMenuItemConnectionWhereArgs = {
  /** The ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** The menu location for the menu being queried */
  location?: Maybe<GQLMenuLocationEnum>;
  /** The database ID of the parent menu object */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The ID of the parent menu object */
  parentId?: Maybe<Scalars['ID']>;
};

/** Connection between the Menu type and the MenuItem type */
export type GQLMenuToMenuItemConnection = {
  /** Edges for the MenuToMenuItemConnection connection */
  edges?: Maybe<Array<Maybe<GQLMenuToMenuItemConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLMenuItem>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLMenuToMenuItemConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLMenuItem>;
};

/** Navigation menu items are the individual items assigned to a menu. These are rendered as the links in a navigation menu. */
export type GQLMenuItem = GQLNode & GQLDatabaseIdentifier & {
  /** Connection between the MenuItem type and the MenuItem type */
  childItems?: Maybe<GQLMenuItemToMenuItemConnection>;
  /** Connection from MenuItem to it&#039;s connected node */
  connectedNode?: Maybe<GQLMenuItemToMenuItemLinkableConnectionEdge>;
  /**
   * The object connected to this menu item.
   * @deprecated Deprecated in favor of the connectedNode field
   */
  connectedObject?: Maybe<GQLMenuItemObjectUnion>;
  /** Class attribute for the menu item link */
  cssClasses?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** The unique identifier stored in the database */
  databaseId: Scalars['Int'];
  /** Description of the menu item. */
  description?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the nav menu item object. */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** Label or title of the menu item. */
  label?: Maybe<Scalars['String']>;
  /** Link relationship (XFN) of the menu item. */
  linkRelationship?: Maybe<Scalars['String']>;
  locations?: Maybe<Array<Maybe<GQLMenuLocationEnum>>>;
  /** The Menu a MenuItem is part of */
  menu?: Maybe<GQLMenuItemToMenuConnectionEdge>;
  /**
   * WP ID of the menu item.
   * @deprecated Deprecated in favor of the databaseId field
   */
  menuItemId?: Maybe<Scalars['Int']>;
  /** Menu item order */
  order?: Maybe<Scalars['Int']>;
  /** The database id of the parent menu item or null if it is the root */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The globally unique identifier of the parent nav menu item object. */
  parentId?: Maybe<Scalars['ID']>;
  /** Path for the resource. Relative path for internal resources. Absolute path for external resources. */
  path: Scalars['String'];
  /** Target attribute for the menu item link. */
  target?: Maybe<Scalars['String']>;
  /** Title attribute for the menu item link */
  title?: Maybe<Scalars['String']>;
  /** URL or destination of the menu item. */
  url?: Maybe<Scalars['String']>;
};


/** Navigation menu items are the individual items assigned to a menu. These are rendered as the links in a navigation menu. */
export type GQLMenuItemchildItemsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  where?: Maybe<GQLMenuItemToMenuItemConnectionWhereArgs>;
};

/** Arguments for filtering the MenuItemToMenuItemConnection connection */
export type GQLMenuItemToMenuItemConnectionWhereArgs = {
  /** The ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** The menu location for the menu being queried */
  location?: Maybe<GQLMenuLocationEnum>;
  /** The database ID of the parent menu object */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The ID of the parent menu object */
  parentId?: Maybe<Scalars['ID']>;
};

/** Connection between the MenuItem type and the MenuItem type */
export type GQLMenuItemToMenuItemConnection = {
  /** Edges for the MenuItemToMenuItemConnection connection */
  edges?: Maybe<Array<Maybe<GQLMenuItemToMenuItemConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLMenuItem>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLMenuItemToMenuItemConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLMenuItem>;
};

/** Connection between the MenuItem type and the MenuItemLinkable type */
export type GQLMenuItemToMenuItemLinkableConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLMenuItemLinkable>;
};

/** Deprecated in favor of MenuItemLinkeable Interface */
export type GQLMenuItemObjectUnion = GQLPost | GQLPage | GQLSocialMediaIcon | GQLCategory | GQLTag | GQLPostFormat;

/** The socialMediaIcon type */
export type GQLSocialMediaIcon = GQLNode & GQLContentNode & GQLDatabaseIdentifier & GQLNodeWithTemplate & GQLUniformResourceIdentifiable & GQLNodeWithTitle & GQLNodeWithFeaturedImage & GQLMenuItemLinkable & {
  /** Connection between the ContentNode type and the ContentType type */
  contentType?: Maybe<GQLContentNodeToContentTypeConnectionEdge>;
  /** The ID of the node in the database. */
  databaseId: Scalars['Int'];
  /** Post publishing date. */
  date?: Maybe<Scalars['String']>;
  /** The publishing date set in GMT. */
  dateGmt?: Maybe<Scalars['String']>;
  /** The desired slug of the post */
  desiredSlug?: Maybe<Scalars['String']>;
  /** If a user has edited the node within the past 15 seconds, this will return the user that last edited. Null if the edit lock doesn&#039;t exist or is greater than 15 seconds */
  editingLockedBy?: Maybe<GQLContentNodeToEditLockConnectionEdge>;
  /** The RSS enclosure for the object */
  enclosure?: Maybe<Scalars['String']>;
  /** Connection between the ContentNode type and the EnqueuedScript type */
  enqueuedScripts?: Maybe<GQLContentNodeToEnqueuedScriptConnection>;
  /** Connection between the ContentNode type and the EnqueuedStylesheet type */
  enqueuedStylesheets?: Maybe<GQLContentNodeToEnqueuedStylesheetConnection>;
  /** Connection between the NodeWithFeaturedImage type and the MediaItem type */
  featuredImage?: Maybe<GQLNodeWithFeaturedImageToMediaItemConnectionEdge>;
  /** The database identifier for the featured image node assigned to the content node */
  featuredImageDatabaseId?: Maybe<Scalars['Int']>;
  /** Globally unique ID of the featured image assigned to the node */
  featuredImageId?: Maybe<Scalars['ID']>;
  /** The global unique identifier for this post. This currently matches the value stored in WP_Post-&gt;guid and the guid column in the &quot;post_objects&quot; database table. */
  guid?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the social-media object. */
  id: Scalars['ID'];
  /** Whether the object is a node in the preview state */
  isPreview?: Maybe<Scalars['Boolean']>;
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The user that most recently edited the node */
  lastEditedBy?: Maybe<GQLContentNodeToEditLastConnectionEdge>;
  /** The permalink of the post */
  link?: Maybe<Scalars['String']>;
  /** The local modified time for a post. If a post was recently updated the modified field will change to match the corresponding time. */
  modified?: Maybe<Scalars['String']>;
  /** The GMT modified time for a post. If a post was recently updated the modified field will change to match the corresponding time in GMT. */
  modifiedGmt?: Maybe<Scalars['String']>;
  /** Connection between the socialMediaIcon type and the socialMediaIcon type */
  preview?: Maybe<GQLSocialMediaIconToPreviewConnectionEdge>;
  /** The database id of the preview node */
  previewRevisionDatabaseId?: Maybe<Scalars['Int']>;
  /** Whether the object is a node in the preview state */
  previewRevisionId?: Maybe<Scalars['ID']>;
  /** The uri slug for the post. This is equivalent to the WP_Post-&gt;post_name field and the post_name column in the database for the &quot;post_objects&quot; table. */
  slug?: Maybe<Scalars['String']>;
  /**
   * The id field matches the WP_Post-&gt;ID field.
   * @deprecated Deprecated in favor of the databaseId field
   */
  socialMediaIconId: Scalars['Int'];
  socialMediaLink?: Maybe<GQLSocialMediaIcon_Socialmedialink>;
  /** The current status of the object */
  status?: Maybe<Scalars['String']>;
  /** The template assigned to a node of content */
  template?: Maybe<GQLContentTemplate>;
  /** The title of the post. This is currently just the raw title. An amendment to support rendered title needs to be made. */
  title?: Maybe<Scalars['String']>;
  /** URI path for the resource */
  uri: Scalars['String'];
};


/** The socialMediaIcon type */
export type GQLSocialMediaIconenqueuedScriptsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The socialMediaIcon type */
export type GQLSocialMediaIconenqueuedStylesheetsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
};


/** The socialMediaIcon type */
export type GQLSocialMediaIcontitleArgs = {
  format?: Maybe<GQLPostObjectFieldFormatEnum>;
};

/** Connection between the socialMediaIcon type and the socialMediaIcon type */
export type GQLSocialMediaIconToPreviewConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLSocialMediaIcon>;
};

/** Field Group */
export type GQLSocialMediaIcon_Socialmedialink = {
  fieldGroupName?: Maybe<Scalars['String']>;
  socialMediaLink?: Maybe<Scalars['String']>;
};

/** Connection between the MenuItem type and the Menu type */
export type GQLMenuItemToMenuConnectionEdge = {
  /** The nodes of the connection, without the edges */
  node?: Maybe<GQLMenu>;
};

/** The Type of Identifier used to fetch a single node. Default is "ID". To be used along with the "id" field. */
export enum GQLMenuItemNodeIdTypeEnum {
  /** Identify a resource by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a resource by the (hashed) Global ID. */
  ID = 'ID'
}

/** Arguments for filtering the RootQueryToMenuItemConnection connection */
export type GQLRootQueryToMenuItemConnectionWhereArgs = {
  /** The ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** The menu location for the menu being queried */
  location?: Maybe<GQLMenuLocationEnum>;
  /** The database ID of the parent menu object */
  parentDatabaseId?: Maybe<Scalars['Int']>;
  /** The ID of the parent menu object */
  parentId?: Maybe<Scalars['ID']>;
};

/** Connection between the RootQuery type and the MenuItem type */
export type GQLRootQueryToMenuItemConnection = {
  /** Edges for the RootQueryToMenuItemConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToMenuItemConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLMenuItem>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToMenuItemConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLMenuItem>;
};

/** Arguments for filtering the RootQueryToMenuConnection connection */
export type GQLRootQueryToMenuConnectionWhereArgs = {
  /** The ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** The menu location for the menu being queried */
  location?: Maybe<GQLMenuLocationEnum>;
  /** The slug of the menu to query items for */
  slug?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the Menu type */
export type GQLRootQueryToMenuConnection = {
  /** Edges for the RootQueryToMenuConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToMenuConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLMenu>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToMenuConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLMenu>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLPageIdType {
  /** Identify a resource by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a resource by the (hashed) Global ID. */
  ID = 'ID',
  /** Identify a resource by the URI. */
  URI = 'URI'
}

/** Arguments for filtering the RootQueryToPageConnection connection */
export type GQLRootQueryToPageConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the page type */
export type GQLRootQueryToPageConnection = {
  /** Edges for the RootQueryToPageConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToPageConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPage>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToPageConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPage>;
};

/** An plugin object */
export type GQLPlugin = GQLNode & {
  /** Name of the plugin author(s), may also be a company name. */
  author?: Maybe<Scalars['String']>;
  /** URI for the related author(s)/company website. */
  authorUri?: Maybe<Scalars['String']>;
  /** Description of the plugin. */
  description?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the plugin object. */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** Display name of the plugin. */
  name?: Maybe<Scalars['String']>;
  /** Plugin path. */
  path?: Maybe<Scalars['String']>;
  /** URI for the plugin website. This is useful for directing users for support requests etc. */
  pluginUri?: Maybe<Scalars['String']>;
  /** Current version of the plugin. */
  version?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the Plugin type */
export type GQLRootQueryToPluginConnection = {
  /** Edges for the RootQueryToPluginConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToPluginConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPlugin>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToPluginConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPlugin>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLPostIdType {
  /** Identify a resource by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a resource by the (hashed) Global ID. */
  ID = 'ID',
  /** Identify a resource by the URI. */
  URI = 'URI',
  /** Identify a resource by the slug. Available to non-hierarchcial Types where the slug is a unique identifier. */
  SLUG = 'SLUG'
}

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLPostFormatIdType {
  /** The Database ID for the node */
  DATABASE_ID = 'DATABASE_ID',
  /** The hashed Global ID */
  ID = 'ID',
  /** The name of the node */
  NAME = 'NAME',
  /** Url friendly name of the node */
  SLUG = 'SLUG',
  /** The URI for the node */
  URI = 'URI'
}

/** Arguments for filtering the RootQueryToPostFormatConnection connection */
export type GQLRootQueryToPostFormatConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the RootQuery type and the postFormat type */
export type GQLRootQueryToPostFormatConnection = {
  /** Edges for the RootQueryToPostFormatConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToPostFormatConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPostFormat>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToPostFormatConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPostFormat>;
};

/** Arguments for filtering the RootQueryToPostConnection connection */
export type GQLRootQueryToPostConnectionWhereArgs = {
  /** The user that's connected as the author of the object. Use the userId for the author object. */
  author?: Maybe<Scalars['Int']>;
  /** Find objects connected to author(s) in the array of author's userIds */
  authorIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Find objects connected to the author by the author's nicename */
  authorName?: Maybe<Scalars['String']>;
  /** Find objects NOT connected to author(s) in the array of author's userIds */
  authorNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Category ID */
  categoryId?: Maybe<Scalars['Int']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Use Category Slug */
  categoryName?: Maybe<Scalars['String']>;
  /** Array of category IDs, used to display objects from one category OR another */
  categoryNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Tag Slug */
  tag?: Maybe<Scalars['String']>;
  /** Use Tag ID */
  tagId?: Maybe<Scalars['String']>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag IDs, used to display objects from one tag OR another */
  tagNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of tag slugs, used to display objects from one tag OR another */
  tagSlugAnd?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of tag slugs, used to exclude objects in specified tags */
  tagSlugIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the post type */
export type GQLRootQueryToPostConnection = {
  /** Edges for the RootQueryToPostConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToPostConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLPost>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToPostConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLPost>;
};

/** The reading setting type */
export type GQLReadingSettings = {
  /** Blog pages show at most. */
  postsPerPage?: Maybe<Scalars['Int']>;
};

/** Connection between the RootQuery type and the EnqueuedScript type */
export type GQLRootQueryToEnqueuedScriptConnection = {
  /** Edges for the RootQueryToEnqueuedScriptConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToEnqueuedScriptConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedScript>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToEnqueuedScriptConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedScript>;
};

/** Connection between the RootQuery type and the EnqueuedStylesheet type */
export type GQLRootQueryToEnqueuedStylesheetConnection = {
  /** Edges for the RootQueryToEnqueuedStylesheetConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToEnqueuedStylesheetConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLEnqueuedStylesheet>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToEnqueuedStylesheetConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLEnqueuedStylesheet>;
};

/** Arguments for filtering the RootQueryToContentRevisionUnionConnection connection */
export type GQLRootQueryToContentRevisionUnionConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the ContentRevisionUnion type */
export type GQLRootQueryToContentRevisionUnionConnection = {
  /** Edges for the RootQueryToContentRevisionUnionConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToContentRevisionUnionConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLContentRevisionUnion>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToContentRevisionUnionConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLContentRevisionUnion>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLSocialMediaIconIdType {
  /** Identify a resource by the Database ID. */
  DATABASE_ID = 'DATABASE_ID',
  /** Identify a resource by the (hashed) Global ID. */
  ID = 'ID',
  /** Identify a resource by the URI. */
  URI = 'URI',
  /** Identify a resource by the slug. Available to non-hierarchcial Types where the slug is a unique identifier. */
  SLUG = 'SLUG'
}

/** Arguments for filtering the RootQueryToSocialMediaIconConnection connection */
export type GQLRootQueryToSocialMediaIconConnectionWhereArgs = {
  /** Filter the connection based on dates */
  dateQuery?: Maybe<GQLDateQueryInput>;
  /** True for objects with passwords; False for objects without passwords; null for all objects with or without passwords */
  hasPassword?: Maybe<Scalars['Boolean']>;
  /** Specific ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** Array of IDs for the objects to retrieve */
  in?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Get objects with a specific mimeType property */
  mimeType?: Maybe<GQLMimeTypeEnum>;
  /** Slug / post_name of the object */
  name?: Maybe<Scalars['String']>;
  /** Specify objects to retrieve. Use slugs */
  nameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Specify IDs NOT to retrieve. If this is used in the same query as "in", it will be ignored */
  notIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLPostObjectsConnectionOrderbyInput>>>;
  /** Use ID to return only children. Use 0 to return only top-level items */
  parent?: Maybe<Scalars['ID']>;
  /** Specify objects whose parent is in an array */
  parentIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Specify posts whose parent is not in an array */
  parentNotIn?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Show posts with a specific password. */
  password?: Maybe<Scalars['String']>;
  /** Show Posts based on a keyword search */
  search?: Maybe<Scalars['String']>;
  stati?: Maybe<Array<Maybe<GQLPostStatusEnum>>>;
  status?: Maybe<GQLPostStatusEnum>;
  /** Title of the object */
  title?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the socialMediaIcon type */
export type GQLRootQueryToSocialMediaIconConnection = {
  /** Edges for the RootQueryToSocialMediaIconConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToSocialMediaIconConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLSocialMediaIcon>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToSocialMediaIconConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLSocialMediaIcon>;
};

/** The Type of Identifier used to fetch a single resource. Default is ID. */
export enum GQLTagIdType {
  /** The Database ID for the node */
  DATABASE_ID = 'DATABASE_ID',
  /** The hashed Global ID */
  ID = 'ID',
  /** The name of the node */
  NAME = 'NAME',
  /** Url friendly name of the node */
  SLUG = 'SLUG',
  /** The URI for the node */
  URI = 'URI'
}

/** Arguments for filtering the RootQueryToTagConnection connection */
export type GQLRootQueryToTagConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the RootQuery type and the tag type */
export type GQLRootQueryToTagConnection = {
  /** Edges for the RootQueryToTagConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToTagConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTag>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToTagConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTag>;
};

/** Connection between the RootQuery type and the Taxonomy type */
export type GQLRootQueryToTaxonomyConnection = {
  /** Edges for the RootQueryToTaxonomyConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToTaxonomyConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTaxonomy>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToTaxonomyConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTaxonomy>;
};

/** The Type of Identifier used to fetch a single Taxonomy node. To be used along with the "id" field. Default is "ID". */
export enum GQLTaxonomyIdTypeEnum {
  /** The globally unique ID */
  ID = 'ID',
  /** The name of the taxonomy */
  NAME = 'NAME'
}

/** The Type of Identifier used to fetch a single resource. Default is "ID". To be used along with the "id" field. */
export enum GQLTermNodeIdTypeEnum {
  /** The Database ID for the node */
  DATABASE_ID = 'DATABASE_ID',
  /** The hashed Global ID */
  ID = 'ID',
  /** The name of the node */
  NAME = 'NAME',
  /** Url friendly name of the node */
  SLUG = 'SLUG',
  /** The URI for the node */
  URI = 'URI'
}

/** Arguments for filtering the RootQueryToTermNodeConnection connection */
export type GQLRootQueryToTermNodeConnectionWhereArgs = {
  /** Unique cache key to be produced when this query is stored in an object cache. Default is 'core'. */
  cacheDomain?: Maybe<Scalars['String']>;
  /** Term ID to retrieve child terms of. If multiple taxonomies are passed, $child_of is ignored. Default 0. */
  childOf?: Maybe<Scalars['Int']>;
  /** True to limit results to terms that have no children. This parameter has no effect on non-hierarchical taxonomies. Default false. */
  childless?: Maybe<Scalars['Boolean']>;
  /** Retrieve terms where the description is LIKE the input value. Default empty. */
  descriptionLike?: Maybe<Scalars['String']>;
  /** Array of term ids to exclude. If $include is non-empty, $exclude is ignored. Default empty array. */
  exclude?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of term ids to exclude along with all of their descendant terms. If $include is non-empty, $exclude_tree is ignored. Default empty array. */
  excludeTree?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to hide terms not assigned to any posts. Accepts true or false. Default false */
  hideEmpty?: Maybe<Scalars['Boolean']>;
  /** Whether to include terms that have non-empty descendants (even if $hide_empty is set to true). Default true. */
  hierarchical?: Maybe<Scalars['Boolean']>;
  /** Array of term ids to include. Default empty array. */
  include?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Array of names to return term(s) for. Default empty. */
  name?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Retrieve terms where the name is LIKE the input value. Default empty. */
  nameLike?: Maybe<Scalars['String']>;
  /** Array of object IDs. Results will be limited to terms associated with these objects. */
  objectIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Field(s) to order terms by. Defaults to 'name'. */
  orderby?: Maybe<GQLTermObjectsConnectionOrderbyEnum>;
  /** Whether to pad the quantity of a term's children in the quantity of each term's "count" object variable. Default false. */
  padCounts?: Maybe<Scalars['Boolean']>;
  /** Parent term ID to retrieve direct-child terms of. Default empty. */
  parent?: Maybe<Scalars['Int']>;
  /** Search criteria to match terms. Will be SQL-formatted with wildcards before and after. Default empty. */
  search?: Maybe<Scalars['String']>;
  /** Array of slugs to return term(s) for. Default empty. */
  slug?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** The Taxonomy to filter terms by */
  taxonomies?: Maybe<Array<Maybe<GQLTaxonomyEnum>>>;
  /** Array of term taxonomy IDs, to match when querying terms. */
  termTaxonomId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Whether to prime meta caches for matched terms. Default true. */
  updateTermMetaCache?: Maybe<Scalars['Boolean']>;
};

/** Connection between the RootQuery type and the TermNode type */
export type GQLRootQueryToTermNodeConnection = {
  /** Edges for the RootQueryToTermNodeConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToTermNodeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTermNode>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToTermNodeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTermNode>;
};

/** A theme object */
export type GQLTheme = GQLNode & {
  /** Name of the theme author(s), could also be a company name. This field is equivalent to WP_Theme-&gt;get( &quot;Author&quot; ). */
  author?: Maybe<Scalars['String']>;
  /** URI for the author/company website. This field is equivalent to WP_Theme-&gt;get( &quot;AuthorURI&quot; ). */
  authorUri?: Maybe<Scalars['String']>;
  /** The description of the theme. This field is equivalent to WP_Theme-&gt;get( &quot;Description&quot; ). */
  description?: Maybe<Scalars['String']>;
  /** The globally unique identifier of the theme object. */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** Display name of the theme. This field is equivalent to WP_Theme-&gt;get( &quot;Name&quot; ). */
  name?: Maybe<Scalars['String']>;
  /** The URL of the screenshot for the theme. The screenshot is intended to give an overview of what the theme looks like. This field is equivalent to WP_Theme-&gt;get_screenshot(). */
  screenshot?: Maybe<Scalars['String']>;
  /** The theme slug is used to internally match themes. Theme slugs can have subdirectories like: my-theme/sub-theme. This field is equivalent to WP_Theme-&gt;get_stylesheet(). */
  slug?: Maybe<Scalars['String']>;
  /** URI for the author/company website. This field is equivalent to WP_Theme-&gt;get( &quot;Tags&quot; ). */
  tags?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** A URI if the theme has a website associated with it. The Theme URI is handy for directing users to a theme site for support etc. This field is equivalent to WP_Theme-&gt;get( &quot;ThemeURI&quot; ). */
  themeUri?: Maybe<Scalars['String']>;
  /** The current version of the theme. This field is equivalent to WP_Theme-&gt;get( &quot;Version&quot; ). */
  version?: Maybe<Scalars['String']>;
};

/** Connection between the RootQuery type and the Theme type */
export type GQLRootQueryToThemeConnection = {
  /** Edges for the RootQueryToThemeConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToThemeConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLTheme>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToThemeConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLTheme>;
};

/** The Type of Identifier used to fetch a single User node. To be used along with the "id" field. Default is "ID". */
export enum GQLUserNodeIdTypeEnum {
  /** The Database ID for the node */
  DATABASE_ID = 'DATABASE_ID',
  /** The Email of the User */
  EMAIL = 'EMAIL',
  /** The hashed Global ID */
  ID = 'ID',
  /** The slug of the User */
  SLUG = 'SLUG',
  /** The URI for the node */
  URI = 'URI',
  /** The username the User uses to login with */
  USERNAME = 'USERNAME'
}

/** Connection between the RootQuery type and the UserRole type */
export type GQLRootQueryToUserRoleConnection = {
  /** Edges for the RootQueryToUserRoleConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToUserRoleConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLUserRole>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToUserRoleConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLUserRole>;
};

/** Arguments for filtering the RootQueryToUserConnection connection */
export type GQLRootQueryToUserConnectionWhereArgs = {
  /** Array of userIds to exclude. */
  exclude?: Maybe<Array<Maybe<Scalars['Int']>>>;
  /** Pass an array of post types to filter results to users who have published posts in those post types. */
  hasPublishedPosts?: Maybe<Array<Maybe<GQLContentTypeEnum>>>;
  /** Array of userIds to include. */
  include?: Maybe<Array<Maybe<Scalars['Int']>>>;
  /** The user login. */
  login?: Maybe<Scalars['String']>;
  /** An array of logins to include. Users matching one of these logins will be included in results. */
  loginIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** An array of logins to exclude. Users matching one of these logins will not be included in results. */
  loginNotIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** The user nicename. */
  nicename?: Maybe<Scalars['String']>;
  /** An array of nicenames to include. Users matching one of these nicenames will be included in results. */
  nicenameIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** An array of nicenames to exclude. Users matching one of these nicenames will not be included in results. */
  nicenameNotIn?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** What paramater to use to order the objects by. */
  orderby?: Maybe<Array<Maybe<GQLUsersConnectionOrderbyInput>>>;
  /** An array of role names that users must match to be included in results. Note that this is an inclusive list: users must match *each* role. */
  role?: Maybe<GQLUserRoleEnum>;
  /** An array of role names. Matched users must have at least one of these roles. */
  roleIn?: Maybe<Array<Maybe<GQLUserRoleEnum>>>;
  /** An array of role names to exclude. Users matching one or more of these roles will not be included in results. */
  roleNotIn?: Maybe<Array<Maybe<GQLUserRoleEnum>>>;
  /** Search keyword. Searches for possible string matches on columns. When "searchColumns" is left empty, it tries to determine which column to search in based on search string. */
  search?: Maybe<Scalars['String']>;
  /** Array of column names to be searched. Accepts 'ID', 'login', 'nicename', 'email', 'url'. */
  searchColumns?: Maybe<Array<Maybe<GQLUsersConnectionSearchColumnEnum>>>;
};

/** Options for ordering the connection */
export type GQLUsersConnectionOrderbyInput = {
  field: GQLUsersConnectionOrderbyEnum;
  order?: Maybe<GQLOrderEnum>;
};

/** Field to order the connection by */
export enum GQLUsersConnectionOrderbyEnum {
  /** Order by display name */
  DISPLAY_NAME = 'DISPLAY_NAME',
  /** Order by email address */
  EMAIL = 'EMAIL',
  /** Order by login */
  LOGIN = 'LOGIN',
  /** Preserve the login order given in the LOGIN_IN array */
  LOGIN_IN = 'LOGIN_IN',
  /** Order by nice name */
  NICE_NAME = 'NICE_NAME',
  /** Preserve the nice name order given in the NICE_NAME_IN array */
  NICE_NAME_IN = 'NICE_NAME_IN',
  /** Order by registration date */
  REGISTERED = 'REGISTERED',
  /** Order by URL */
  URL = 'URL'
}

/** Names of available user roles */
export enum GQLUserRoleEnum {
  ADMINISTRATOR = 'ADMINISTRATOR',
  AUTHOR = 'AUTHOR',
  CONTRIBUTOR = 'CONTRIBUTOR',
  EDITOR = 'EDITOR',
  SUBSCRIBER = 'SUBSCRIBER'
}

/** Names of available user roles */
export enum GQLUsersConnectionSearchColumnEnum {
  ADMINISTRATOR = 'ADMINISTRATOR',
  AUTHOR = 'AUTHOR',
  CONTRIBUTOR = 'CONTRIBUTOR',
  EDITOR = 'EDITOR',
  SUBSCRIBER = 'SUBSCRIBER'
}

/** Connection between the RootQuery type and the User type */
export type GQLRootQueryToUserConnection = {
  /** Edges for the RootQueryToUserConnection connection */
  edges?: Maybe<Array<Maybe<GQLRootQueryToUserConnectionEdge>>>;
  /** The nodes of the connection, without the edges */
  nodes?: Maybe<Array<Maybe<GQLUser>>>;
  /** Information about pagination in a connection. */
  pageInfo?: Maybe<GQLWPPageInfo>;
};

/** An edge in a connection */
export type GQLRootQueryToUserConnectionEdge = {
  /** A cursor for use in pagination */
  cursor?: Maybe<Scalars['String']>;
  /** The item at the end of the edge */
  node?: Maybe<GQLUser>;
};

/** The writing setting type */
export type GQLWritingSettings = {
  /** Default post category. */
  defaultCategory?: Maybe<Scalars['Int']>;
  /** Default post format. */
  defaultPostFormat?: Maybe<Scalars['String']>;
  /** Convert emoticons like :-) and :-P to graphics on display. */
  useSmilies?: Maybe<Scalars['Boolean']>;
};

/** The root mutation */
export type GQLRootMutation = {
  /** The payload for the UpdateCategory mutation */
  updateCategory?: Maybe<GQLUpdateCategoryPayload>;
  /** The payload for the UpdatePostFormat mutation */
  updatePostFormat?: Maybe<GQLUpdatePostFormatPayload>;
  /** The payload for the UpdateTag mutation */
  updateTag?: Maybe<GQLUpdateTagPayload>;
  /** The payload for the createCategory mutation */
  createCategory?: Maybe<GQLCreateCategoryPayload>;
  /** The payload for the createComment mutation */
  createComment?: Maybe<GQLCreateCommentPayload>;
  /** The payload for the createMediaItem mutation */
  createMediaItem?: Maybe<GQLCreateMediaItemPayload>;
  /** The payload for the createPage mutation */
  createPage?: Maybe<GQLCreatePagePayload>;
  /** The payload for the createPost mutation */
  createPost?: Maybe<GQLCreatePostPayload>;
  /** The payload for the createPostFormat mutation */
  createPostFormat?: Maybe<GQLCreatePostFormatPayload>;
  /** The payload for the createSocialMediaIcon mutation */
  createSocialMediaIcon?: Maybe<GQLCreateSocialMediaIconPayload>;
  /** The payload for the createTag mutation */
  createTag?: Maybe<GQLCreateTagPayload>;
  /** The payload for the createUser mutation */
  createUser?: Maybe<GQLCreateUserPayload>;
  /** The payload for the deleteCategory mutation */
  deleteCategory?: Maybe<GQLDeleteCategoryPayload>;
  /** The payload for the deleteComment mutation */
  deleteComment?: Maybe<GQLDeleteCommentPayload>;
  /** The payload for the deleteMediaItem mutation */
  deleteMediaItem?: Maybe<GQLDeleteMediaItemPayload>;
  /** The payload for the deletePage mutation */
  deletePage?: Maybe<GQLDeletePagePayload>;
  /** The payload for the deletePost mutation */
  deletePost?: Maybe<GQLDeletePostPayload>;
  /** The payload for the deletePostFormat mutation */
  deletePostFormat?: Maybe<GQLDeletePostFormatPayload>;
  /** The payload for the deleteSocialMediaIcon mutation */
  deleteSocialMediaIcon?: Maybe<GQLDeleteSocialMediaIconPayload>;
  /** The payload for the deleteTag mutation */
  deleteTag?: Maybe<GQLDeleteTagPayload>;
  /** The payload for the deleteUser mutation */
  deleteUser?: Maybe<GQLDeleteUserPayload>;
  increaseCount?: Maybe<Scalars['Int']>;
  /** The payload for the login mutation */
  login?: Maybe<GQLLoginPayload>;
  /** The payload for the refreshJwtAuthToken mutation */
  refreshJwtAuthToken?: Maybe<GQLRefreshJwtAuthTokenPayload>;
  /** The payload for the registerUser mutation */
  registerUser?: Maybe<GQLRegisterUserPayload>;
  /** The payload for the resetUserPassword mutation */
  resetUserPassword?: Maybe<GQLResetUserPasswordPayload>;
  /** The payload for the restoreComment mutation */
  restoreComment?: Maybe<GQLRestoreCommentPayload>;
  /** The payload for the sendPasswordResetEmail mutation */
  sendPasswordResetEmail?: Maybe<GQLSendPasswordResetEmailPayload>;
  /** The payload for the updateComment mutation */
  updateComment?: Maybe<GQLUpdateCommentPayload>;
  /** The payload for the updateMediaItem mutation */
  updateMediaItem?: Maybe<GQLUpdateMediaItemPayload>;
  /** The payload for the updatePage mutation */
  updatePage?: Maybe<GQLUpdatePagePayload>;
  /** The payload for the updatePost mutation */
  updatePost?: Maybe<GQLUpdatePostPayload>;
  /** The payload for the updateSettings mutation */
  updateSettings?: Maybe<GQLUpdateSettingsPayload>;
  /** The payload for the updateSocialMediaIcon mutation */
  updateSocialMediaIcon?: Maybe<GQLUpdateSocialMediaIconPayload>;
  /** The payload for the updateUser mutation */
  updateUser?: Maybe<GQLUpdateUserPayload>;
};


/** The root mutation */
export type GQLRootMutationupdateCategoryArgs = {
  input: GQLUpdateCategoryInput;
};


/** The root mutation */
export type GQLRootMutationupdatePostFormatArgs = {
  input: GQLUpdatePostFormatInput;
};


/** The root mutation */
export type GQLRootMutationupdateTagArgs = {
  input: GQLUpdateTagInput;
};


/** The root mutation */
export type GQLRootMutationcreateCategoryArgs = {
  input: GQLCreateCategoryInput;
};


/** The root mutation */
export type GQLRootMutationcreateCommentArgs = {
  input: GQLCreateCommentInput;
};


/** The root mutation */
export type GQLRootMutationcreateMediaItemArgs = {
  input: GQLCreateMediaItemInput;
};


/** The root mutation */
export type GQLRootMutationcreatePageArgs = {
  input: GQLCreatePageInput;
};


/** The root mutation */
export type GQLRootMutationcreatePostArgs = {
  input: GQLCreatePostInput;
};


/** The root mutation */
export type GQLRootMutationcreatePostFormatArgs = {
  input: GQLCreatePostFormatInput;
};


/** The root mutation */
export type GQLRootMutationcreateSocialMediaIconArgs = {
  input: GQLCreateSocialMediaIconInput;
};


/** The root mutation */
export type GQLRootMutationcreateTagArgs = {
  input: GQLCreateTagInput;
};


/** The root mutation */
export type GQLRootMutationcreateUserArgs = {
  input: GQLCreateUserInput;
};


/** The root mutation */
export type GQLRootMutationdeleteCategoryArgs = {
  input: GQLDeleteCategoryInput;
};


/** The root mutation */
export type GQLRootMutationdeleteCommentArgs = {
  input: GQLDeleteCommentInput;
};


/** The root mutation */
export type GQLRootMutationdeleteMediaItemArgs = {
  input: GQLDeleteMediaItemInput;
};


/** The root mutation */
export type GQLRootMutationdeletePageArgs = {
  input: GQLDeletePageInput;
};


/** The root mutation */
export type GQLRootMutationdeletePostArgs = {
  input: GQLDeletePostInput;
};


/** The root mutation */
export type GQLRootMutationdeletePostFormatArgs = {
  input: GQLDeletePostFormatInput;
};


/** The root mutation */
export type GQLRootMutationdeleteSocialMediaIconArgs = {
  input: GQLDeleteSocialMediaIconInput;
};


/** The root mutation */
export type GQLRootMutationdeleteTagArgs = {
  input: GQLDeleteTagInput;
};


/** The root mutation */
export type GQLRootMutationdeleteUserArgs = {
  input: GQLDeleteUserInput;
};


/** The root mutation */
export type GQLRootMutationincreaseCountArgs = {
  count?: Maybe<Scalars['Int']>;
};


/** The root mutation */
export type GQLRootMutationloginArgs = {
  input: GQLLoginInput;
};


/** The root mutation */
export type GQLRootMutationrefreshJwtAuthTokenArgs = {
  input: GQLRefreshJwtAuthTokenInput;
};


/** The root mutation */
export type GQLRootMutationregisterUserArgs = {
  input: GQLRegisterUserInput;
};


/** The root mutation */
export type GQLRootMutationresetUserPasswordArgs = {
  input: GQLResetUserPasswordInput;
};


/** The root mutation */
export type GQLRootMutationrestoreCommentArgs = {
  input: GQLRestoreCommentInput;
};


/** The root mutation */
export type GQLRootMutationsendPasswordResetEmailArgs = {
  input: GQLSendPasswordResetEmailInput;
};


/** The root mutation */
export type GQLRootMutationupdateCommentArgs = {
  input: GQLUpdateCommentInput;
};


/** The root mutation */
export type GQLRootMutationupdateMediaItemArgs = {
  input: GQLUpdateMediaItemInput;
};


/** The root mutation */
export type GQLRootMutationupdatePageArgs = {
  input: GQLUpdatePageInput;
};


/** The root mutation */
export type GQLRootMutationupdatePostArgs = {
  input: GQLUpdatePostInput;
};


/** The root mutation */
export type GQLRootMutationupdateSettingsArgs = {
  input: GQLUpdateSettingsInput;
};


/** The root mutation */
export type GQLRootMutationupdateSocialMediaIconArgs = {
  input: GQLUpdateSocialMediaIconInput;
};


/** The root mutation */
export type GQLRootMutationupdateUserArgs = {
  input: GQLUpdateUserInput;
};

/** Input for the UpdateCategory mutation */
export type GQLUpdateCategoryInput = {
  /** The slug that the category will be an alias of */
  aliasOf?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The description of the category object */
  description?: Maybe<Scalars['String']>;
  /** The ID of the category object to update */
  id: Scalars['ID'];
  /** The name of the category object to mutate */
  name?: Maybe<Scalars['String']>;
  /** The ID of the category that should be set as the parent */
  parentId?: Maybe<Scalars['ID']>;
  /** If this argument exists then the slug will be checked to see if it is not an existing valid term. If that check succeeds (it is not a valid term), then it is added and the term id is given. If it fails, then a check is made to whether the taxonomy is hierarchical and the parent argument is not empty. If the second check succeeds, the term will be inserted and the term id will be given. If the slug argument is empty, then it will be calculated from the term name. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the UpdateCategory mutation */
export type GQLUpdateCategoryPayload = {
  /** The created category */
  category?: Maybe<GQLCategory>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Input for the UpdatePostFormat mutation */
export type GQLUpdatePostFormatInput = {
  /** The slug that the post_format will be an alias of */
  aliasOf?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The description of the post_format object */
  description?: Maybe<Scalars['String']>;
  /** The ID of the postFormat object to update */
  id: Scalars['ID'];
  /** The name of the post_format object to mutate */
  name?: Maybe<Scalars['String']>;
  /** If this argument exists then the slug will be checked to see if it is not an existing valid term. If that check succeeds (it is not a valid term), then it is added and the term id is given. If it fails, then a check is made to whether the taxonomy is hierarchical and the parent argument is not empty. If the second check succeeds, the term will be inserted and the term id will be given. If the slug argument is empty, then it will be calculated from the term name. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the UpdatePostFormat mutation */
export type GQLUpdatePostFormatPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The created post_format */
  postFormat?: Maybe<GQLPostFormat>;
};

/** Input for the UpdateTag mutation */
export type GQLUpdateTagInput = {
  /** The slug that the post_tag will be an alias of */
  aliasOf?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The description of the post_tag object */
  description?: Maybe<Scalars['String']>;
  /** The ID of the tag object to update */
  id: Scalars['ID'];
  /** The name of the post_tag object to mutate */
  name?: Maybe<Scalars['String']>;
  /** If this argument exists then the slug will be checked to see if it is not an existing valid term. If that check succeeds (it is not a valid term), then it is added and the term id is given. If it fails, then a check is made to whether the taxonomy is hierarchical and the parent argument is not empty. If the second check succeeds, the term will be inserted and the term id will be given. If the slug argument is empty, then it will be calculated from the term name. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the UpdateTag mutation */
export type GQLUpdateTagPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The created post_tag */
  tag?: Maybe<GQLTag>;
};

/** Input for the createCategory mutation */
export type GQLCreateCategoryInput = {
  /** The slug that the category will be an alias of */
  aliasOf?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The description of the category object */
  description?: Maybe<Scalars['String']>;
  /** The name of the category object to mutate */
  name: Scalars['String'];
  /** The ID of the category that should be set as the parent */
  parentId?: Maybe<Scalars['ID']>;
  /** If this argument exists then the slug will be checked to see if it is not an existing valid term. If that check succeeds (it is not a valid term), then it is added and the term id is given. If it fails, then a check is made to whether the taxonomy is hierarchical and the parent argument is not empty. If the second check succeeds, the term will be inserted and the term id will be given. If the slug argument is empty, then it will be calculated from the term name. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the createCategory mutation */
export type GQLCreateCategoryPayload = {
  /** The created category */
  category?: Maybe<GQLCategory>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Input for the createComment mutation */
export type GQLCreateCommentInput = {
  /** The approval status of the comment. */
  approved?: Maybe<Scalars['String']>;
  /** The name of the comment's author. */
  author?: Maybe<Scalars['String']>;
  /** The email of the comment's author. */
  authorEmail?: Maybe<Scalars['String']>;
  /** The url of the comment's author. */
  authorUrl?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the post object the comment belongs to. */
  commentOn?: Maybe<Scalars['Int']>;
  /** Content of the comment. */
  content?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day ( e.g. 01/31/2017 ) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** Parent comment of current comment. */
  parent?: Maybe<Scalars['ID']>;
  /** Type of comment. */
  type?: Maybe<Scalars['String']>;
};

/** The payload for the createComment mutation */
export type GQLCreateCommentPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment that was created */
  comment?: Maybe<GQLComment>;
  /** Whether the mutation succeeded. If the comment is not approved, the server will not return the comment to a non authenticated user, but a success message can be returned if the create succeeded, and the client can optimistically add the comment to the client cache */
  success?: Maybe<Scalars['Boolean']>;
};

/** Input for the createMediaItem mutation */
export type GQLCreateMediaItemInput = {
  /** Alternative text to display when mediaItem is not displayed */
  altText?: Maybe<Scalars['String']>;
  /** The userId to assign as the author of the mediaItem */
  authorId?: Maybe<Scalars['ID']>;
  /** The caption for the mediaItem */
  caption?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment status for the mediaItem */
  commentStatus?: Maybe<Scalars['String']>;
  /** The date of the mediaItem */
  date?: Maybe<Scalars['String']>;
  /** The date (in GMT zone) of the mediaItem */
  dateGmt?: Maybe<Scalars['String']>;
  /** Description of the mediaItem */
  description?: Maybe<Scalars['String']>;
  /** The file name of the mediaItem */
  filePath?: Maybe<Scalars['String']>;
  /** The file type of the mediaItem */
  fileType?: Maybe<GQLMimeTypeEnum>;
  /** The WordPress post ID or the graphQL postId of the parent object */
  parentId?: Maybe<Scalars['ID']>;
  /** The ping status for the mediaItem */
  pingStatus?: Maybe<Scalars['String']>;
  /** The slug of the mediaItem */
  slug?: Maybe<Scalars['String']>;
  /** The status of the mediaItem */
  status?: Maybe<GQLMediaItemStatusEnum>;
  /** The title of the mediaItem */
  title?: Maybe<Scalars['String']>;
};

/** The status of the media item object. */
export enum GQLMediaItemStatusEnum {
  /** Objects with the auto-draft status */
  AUTO_DRAFT = 'AUTO_DRAFT',
  /** Objects with the inherit status */
  INHERIT = 'INHERIT',
  /** Objects with the private status */
  PRIVATE = 'PRIVATE',
  /** Objects with the trash status */
  TRASH = 'TRASH'
}

/** The payload for the createMediaItem mutation */
export type GQLCreateMediaItemPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  mediaItem?: Maybe<GQLMediaItem>;
};

/** Input for the createPage mutation */
export type GQLCreatePageInput = {
  /** The userId to assign as the author of the object */
  authorId?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment status for the object */
  commentStatus?: Maybe<Scalars['String']>;
  /** The content of the object */
  content?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day (e.g. 01/31/2017) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The ID of the parent object */
  parentId?: Maybe<Scalars['ID']>;
  /** The password used to protect the content of the object */
  password?: Maybe<Scalars['String']>;
  /** The slug of the object */
  slug?: Maybe<Scalars['String']>;
  /** The status of the object */
  status?: Maybe<GQLPostStatusEnum>;
  /** The title of the object */
  title?: Maybe<Scalars['String']>;
};

/** The payload for the createPage mutation */
export type GQLCreatePagePayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  page?: Maybe<GQLPage>;
};

/** Input for the createPost mutation */
export type GQLCreatePostInput = {
  /** The userId to assign as the author of the object */
  authorId?: Maybe<Scalars['ID']>;
  /** Set connections between the post and categories */
  categories?: Maybe<GQLPostCategoriesInput>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment status for the object */
  commentStatus?: Maybe<Scalars['String']>;
  /** The content of the object */
  content?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day (e.g. 01/31/2017) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** The excerpt of the object */
  excerpt?: Maybe<Scalars['String']>;
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The password used to protect the content of the object */
  password?: Maybe<Scalars['String']>;
  /** The ping status for the object */
  pingStatus?: Maybe<Scalars['String']>;
  /** URLs that have been pinged. */
  pinged?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Set connections between the post and postFormats */
  postFormats?: Maybe<GQLPostPostFormatsInput>;
  /** The slug of the object */
  slug?: Maybe<Scalars['String']>;
  /** The status of the object */
  status?: Maybe<GQLPostStatusEnum>;
  /** Set connections between the post and tags */
  tags?: Maybe<GQLPostTagsInput>;
  /** The title of the object */
  title?: Maybe<Scalars['String']>;
  /** URLs queued to be pinged. */
  toPing?: Maybe<Array<Maybe<Scalars['String']>>>;
};

/** Set relationships between the post to categories */
export type GQLPostCategoriesInput = {
  /** If true, this will append the category to existing related categories. If false, this will replace existing relationships. Default true. */
  append?: Maybe<Scalars['Boolean']>;
  nodes?: Maybe<Array<Maybe<GQLPostCategoriesNodeInput>>>;
};

/** List of categories to connect the post to. If an ID is set, it will be used to create the connection. If not, it will look for a slug. If neither are valid existing terms, and the site is configured to allow terms to be created during post mutations, a term will be created using the Name if it exists in the input, then fallback to the slug if it exists. */
export type GQLPostCategoriesNodeInput = {
  /** The description of the category. This field is used to set a description of the category if a new one is created during the mutation. */
  description?: Maybe<Scalars['String']>;
  /** The ID of the category. If present, this will be used to connect to the post. If no existing category exists with this ID, no connection will be made. */
  id?: Maybe<Scalars['ID']>;
  /** The name of the category. This field is used to create a new term, if term creation is enabled in nested mutations, and if one does not already exist with the provided slug or ID or if a slug or ID is not provided. If no name is included and a term is created, the creation will fallback to the slug field. */
  name?: Maybe<Scalars['String']>;
  /** The slug of the category. If no ID is present, this field will be used to make a connection. If no existing term exists with this slug, this field will be used as a fallback to the Name field when creating a new term to connect to, if term creation is enabled as a nested mutation. */
  slug?: Maybe<Scalars['String']>;
};

/** Set relationships between the post to postFormats */
export type GQLPostPostFormatsInput = {
  /** If true, this will append the postFormat to existing related postFormats. If false, this will replace existing relationships. Default true. */
  append?: Maybe<Scalars['Boolean']>;
  nodes?: Maybe<Array<Maybe<GQLPostPostFormatsNodeInput>>>;
};

/** List of postFormats to connect the post to. If an ID is set, it will be used to create the connection. If not, it will look for a slug. If neither are valid existing terms, and the site is configured to allow terms to be created during post mutations, a term will be created using the Name if it exists in the input, then fallback to the slug if it exists. */
export type GQLPostPostFormatsNodeInput = {
  /** The description of the postFormat. This field is used to set a description of the postFormat if a new one is created during the mutation. */
  description?: Maybe<Scalars['String']>;
  /** The ID of the postFormat. If present, this will be used to connect to the post. If no existing postFormat exists with this ID, no connection will be made. */
  id?: Maybe<Scalars['ID']>;
  /** The name of the postFormat. This field is used to create a new term, if term creation is enabled in nested mutations, and if one does not already exist with the provided slug or ID or if a slug or ID is not provided. If no name is included and a term is created, the creation will fallback to the slug field. */
  name?: Maybe<Scalars['String']>;
  /** The slug of the postFormat. If no ID is present, this field will be used to make a connection. If no existing term exists with this slug, this field will be used as a fallback to the Name field when creating a new term to connect to, if term creation is enabled as a nested mutation. */
  slug?: Maybe<Scalars['String']>;
};

/** Set relationships between the post to tags */
export type GQLPostTagsInput = {
  /** If true, this will append the tag to existing related tags. If false, this will replace existing relationships. Default true. */
  append?: Maybe<Scalars['Boolean']>;
  nodes?: Maybe<Array<Maybe<GQLPostTagsNodeInput>>>;
};

/** List of tags to connect the post to. If an ID is set, it will be used to create the connection. If not, it will look for a slug. If neither are valid existing terms, and the site is configured to allow terms to be created during post mutations, a term will be created using the Name if it exists in the input, then fallback to the slug if it exists. */
export type GQLPostTagsNodeInput = {
  /** The description of the tag. This field is used to set a description of the tag if a new one is created during the mutation. */
  description?: Maybe<Scalars['String']>;
  /** The ID of the tag. If present, this will be used to connect to the post. If no existing tag exists with this ID, no connection will be made. */
  id?: Maybe<Scalars['ID']>;
  /** The name of the tag. This field is used to create a new term, if term creation is enabled in nested mutations, and if one does not already exist with the provided slug or ID or if a slug or ID is not provided. If no name is included and a term is created, the creation will fallback to the slug field. */
  name?: Maybe<Scalars['String']>;
  /** The slug of the tag. If no ID is present, this field will be used to make a connection. If no existing term exists with this slug, this field will be used as a fallback to the Name field when creating a new term to connect to, if term creation is enabled as a nested mutation. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the createPost mutation */
export type GQLCreatePostPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  post?: Maybe<GQLPost>;
};

/** Input for the createPostFormat mutation */
export type GQLCreatePostFormatInput = {
  /** The slug that the post_format will be an alias of */
  aliasOf?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The description of the post_format object */
  description?: Maybe<Scalars['String']>;
  /** The name of the post_format object to mutate */
  name: Scalars['String'];
  /** If this argument exists then the slug will be checked to see if it is not an existing valid term. If that check succeeds (it is not a valid term), then it is added and the term id is given. If it fails, then a check is made to whether the taxonomy is hierarchical and the parent argument is not empty. If the second check succeeds, the term will be inserted and the term id will be given. If the slug argument is empty, then it will be calculated from the term name. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the createPostFormat mutation */
export type GQLCreatePostFormatPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The created post_format */
  postFormat?: Maybe<GQLPostFormat>;
};

/** Input for the createSocialMediaIcon mutation */
export type GQLCreateSocialMediaIconInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day (e.g. 01/31/2017) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The password used to protect the content of the object */
  password?: Maybe<Scalars['String']>;
  /** The slug of the object */
  slug?: Maybe<Scalars['String']>;
  /** The status of the object */
  status?: Maybe<GQLPostStatusEnum>;
  /** The title of the object */
  title?: Maybe<Scalars['String']>;
};

/** The payload for the createSocialMediaIcon mutation */
export type GQLCreateSocialMediaIconPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  socialMediaIcon?: Maybe<GQLSocialMediaIcon>;
};

/** Input for the createTag mutation */
export type GQLCreateTagInput = {
  /** The slug that the post_tag will be an alias of */
  aliasOf?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The description of the post_tag object */
  description?: Maybe<Scalars['String']>;
  /** The name of the post_tag object to mutate */
  name: Scalars['String'];
  /** If this argument exists then the slug will be checked to see if it is not an existing valid term. If that check succeeds (it is not a valid term), then it is added and the term id is given. If it fails, then a check is made to whether the taxonomy is hierarchical and the parent argument is not empty. If the second check succeeds, the term will be inserted and the term id will be given. If the slug argument is empty, then it will be calculated from the term name. */
  slug?: Maybe<Scalars['String']>;
};

/** The payload for the createTag mutation */
export type GQLCreateTagPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The created post_tag */
  tag?: Maybe<GQLTag>;
};

/** Input for the createUser mutation */
export type GQLCreateUserInput = {
  /** User's AOL IM account. */
  aim?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** A string containing content about the user. */
  description?: Maybe<Scalars['String']>;
  /** A string that will be shown on the site. Defaults to user's username. It is likely that you will want to change this, for both appearance and security through obscurity (that is if you dont use and delete the default admin user). */
  displayName?: Maybe<Scalars['String']>;
  /** A string containing the user's email address. */
  email?: Maybe<Scalars['String']>;
  /** 	The user's first name. */
  firstName?: Maybe<Scalars['String']>;
  /** User's Jabber account. */
  jabber?: Maybe<Scalars['String']>;
  /** The user's last name. */
  lastName?: Maybe<Scalars['String']>;
  /** User's locale. */
  locale?: Maybe<Scalars['String']>;
  /** A string that contains a URL-friendly name for the user. The default is the user's username. */
  nicename?: Maybe<Scalars['String']>;
  /** The user's nickname, defaults to the user's username. */
  nickname?: Maybe<Scalars['String']>;
  /** A string that contains the plain text password for the user. */
  password?: Maybe<Scalars['String']>;
  /** If true, this will refresh the users JWT secret. */
  refreshJwtUserSecret?: Maybe<Scalars['Boolean']>;
  /** The date the user registered. Format is Y-m-d H:i:s. */
  registered?: Maybe<Scalars['String']>;
  /** If true, this will revoke the users JWT secret. If false, this will unrevoke the JWT secret AND issue a new one. To revoke, the user must have proper capabilities to edit users JWT secrets. */
  revokeJwtUserSecret?: Maybe<Scalars['Boolean']>;
  /** A string for whether to enable the rich editor or not. False if not empty. */
  richEditing?: Maybe<Scalars['String']>;
  /** An array of roles to be assigned to the user. */
  roles?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** A string that contains the user's username for logging in. */
  username: Scalars['String'];
  /** A string containing the user's URL for the user's web site. */
  websiteUrl?: Maybe<Scalars['String']>;
  /** User's Yahoo IM account. */
  yim?: Maybe<Scalars['String']>;
};

/** The payload for the createUser mutation */
export type GQLCreateUserPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<GQLUser>;
};

/** Input for the deleteCategory mutation */
export type GQLDeleteCategoryInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the category to delete */
  id: Scalars['ID'];
};

/** The payload for the deleteCategory mutation */
export type GQLDeleteCategoryPayload = {
  /** The deteted term object */
  category?: Maybe<GQLCategory>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted object */
  deletedId?: Maybe<Scalars['ID']>;
};

/** Input for the deleteComment mutation */
export type GQLDeleteCommentInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Whether the comment should be force deleted instead of being moved to the trash */
  forceDelete?: Maybe<Scalars['Boolean']>;
  /** The deleted comment ID */
  id: Scalars['ID'];
};

/** The payload for the deleteComment mutation */
export type GQLDeleteCommentPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The deleted comment object */
  comment?: Maybe<GQLComment>;
  /** The deleted comment ID */
  deletedId?: Maybe<Scalars['ID']>;
};

/** Input for the deleteMediaItem mutation */
export type GQLDeleteMediaItemInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Whether the mediaItem should be force deleted instead of being moved to the trash */
  forceDelete?: Maybe<Scalars['Boolean']>;
  /** The ID of the mediaItem to delete */
  id: Scalars['ID'];
};

/** The payload for the deleteMediaItem mutation */
export type GQLDeleteMediaItemPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted mediaItem */
  deletedId?: Maybe<Scalars['ID']>;
  /** The mediaItem before it was deleted */
  mediaItem?: Maybe<GQLMediaItem>;
};

/** Input for the deletePage mutation */
export type GQLDeletePageInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Whether the object should be force deleted instead of being moved to the trash */
  forceDelete?: Maybe<Scalars['Boolean']>;
  /** The ID of the page to delete */
  id: Scalars['ID'];
};

/** The payload for the deletePage mutation */
export type GQLDeletePagePayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted object */
  deletedId?: Maybe<Scalars['ID']>;
  /** The object before it was deleted */
  page?: Maybe<GQLPage>;
};

/** Input for the deletePost mutation */
export type GQLDeletePostInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Whether the object should be force deleted instead of being moved to the trash */
  forceDelete?: Maybe<Scalars['Boolean']>;
  /** The ID of the post to delete */
  id: Scalars['ID'];
};

/** The payload for the deletePost mutation */
export type GQLDeletePostPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted object */
  deletedId?: Maybe<Scalars['ID']>;
  /** The object before it was deleted */
  post?: Maybe<GQLPost>;
};

/** Input for the deletePostFormat mutation */
export type GQLDeletePostFormatInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the postFormat to delete */
  id: Scalars['ID'];
};

/** The payload for the deletePostFormat mutation */
export type GQLDeletePostFormatPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted object */
  deletedId?: Maybe<Scalars['ID']>;
  /** The deteted term object */
  postFormat?: Maybe<GQLPostFormat>;
};

/** Input for the deleteSocialMediaIcon mutation */
export type GQLDeleteSocialMediaIconInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Whether the object should be force deleted instead of being moved to the trash */
  forceDelete?: Maybe<Scalars['Boolean']>;
  /** The ID of the socialMediaIcon to delete */
  id: Scalars['ID'];
};

/** The payload for the deleteSocialMediaIcon mutation */
export type GQLDeleteSocialMediaIconPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted object */
  deletedId?: Maybe<Scalars['ID']>;
  /** The object before it was deleted */
  socialMediaIcon?: Maybe<GQLSocialMediaIcon>;
};

/** Input for the deleteTag mutation */
export type GQLDeleteTagInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the tag to delete */
  id: Scalars['ID'];
};

/** The payload for the deleteTag mutation */
export type GQLDeleteTagPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the deleted object */
  deletedId?: Maybe<Scalars['ID']>;
  /** The deteted term object */
  tag?: Maybe<GQLTag>;
};

/** Input for the deleteUser mutation */
export type GQLDeleteUserInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the user you want to delete */
  id: Scalars['ID'];
  /** Reassign posts and links to new User ID. */
  reassignId?: Maybe<Scalars['ID']>;
};

/** The payload for the deleteUser mutation */
export type GQLDeleteUserPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the user that you just deleted */
  deletedId?: Maybe<Scalars['ID']>;
  /** The deleted user object */
  user?: Maybe<GQLUser>;
};

/** Input for the login mutation */
export type GQLLoginInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The plain-text password for the user logging in. */
  password: Scalars['String'];
  /** The username used for login. Typically a unique or email address depending on specific configuration */
  username: Scalars['String'];
};

/** The payload for the login mutation */
export type GQLLoginPayload = {
  /** JWT Token that can be used in future requests for Authentication */
  authToken?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** A JWT token that can be used in future requests to get a refreshed jwtAuthToken. If the refresh token used in a request is revoked or otherwise invalid, a valid Auth token will NOT be issued in the response headers. */
  refreshToken?: Maybe<Scalars['String']>;
  /** The user that was logged in */
  user?: Maybe<GQLUser>;
};

/** Input for the refreshJwtAuthToken mutation */
export type GQLRefreshJwtAuthTokenInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** A valid, previously issued JWT refresh token. If valid a new Auth token will be provided. If invalid, expired, revoked or otherwise invalid, a new AuthToken will not be provided. */
  jwtRefreshToken: Scalars['String'];
};

/** The payload for the refreshJwtAuthToken mutation */
export type GQLRefreshJwtAuthTokenPayload = {
  /** JWT Token that can be used in future requests for Authentication */
  authToken?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Input for the registerUser mutation */
export type GQLRegisterUserInput = {
  /** User's AOL IM account. */
  aim?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** A string containing content about the user. */
  description?: Maybe<Scalars['String']>;
  /** A string that will be shown on the site. Defaults to user's username. It is likely that you will want to change this, for both appearance and security through obscurity (that is if you dont use and delete the default admin user). */
  displayName?: Maybe<Scalars['String']>;
  /** A string containing the user's email address. */
  email?: Maybe<Scalars['String']>;
  /** 	The user's first name. */
  firstName?: Maybe<Scalars['String']>;
  /** User's Jabber account. */
  jabber?: Maybe<Scalars['String']>;
  /** The user's last name. */
  lastName?: Maybe<Scalars['String']>;
  /** User's locale. */
  locale?: Maybe<Scalars['String']>;
  /** A string that contains a URL-friendly name for the user. The default is the user's username. */
  nicename?: Maybe<Scalars['String']>;
  /** The user's nickname, defaults to the user's username. */
  nickname?: Maybe<Scalars['String']>;
  /** A string that contains the plain text password for the user. */
  password?: Maybe<Scalars['String']>;
  /** If true, this will refresh the users JWT secret. */
  refreshJwtUserSecret?: Maybe<Scalars['Boolean']>;
  /** The date the user registered. Format is Y-m-d H:i:s. */
  registered?: Maybe<Scalars['String']>;
  /** If true, this will revoke the users JWT secret. If false, this will unrevoke the JWT secret AND issue a new one. To revoke, the user must have proper capabilities to edit users JWT secrets. */
  revokeJwtUserSecret?: Maybe<Scalars['Boolean']>;
  /** A string for whether to enable the rich editor or not. False if not empty. */
  richEditing?: Maybe<Scalars['String']>;
  /** A string that contains the user's username. */
  username: Scalars['String'];
  /** A string containing the user's URL for the user's web site. */
  websiteUrl?: Maybe<Scalars['String']>;
  /** User's Yahoo IM account. */
  yim?: Maybe<Scalars['String']>;
};

/** The payload for the registerUser mutation */
export type GQLRegisterUserPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<GQLUser>;
};

/** Input for the resetUserPassword mutation */
export type GQLResetUserPasswordInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Password reset key */
  key?: Maybe<Scalars['String']>;
  /** The user's login (username). */
  login?: Maybe<Scalars['String']>;
  /** The new password. */
  password?: Maybe<Scalars['String']>;
};

/** The payload for the resetUserPassword mutation */
export type GQLResetUserPasswordPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<GQLUser>;
};

/** Input for the restoreComment mutation */
export type GQLRestoreCommentInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the comment to be restored */
  id: Scalars['ID'];
};

/** The payload for the restoreComment mutation */
export type GQLRestoreCommentPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The restored comment object */
  comment?: Maybe<GQLComment>;
  /** The ID of the restored comment */
  restoredId?: Maybe<Scalars['ID']>;
};

/** Input for the sendPasswordResetEmail mutation */
export type GQLSendPasswordResetEmailInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** A string that contains the user's username or email address. */
  username: Scalars['String'];
};

/** The payload for the sendPasswordResetEmail mutation */
export type GQLSendPasswordResetEmailPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The user that the password reset email was sent to */
  user?: Maybe<GQLUser>;
};

/** Input for the updateComment mutation */
export type GQLUpdateCommentInput = {
  /** The approval status of the comment. */
  approved?: Maybe<Scalars['String']>;
  /** The name of the comment's author. */
  author?: Maybe<Scalars['String']>;
  /** The email of the comment's author. */
  authorEmail?: Maybe<Scalars['String']>;
  /** The url of the comment's author. */
  authorUrl?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The ID of the post object the comment belongs to. */
  commentOn?: Maybe<Scalars['Int']>;
  /** Content of the comment. */
  content?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day ( e.g. 01/31/2017 ) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** The ID of the comment being updated. */
  id: Scalars['ID'];
  /** Parent comment of current comment. */
  parent?: Maybe<Scalars['ID']>;
  /** Type of comment. */
  type?: Maybe<Scalars['String']>;
};

/** The payload for the updateComment mutation */
export type GQLUpdateCommentPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment that was created */
  comment?: Maybe<GQLComment>;
  /** Whether the mutation succeeded. If the comment is not approved, the server will not return the comment to a non authenticated user, but a success message can be returned if the create succeeded, and the client can optimistically add the comment to the client cache */
  success?: Maybe<Scalars['Boolean']>;
};

/** Input for the updateMediaItem mutation */
export type GQLUpdateMediaItemInput = {
  /** Alternative text to display when mediaItem is not displayed */
  altText?: Maybe<Scalars['String']>;
  /** The userId to assign as the author of the mediaItem */
  authorId?: Maybe<Scalars['ID']>;
  /** The caption for the mediaItem */
  caption?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment status for the mediaItem */
  commentStatus?: Maybe<Scalars['String']>;
  /** The date of the mediaItem */
  date?: Maybe<Scalars['String']>;
  /** The date (in GMT zone) of the mediaItem */
  dateGmt?: Maybe<Scalars['String']>;
  /** Description of the mediaItem */
  description?: Maybe<Scalars['String']>;
  /** The file name of the mediaItem */
  filePath?: Maybe<Scalars['String']>;
  /** The file type of the mediaItem */
  fileType?: Maybe<GQLMimeTypeEnum>;
  /** The ID of the mediaItem object */
  id: Scalars['ID'];
  /** The WordPress post ID or the graphQL postId of the parent object */
  parentId?: Maybe<Scalars['ID']>;
  /** The ping status for the mediaItem */
  pingStatus?: Maybe<Scalars['String']>;
  /** The slug of the mediaItem */
  slug?: Maybe<Scalars['String']>;
  /** The status of the mediaItem */
  status?: Maybe<GQLMediaItemStatusEnum>;
  /** The title of the mediaItem */
  title?: Maybe<Scalars['String']>;
};

/** The payload for the updateMediaItem mutation */
export type GQLUpdateMediaItemPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  mediaItem?: Maybe<GQLMediaItem>;
};

/** Input for the updatePage mutation */
export type GQLUpdatePageInput = {
  /** The userId to assign as the author of the object */
  authorId?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment status for the object */
  commentStatus?: Maybe<Scalars['String']>;
  /** The content of the object */
  content?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day (e.g. 01/31/2017) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** The ID of the page object */
  id: Scalars['ID'];
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The ID of the parent object */
  parentId?: Maybe<Scalars['ID']>;
  /** The password used to protect the content of the object */
  password?: Maybe<Scalars['String']>;
  /** The slug of the object */
  slug?: Maybe<Scalars['String']>;
  /** The status of the object */
  status?: Maybe<GQLPostStatusEnum>;
  /** The title of the object */
  title?: Maybe<Scalars['String']>;
};

/** The payload for the updatePage mutation */
export type GQLUpdatePagePayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  page?: Maybe<GQLPage>;
};

/** Input for the updatePost mutation */
export type GQLUpdatePostInput = {
  /** The userId to assign as the author of the object */
  authorId?: Maybe<Scalars['ID']>;
  /** Set connections between the post and categories */
  categories?: Maybe<GQLPostCategoriesInput>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The comment status for the object */
  commentStatus?: Maybe<Scalars['String']>;
  /** The content of the object */
  content?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day (e.g. 01/31/2017) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** The excerpt of the object */
  excerpt?: Maybe<Scalars['String']>;
  /** The ID of the post object */
  id: Scalars['ID'];
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The password used to protect the content of the object */
  password?: Maybe<Scalars['String']>;
  /** The ping status for the object */
  pingStatus?: Maybe<Scalars['String']>;
  /** URLs that have been pinged. */
  pinged?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Set connections between the post and postFormats */
  postFormats?: Maybe<GQLPostPostFormatsInput>;
  /** The slug of the object */
  slug?: Maybe<Scalars['String']>;
  /** The status of the object */
  status?: Maybe<GQLPostStatusEnum>;
  /** Set connections between the post and tags */
  tags?: Maybe<GQLPostTagsInput>;
  /** The title of the object */
  title?: Maybe<Scalars['String']>;
  /** URLs queued to be pinged. */
  toPing?: Maybe<Array<Maybe<Scalars['String']>>>;
};

/** The payload for the updatePost mutation */
export type GQLUpdatePostPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  post?: Maybe<GQLPost>;
};

/** Input for the updateSettings mutation */
export type GQLUpdateSettingsInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** Allow people to submit comments on new posts. */
  discussionSettingsDefaultCommentStatus?: Maybe<Scalars['String']>;
  /** Allow link notifications from other blogs (pingbacks and trackbacks) on new articles. */
  discussionSettingsDefaultPingStatus?: Maybe<Scalars['String']>;
  /** A date format for all date strings. */
  generalSettingsDateFormat?: Maybe<Scalars['String']>;
  /** Site tagline. */
  generalSettingsDescription?: Maybe<Scalars['String']>;
  /** This address is used for admin purposes, like new user notification. */
  generalSettingsEmail?: Maybe<Scalars['String']>;
  /** WordPress locale code. */
  generalSettingsLanguage?: Maybe<Scalars['String']>;
  /** A day number of the week that the week should start on. */
  generalSettingsStartOfWeek?: Maybe<Scalars['Int']>;
  /** A time format for all time strings. */
  generalSettingsTimeFormat?: Maybe<Scalars['String']>;
  /** A city in the same timezone as you. */
  generalSettingsTimezone?: Maybe<Scalars['String']>;
  /** Site title. */
  generalSettingsTitle?: Maybe<Scalars['String']>;
  /** Site URL. */
  generalSettingsUrl?: Maybe<Scalars['String']>;
  /** Blog pages show at most. */
  readingSettingsPostsPerPage?: Maybe<Scalars['Int']>;
  /** Default post category. */
  writingSettingsDefaultCategory?: Maybe<Scalars['Int']>;
  /** Default post format. */
  writingSettingsDefaultPostFormat?: Maybe<Scalars['String']>;
  /** Convert emoticons like :-) and :-P to graphics on display. */
  writingSettingsUseSmilies?: Maybe<Scalars['Boolean']>;
};

/** The payload for the updateSettings mutation */
export type GQLUpdateSettingsPayload = {
  allSettings?: Maybe<GQLSettings>;
  clientMutationId?: Maybe<Scalars['String']>;
  discussionSettings?: Maybe<GQLDiscussionSettings>;
  generalSettings?: Maybe<GQLGeneralSettings>;
  readingSettings?: Maybe<GQLReadingSettings>;
  writingSettings?: Maybe<GQLWritingSettings>;
};

/** Input for the updateSocialMediaIcon mutation */
export type GQLUpdateSocialMediaIconInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The date of the object. Preferable to enter as year/month/day (e.g. 01/31/2017) as it will rearrange date as fit if it is not specified. Incomplete dates may have unintended results for example, "2017" as the input will use current date with timestamp 20:17  */
  date?: Maybe<Scalars['String']>;
  /** The ID of the socialMediaIcon object */
  id: Scalars['ID'];
  /** A field used for ordering posts. This is typically used with nav menu items or for special ordering of hierarchical content types. */
  menuOrder?: Maybe<Scalars['Int']>;
  /** The password used to protect the content of the object */
  password?: Maybe<Scalars['String']>;
  /** The slug of the object */
  slug?: Maybe<Scalars['String']>;
  /** The status of the object */
  status?: Maybe<GQLPostStatusEnum>;
  /** The title of the object */
  title?: Maybe<Scalars['String']>;
};

/** The payload for the updateSocialMediaIcon mutation */
export type GQLUpdateSocialMediaIconPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  socialMediaIcon?: Maybe<GQLSocialMediaIcon>;
};

/** Input for the updateUser mutation */
export type GQLUpdateUserInput = {
  /** User's AOL IM account. */
  aim?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** A string containing content about the user. */
  description?: Maybe<Scalars['String']>;
  /** A string that will be shown on the site. Defaults to user's username. It is likely that you will want to change this, for both appearance and security through obscurity (that is if you dont use and delete the default admin user). */
  displayName?: Maybe<Scalars['String']>;
  /** A string containing the user's email address. */
  email?: Maybe<Scalars['String']>;
  /** 	The user's first name. */
  firstName?: Maybe<Scalars['String']>;
  /** The ID of the user */
  id: Scalars['ID'];
  /** User's Jabber account. */
  jabber?: Maybe<Scalars['String']>;
  /** The user's last name. */
  lastName?: Maybe<Scalars['String']>;
  /** User's locale. */
  locale?: Maybe<Scalars['String']>;
  /** A string that contains a URL-friendly name for the user. The default is the user's username. */
  nicename?: Maybe<Scalars['String']>;
  /** The user's nickname, defaults to the user's username. */
  nickname?: Maybe<Scalars['String']>;
  /** A string that contains the plain text password for the user. */
  password?: Maybe<Scalars['String']>;
  /** If true, this will refresh the users JWT secret. */
  refreshJwtUserSecret?: Maybe<Scalars['Boolean']>;
  /** The date the user registered. Format is Y-m-d H:i:s. */
  registered?: Maybe<Scalars['String']>;
  /** If true, this will revoke the users JWT secret. If false, this will unrevoke the JWT secret AND issue a new one. To revoke, the user must have proper capabilities to edit users JWT secrets. */
  revokeJwtUserSecret?: Maybe<Scalars['Boolean']>;
  /** A string for whether to enable the rich editor or not. False if not empty. */
  richEditing?: Maybe<Scalars['String']>;
  /** An array of roles to be assigned to the user. */
  roles?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** A string containing the user's URL for the user's web site. */
  websiteUrl?: Maybe<Scalars['String']>;
  /** User's Yahoo IM account. */
  yim?: Maybe<Scalars['String']>;
};

/** The payload for the updateUser mutation */
export type GQLUpdateUserPayload = {
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<GQLUser>;
};

/** A Comment Author object */
export type GQLCommentAuthor = GQLNode & GQLCommenter & {
  /** Identifies the primary key from the database. */
  databaseId: Scalars['Int'];
  /** The email for the comment author */
  email?: Maybe<Scalars['String']>;
  /** The globally unique identifier for the comment author object */
  id: Scalars['ID'];
  /** Whether the object is restricted from the current viewer */
  isRestricted?: Maybe<Scalars['Boolean']>;
  /** The name for the comment author. */
  name?: Maybe<Scalars['String']>;
  /** The url the comment author. */
  url?: Maybe<Scalars['String']>;
};

/** Available timezones */
export enum GQLTimezoneEnum {
  /** Abidjan */
  AFRICA_ABIDJAN = 'AFRICA_ABIDJAN',
  /** Accra */
  AFRICA_ACCRA = 'AFRICA_ACCRA',
  /** Addis Ababa */
  AFRICA_ADDIS_ABABA = 'AFRICA_ADDIS_ABABA',
  /** Algiers */
  AFRICA_ALGIERS = 'AFRICA_ALGIERS',
  /** Asmara */
  AFRICA_ASMARA = 'AFRICA_ASMARA',
  /** Bamako */
  AFRICA_BAMAKO = 'AFRICA_BAMAKO',
  /** Bangui */
  AFRICA_BANGUI = 'AFRICA_BANGUI',
  /** Banjul */
  AFRICA_BANJUL = 'AFRICA_BANJUL',
  /** Bissau */
  AFRICA_BISSAU = 'AFRICA_BISSAU',
  /** Blantyre */
  AFRICA_BLANTYRE = 'AFRICA_BLANTYRE',
  /** Brazzaville */
  AFRICA_BRAZZAVILLE = 'AFRICA_BRAZZAVILLE',
  /** Bujumbura */
  AFRICA_BUJUMBURA = 'AFRICA_BUJUMBURA',
  /** Cairo */
  AFRICA_CAIRO = 'AFRICA_CAIRO',
  /** Casablanca */
  AFRICA_CASABLANCA = 'AFRICA_CASABLANCA',
  /** Ceuta */
  AFRICA_CEUTA = 'AFRICA_CEUTA',
  /** Conakry */
  AFRICA_CONAKRY = 'AFRICA_CONAKRY',
  /** Dakar */
  AFRICA_DAKAR = 'AFRICA_DAKAR',
  /** Dar es Salaam */
  AFRICA_DAR_ES_SALAAM = 'AFRICA_DAR_ES_SALAAM',
  /** Djibouti */
  AFRICA_DJIBOUTI = 'AFRICA_DJIBOUTI',
  /** Douala */
  AFRICA_DOUALA = 'AFRICA_DOUALA',
  /** El Aaiun */
  AFRICA_EL_AAIUN = 'AFRICA_EL_AAIUN',
  /** Freetown */
  AFRICA_FREETOWN = 'AFRICA_FREETOWN',
  /** Gaborone */
  AFRICA_GABORONE = 'AFRICA_GABORONE',
  /** Harare */
  AFRICA_HARARE = 'AFRICA_HARARE',
  /** Johannesburg */
  AFRICA_JOHANNESBURG = 'AFRICA_JOHANNESBURG',
  /** Juba */
  AFRICA_JUBA = 'AFRICA_JUBA',
  /** Kampala */
  AFRICA_KAMPALA = 'AFRICA_KAMPALA',
  /** Khartoum */
  AFRICA_KHARTOUM = 'AFRICA_KHARTOUM',
  /** Kigali */
  AFRICA_KIGALI = 'AFRICA_KIGALI',
  /** Kinshasa */
  AFRICA_KINSHASA = 'AFRICA_KINSHASA',
  /** Lagos */
  AFRICA_LAGOS = 'AFRICA_LAGOS',
  /** Libreville */
  AFRICA_LIBREVILLE = 'AFRICA_LIBREVILLE',
  /** Lome */
  AFRICA_LOME = 'AFRICA_LOME',
  /** Luanda */
  AFRICA_LUANDA = 'AFRICA_LUANDA',
  /** Lubumbashi */
  AFRICA_LUBUMBASHI = 'AFRICA_LUBUMBASHI',
  /** Lusaka */
  AFRICA_LUSAKA = 'AFRICA_LUSAKA',
  /** Malabo */
  AFRICA_MALABO = 'AFRICA_MALABO',
  /** Maputo */
  AFRICA_MAPUTO = 'AFRICA_MAPUTO',
  /** Maseru */
  AFRICA_MASERU = 'AFRICA_MASERU',
  /** Mbabane */
  AFRICA_MBABANE = 'AFRICA_MBABANE',
  /** Mogadishu */
  AFRICA_MOGADISHU = 'AFRICA_MOGADISHU',
  /** Monrovia */
  AFRICA_MONROVIA = 'AFRICA_MONROVIA',
  /** Nairobi */
  AFRICA_NAIROBI = 'AFRICA_NAIROBI',
  /** Ndjamena */
  AFRICA_NDJAMENA = 'AFRICA_NDJAMENA',
  /** Niamey */
  AFRICA_NIAMEY = 'AFRICA_NIAMEY',
  /** Nouakchott */
  AFRICA_NOUAKCHOTT = 'AFRICA_NOUAKCHOTT',
  /** Ouagadougou */
  AFRICA_OUAGADOUGOU = 'AFRICA_OUAGADOUGOU',
  /** Porto-Novo */
  AFRICA_PORTO_NOVO = 'AFRICA_PORTO_NOVO',
  /** Sao Tome */
  AFRICA_SAO_TOME = 'AFRICA_SAO_TOME',
  /** Tripoli */
  AFRICA_TRIPOLI = 'AFRICA_TRIPOLI',
  /** Tunis */
  AFRICA_TUNIS = 'AFRICA_TUNIS',
  /** Windhoek */
  AFRICA_WINDHOEK = 'AFRICA_WINDHOEK',
  /** Adak */
  AMERICA_ADAK = 'AMERICA_ADAK',
  /** Anchorage */
  AMERICA_ANCHORAGE = 'AMERICA_ANCHORAGE',
  /** Anguilla */
  AMERICA_ANGUILLA = 'AMERICA_ANGUILLA',
  /** Antigua */
  AMERICA_ANTIGUA = 'AMERICA_ANTIGUA',
  /** Araguaina */
  AMERICA_ARAGUAINA = 'AMERICA_ARAGUAINA',
  /** Argentina - Buenos Aires */
  AMERICA_ARGENTINA_BUENOS_AIRES = 'AMERICA_ARGENTINA_BUENOS_AIRES',
  /** Argentina - Catamarca */
  AMERICA_ARGENTINA_CATAMARCA = 'AMERICA_ARGENTINA_CATAMARCA',
  /** Argentina - Cordoba */
  AMERICA_ARGENTINA_CORDOBA = 'AMERICA_ARGENTINA_CORDOBA',
  /** Argentina - Jujuy */
  AMERICA_ARGENTINA_JUJUY = 'AMERICA_ARGENTINA_JUJUY',
  /** Argentina - La Rioja */
  AMERICA_ARGENTINA_LA_RIOJA = 'AMERICA_ARGENTINA_LA_RIOJA',
  /** Argentina - Mendoza */
  AMERICA_ARGENTINA_MENDOZA = 'AMERICA_ARGENTINA_MENDOZA',
  /** Argentina - Rio Gallegos */
  AMERICA_ARGENTINA_RIO_GALLEGOS = 'AMERICA_ARGENTINA_RIO_GALLEGOS',
  /** Argentina - Salta */
  AMERICA_ARGENTINA_SALTA = 'AMERICA_ARGENTINA_SALTA',
  /** Argentina - San Juan */
  AMERICA_ARGENTINA_SAN_JUAN = 'AMERICA_ARGENTINA_SAN_JUAN',
  /** Argentina - San Luis */
  AMERICA_ARGENTINA_SAN_LUIS = 'AMERICA_ARGENTINA_SAN_LUIS',
  /** Argentina - Tucuman */
  AMERICA_ARGENTINA_TUCUMAN = 'AMERICA_ARGENTINA_TUCUMAN',
  /** Argentina - Ushuaia */
  AMERICA_ARGENTINA_USHUAIA = 'AMERICA_ARGENTINA_USHUAIA',
  /** Aruba */
  AMERICA_ARUBA = 'AMERICA_ARUBA',
  /** Asuncion */
  AMERICA_ASUNCION = 'AMERICA_ASUNCION',
  /** Atikokan */
  AMERICA_ATIKOKAN = 'AMERICA_ATIKOKAN',
  /** Bahia */
  AMERICA_BAHIA = 'AMERICA_BAHIA',
  /** Bahia Banderas */
  AMERICA_BAHIA_BANDERAS = 'AMERICA_BAHIA_BANDERAS',
  /** Barbados */
  AMERICA_BARBADOS = 'AMERICA_BARBADOS',
  /** Belem */
  AMERICA_BELEM = 'AMERICA_BELEM',
  /** Belize */
  AMERICA_BELIZE = 'AMERICA_BELIZE',
  /** Blanc-Sablon */
  AMERICA_BLANC_SABLON = 'AMERICA_BLANC_SABLON',
  /** Boa Vista */
  AMERICA_BOA_VISTA = 'AMERICA_BOA_VISTA',
  /** Bogota */
  AMERICA_BOGOTA = 'AMERICA_BOGOTA',
  /** Boise */
  AMERICA_BOISE = 'AMERICA_BOISE',
  /** Cambridge Bay */
  AMERICA_CAMBRIDGE_BAY = 'AMERICA_CAMBRIDGE_BAY',
  /** Campo Grande */
  AMERICA_CAMPO_GRANDE = 'AMERICA_CAMPO_GRANDE',
  /** Cancun */
  AMERICA_CANCUN = 'AMERICA_CANCUN',
  /** Caracas */
  AMERICA_CARACAS = 'AMERICA_CARACAS',
  /** Cayenne */
  AMERICA_CAYENNE = 'AMERICA_CAYENNE',
  /** Cayman */
  AMERICA_CAYMAN = 'AMERICA_CAYMAN',
  /** Chicago */
  AMERICA_CHICAGO = 'AMERICA_CHICAGO',
  /** Chihuahua */
  AMERICA_CHIHUAHUA = 'AMERICA_CHIHUAHUA',
  /** Costa Rica */
  AMERICA_COSTA_RICA = 'AMERICA_COSTA_RICA',
  /** Creston */
  AMERICA_CRESTON = 'AMERICA_CRESTON',
  /** Cuiaba */
  AMERICA_CUIABA = 'AMERICA_CUIABA',
  /** Curacao */
  AMERICA_CURACAO = 'AMERICA_CURACAO',
  /** Danmarkshavn */
  AMERICA_DANMARKSHAVN = 'AMERICA_DANMARKSHAVN',
  /** Dawson */
  AMERICA_DAWSON = 'AMERICA_DAWSON',
  /** Dawson Creek */
  AMERICA_DAWSON_CREEK = 'AMERICA_DAWSON_CREEK',
  /** Denver */
  AMERICA_DENVER = 'AMERICA_DENVER',
  /** Detroit */
  AMERICA_DETROIT = 'AMERICA_DETROIT',
  /** Dominica */
  AMERICA_DOMINICA = 'AMERICA_DOMINICA',
  /** Edmonton */
  AMERICA_EDMONTON = 'AMERICA_EDMONTON',
  /** Eirunepe */
  AMERICA_EIRUNEPE = 'AMERICA_EIRUNEPE',
  /** El Salvador */
  AMERICA_EL_SALVADOR = 'AMERICA_EL_SALVADOR',
  /** Fortaleza */
  AMERICA_FORTALEZA = 'AMERICA_FORTALEZA',
  /** Fort Nelson */
  AMERICA_FORT_NELSON = 'AMERICA_FORT_NELSON',
  /** Glace Bay */
  AMERICA_GLACE_BAY = 'AMERICA_GLACE_BAY',
  /** Godthab */
  AMERICA_GODTHAB = 'AMERICA_GODTHAB',
  /** Goose Bay */
  AMERICA_GOOSE_BAY = 'AMERICA_GOOSE_BAY',
  /** Grand Turk */
  AMERICA_GRAND_TURK = 'AMERICA_GRAND_TURK',
  /** Grenada */
  AMERICA_GRENADA = 'AMERICA_GRENADA',
  /** Guadeloupe */
  AMERICA_GUADELOUPE = 'AMERICA_GUADELOUPE',
  /** Guatemala */
  AMERICA_GUATEMALA = 'AMERICA_GUATEMALA',
  /** Guayaquil */
  AMERICA_GUAYAQUIL = 'AMERICA_GUAYAQUIL',
  /** Guyana */
  AMERICA_GUYANA = 'AMERICA_GUYANA',
  /** Halifax */
  AMERICA_HALIFAX = 'AMERICA_HALIFAX',
  /** Havana */
  AMERICA_HAVANA = 'AMERICA_HAVANA',
  /** Hermosillo */
  AMERICA_HERMOSILLO = 'AMERICA_HERMOSILLO',
  /** Indiana - Indianapolis */
  AMERICA_INDIANA_INDIANAPOLIS = 'AMERICA_INDIANA_INDIANAPOLIS',
  /** Indiana - Knox */
  AMERICA_INDIANA_KNOX = 'AMERICA_INDIANA_KNOX',
  /** Indiana - Marengo */
  AMERICA_INDIANA_MARENGO = 'AMERICA_INDIANA_MARENGO',
  /** Indiana - Petersburg */
  AMERICA_INDIANA_PETERSBURG = 'AMERICA_INDIANA_PETERSBURG',
  /** Indiana - Tell City */
  AMERICA_INDIANA_TELL_CITY = 'AMERICA_INDIANA_TELL_CITY',
  /** Indiana - Vevay */
  AMERICA_INDIANA_VEVAY = 'AMERICA_INDIANA_VEVAY',
  /** Indiana - Vincennes */
  AMERICA_INDIANA_VINCENNES = 'AMERICA_INDIANA_VINCENNES',
  /** Indiana - Winamac */
  AMERICA_INDIANA_WINAMAC = 'AMERICA_INDIANA_WINAMAC',
  /** Inuvik */
  AMERICA_INUVIK = 'AMERICA_INUVIK',
  /** Iqaluit */
  AMERICA_IQALUIT = 'AMERICA_IQALUIT',
  /** Jamaica */
  AMERICA_JAMAICA = 'AMERICA_JAMAICA',
  /** Juneau */
  AMERICA_JUNEAU = 'AMERICA_JUNEAU',
  /** Kentucky - Louisville */
  AMERICA_KENTUCKY_LOUISVILLE = 'AMERICA_KENTUCKY_LOUISVILLE',
  /** Kentucky - Monticello */
  AMERICA_KENTUCKY_MONTICELLO = 'AMERICA_KENTUCKY_MONTICELLO',
  /** Kralendijk */
  AMERICA_KRALENDIJK = 'AMERICA_KRALENDIJK',
  /** La Paz */
  AMERICA_LA_PAZ = 'AMERICA_LA_PAZ',
  /** Lima */
  AMERICA_LIMA = 'AMERICA_LIMA',
  /** Los Angeles */
  AMERICA_LOS_ANGELES = 'AMERICA_LOS_ANGELES',
  /** Lower Princes */
  AMERICA_LOWER_PRINCES = 'AMERICA_LOWER_PRINCES',
  /** Maceio */
  AMERICA_MACEIO = 'AMERICA_MACEIO',
  /** Managua */
  AMERICA_MANAGUA = 'AMERICA_MANAGUA',
  /** Manaus */
  AMERICA_MANAUS = 'AMERICA_MANAUS',
  /** Marigot */
  AMERICA_MARIGOT = 'AMERICA_MARIGOT',
  /** Martinique */
  AMERICA_MARTINIQUE = 'AMERICA_MARTINIQUE',
  /** Matamoros */
  AMERICA_MATAMOROS = 'AMERICA_MATAMOROS',
  /** Mazatlan */
  AMERICA_MAZATLAN = 'AMERICA_MAZATLAN',
  /** Menominee */
  AMERICA_MENOMINEE = 'AMERICA_MENOMINEE',
  /** Merida */
  AMERICA_MERIDA = 'AMERICA_MERIDA',
  /** Metlakatla */
  AMERICA_METLAKATLA = 'AMERICA_METLAKATLA',
  /** Mexico City */
  AMERICA_MEXICO_CITY = 'AMERICA_MEXICO_CITY',
  /** Miquelon */
  AMERICA_MIQUELON = 'AMERICA_MIQUELON',
  /** Moncton */
  AMERICA_MONCTON = 'AMERICA_MONCTON',
  /** Monterrey */
  AMERICA_MONTERREY = 'AMERICA_MONTERREY',
  /** Montevideo */
  AMERICA_MONTEVIDEO = 'AMERICA_MONTEVIDEO',
  /** Montserrat */
  AMERICA_MONTSERRAT = 'AMERICA_MONTSERRAT',
  /** Nassau */
  AMERICA_NASSAU = 'AMERICA_NASSAU',
  /** New York */
  AMERICA_NEW_YORK = 'AMERICA_NEW_YORK',
  /** Nipigon */
  AMERICA_NIPIGON = 'AMERICA_NIPIGON',
  /** Nome */
  AMERICA_NOME = 'AMERICA_NOME',
  /** Noronha */
  AMERICA_NORONHA = 'AMERICA_NORONHA',
  /** North Dakota - Beulah */
  AMERICA_NORTH_DAKOTA_BEULAH = 'AMERICA_NORTH_DAKOTA_BEULAH',
  /** North Dakota - Center */
  AMERICA_NORTH_DAKOTA_CENTER = 'AMERICA_NORTH_DAKOTA_CENTER',
  /** North Dakota - New Salem */
  AMERICA_NORTH_DAKOTA_NEW_SALEM = 'AMERICA_NORTH_DAKOTA_NEW_SALEM',
  /** Ojinaga */
  AMERICA_OJINAGA = 'AMERICA_OJINAGA',
  /** Panama */
  AMERICA_PANAMA = 'AMERICA_PANAMA',
  /** Pangnirtung */
  AMERICA_PANGNIRTUNG = 'AMERICA_PANGNIRTUNG',
  /** Paramaribo */
  AMERICA_PARAMARIBO = 'AMERICA_PARAMARIBO',
  /** Phoenix */
  AMERICA_PHOENIX = 'AMERICA_PHOENIX',
  /** Porto Velho */
  AMERICA_PORTO_VELHO = 'AMERICA_PORTO_VELHO',
  /** Port-au-Prince */
  AMERICA_PORT_AU_PRINCE = 'AMERICA_PORT_AU_PRINCE',
  /** Port of Spain */
  AMERICA_PORT_OF_SPAIN = 'AMERICA_PORT_OF_SPAIN',
  /** Puerto Rico */
  AMERICA_PUERTO_RICO = 'AMERICA_PUERTO_RICO',
  /** Punta Arenas */
  AMERICA_PUNTA_ARENAS = 'AMERICA_PUNTA_ARENAS',
  /** Rainy River */
  AMERICA_RAINY_RIVER = 'AMERICA_RAINY_RIVER',
  /** Rankin Inlet */
  AMERICA_RANKIN_INLET = 'AMERICA_RANKIN_INLET',
  /** Recife */
  AMERICA_RECIFE = 'AMERICA_RECIFE',
  /** Regina */
  AMERICA_REGINA = 'AMERICA_REGINA',
  /** Resolute */
  AMERICA_RESOLUTE = 'AMERICA_RESOLUTE',
  /** Rio Branco */
  AMERICA_RIO_BRANCO = 'AMERICA_RIO_BRANCO',
  /** Santarem */
  AMERICA_SANTAREM = 'AMERICA_SANTAREM',
  /** Santiago */
  AMERICA_SANTIAGO = 'AMERICA_SANTIAGO',
  /** Santo Domingo */
  AMERICA_SANTO_DOMINGO = 'AMERICA_SANTO_DOMINGO',
  /** Sao Paulo */
  AMERICA_SAO_PAULO = 'AMERICA_SAO_PAULO',
  /** Scoresbysund */
  AMERICA_SCORESBYSUND = 'AMERICA_SCORESBYSUND',
  /** Sitka */
  AMERICA_SITKA = 'AMERICA_SITKA',
  /** St Barthelemy */
  AMERICA_ST_BARTHELEMY = 'AMERICA_ST_BARTHELEMY',
  /** St Johns */
  AMERICA_ST_JOHNS = 'AMERICA_ST_JOHNS',
  /** St Kitts */
  AMERICA_ST_KITTS = 'AMERICA_ST_KITTS',
  /** St Lucia */
  AMERICA_ST_LUCIA = 'AMERICA_ST_LUCIA',
  /** St Thomas */
  AMERICA_ST_THOMAS = 'AMERICA_ST_THOMAS',
  /** St Vincent */
  AMERICA_ST_VINCENT = 'AMERICA_ST_VINCENT',
  /** Swift Current */
  AMERICA_SWIFT_CURRENT = 'AMERICA_SWIFT_CURRENT',
  /** Tegucigalpa */
  AMERICA_TEGUCIGALPA = 'AMERICA_TEGUCIGALPA',
  /** Thule */
  AMERICA_THULE = 'AMERICA_THULE',
  /** Thunder Bay */
  AMERICA_THUNDER_BAY = 'AMERICA_THUNDER_BAY',
  /** Tijuana */
  AMERICA_TIJUANA = 'AMERICA_TIJUANA',
  /** Toronto */
  AMERICA_TORONTO = 'AMERICA_TORONTO',
  /** Tortola */
  AMERICA_TORTOLA = 'AMERICA_TORTOLA',
  /** Vancouver */
  AMERICA_VANCOUVER = 'AMERICA_VANCOUVER',
  /** Whitehorse */
  AMERICA_WHITEHORSE = 'AMERICA_WHITEHORSE',
  /** Winnipeg */
  AMERICA_WINNIPEG = 'AMERICA_WINNIPEG',
  /** Yakutat */
  AMERICA_YAKUTAT = 'AMERICA_YAKUTAT',
  /** Yellowknife */
  AMERICA_YELLOWKNIFE = 'AMERICA_YELLOWKNIFE',
  /** Casey */
  ANTARCTICA_CASEY = 'ANTARCTICA_CASEY',
  /** Davis */
  ANTARCTICA_DAVIS = 'ANTARCTICA_DAVIS',
  /** DumontDUrville */
  ANTARCTICA_DUMONTDURVILLE = 'ANTARCTICA_DUMONTDURVILLE',
  /** Macquarie */
  ANTARCTICA_MACQUARIE = 'ANTARCTICA_MACQUARIE',
  /** Mawson */
  ANTARCTICA_MAWSON = 'ANTARCTICA_MAWSON',
  /** McMurdo */
  ANTARCTICA_MCMURDO = 'ANTARCTICA_MCMURDO',
  /** Palmer */
  ANTARCTICA_PALMER = 'ANTARCTICA_PALMER',
  /** Rothera */
  ANTARCTICA_ROTHERA = 'ANTARCTICA_ROTHERA',
  /** Syowa */
  ANTARCTICA_SYOWA = 'ANTARCTICA_SYOWA',
  /** Troll */
  ANTARCTICA_TROLL = 'ANTARCTICA_TROLL',
  /** Vostok */
  ANTARCTICA_VOSTOK = 'ANTARCTICA_VOSTOK',
  /** Longyearbyen */
  ARCTIC_LONGYEARBYEN = 'ARCTIC_LONGYEARBYEN',
  /** Aden */
  ASIA_ADEN = 'ASIA_ADEN',
  /** Almaty */
  ASIA_ALMATY = 'ASIA_ALMATY',
  /** Amman */
  ASIA_AMMAN = 'ASIA_AMMAN',
  /** Anadyr */
  ASIA_ANADYR = 'ASIA_ANADYR',
  /** Aqtau */
  ASIA_AQTAU = 'ASIA_AQTAU',
  /** Aqtobe */
  ASIA_AQTOBE = 'ASIA_AQTOBE',
  /** Ashgabat */
  ASIA_ASHGABAT = 'ASIA_ASHGABAT',
  /** Atyrau */
  ASIA_ATYRAU = 'ASIA_ATYRAU',
  /** Baghdad */
  ASIA_BAGHDAD = 'ASIA_BAGHDAD',
  /** Bahrain */
  ASIA_BAHRAIN = 'ASIA_BAHRAIN',
  /** Baku */
  ASIA_BAKU = 'ASIA_BAKU',
  /** Bangkok */
  ASIA_BANGKOK = 'ASIA_BANGKOK',
  /** Barnaul */
  ASIA_BARNAUL = 'ASIA_BARNAUL',
  /** Beirut */
  ASIA_BEIRUT = 'ASIA_BEIRUT',
  /** Bishkek */
  ASIA_BISHKEK = 'ASIA_BISHKEK',
  /** Brunei */
  ASIA_BRUNEI = 'ASIA_BRUNEI',
  /** Chita */
  ASIA_CHITA = 'ASIA_CHITA',
  /** Choibalsan */
  ASIA_CHOIBALSAN = 'ASIA_CHOIBALSAN',
  /** Colombo */
  ASIA_COLOMBO = 'ASIA_COLOMBO',
  /** Damascus */
  ASIA_DAMASCUS = 'ASIA_DAMASCUS',
  /** Dhaka */
  ASIA_DHAKA = 'ASIA_DHAKA',
  /** Dili */
  ASIA_DILI = 'ASIA_DILI',
  /** Dubai */
  ASIA_DUBAI = 'ASIA_DUBAI',
  /** Dushanbe */
  ASIA_DUSHANBE = 'ASIA_DUSHANBE',
  /** Famagusta */
  ASIA_FAMAGUSTA = 'ASIA_FAMAGUSTA',
  /** Gaza */
  ASIA_GAZA = 'ASIA_GAZA',
  /** Hebron */
  ASIA_HEBRON = 'ASIA_HEBRON',
  /** Hong Kong */
  ASIA_HONG_KONG = 'ASIA_HONG_KONG',
  /** Hovd */
  ASIA_HOVD = 'ASIA_HOVD',
  /** Ho Chi Minh */
  ASIA_HO_CHI_MINH = 'ASIA_HO_CHI_MINH',
  /** Irkutsk */
  ASIA_IRKUTSK = 'ASIA_IRKUTSK',
  /** Jakarta */
  ASIA_JAKARTA = 'ASIA_JAKARTA',
  /** Jayapura */
  ASIA_JAYAPURA = 'ASIA_JAYAPURA',
  /** Jerusalem */
  ASIA_JERUSALEM = 'ASIA_JERUSALEM',
  /** Kabul */
  ASIA_KABUL = 'ASIA_KABUL',
  /** Kamchatka */
  ASIA_KAMCHATKA = 'ASIA_KAMCHATKA',
  /** Karachi */
  ASIA_KARACHI = 'ASIA_KARACHI',
  /** Kathmandu */
  ASIA_KATHMANDU = 'ASIA_KATHMANDU',
  /** Khandyga */
  ASIA_KHANDYGA = 'ASIA_KHANDYGA',
  /** Kolkata */
  ASIA_KOLKATA = 'ASIA_KOLKATA',
  /** Krasnoyarsk */
  ASIA_KRASNOYARSK = 'ASIA_KRASNOYARSK',
  /** Kuala Lumpur */
  ASIA_KUALA_LUMPUR = 'ASIA_KUALA_LUMPUR',
  /** Kuching */
  ASIA_KUCHING = 'ASIA_KUCHING',
  /** Kuwait */
  ASIA_KUWAIT = 'ASIA_KUWAIT',
  /** Macau */
  ASIA_MACAU = 'ASIA_MACAU',
  /** Magadan */
  ASIA_MAGADAN = 'ASIA_MAGADAN',
  /** Makassar */
  ASIA_MAKASSAR = 'ASIA_MAKASSAR',
  /** Manila */
  ASIA_MANILA = 'ASIA_MANILA',
  /** Muscat */
  ASIA_MUSCAT = 'ASIA_MUSCAT',
  /** Nicosia */
  ASIA_NICOSIA = 'ASIA_NICOSIA',
  /** Novokuznetsk */
  ASIA_NOVOKUZNETSK = 'ASIA_NOVOKUZNETSK',
  /** Novosibirsk */
  ASIA_NOVOSIBIRSK = 'ASIA_NOVOSIBIRSK',
  /** Omsk */
  ASIA_OMSK = 'ASIA_OMSK',
  /** Oral */
  ASIA_ORAL = 'ASIA_ORAL',
  /** Phnom Penh */
  ASIA_PHNOM_PENH = 'ASIA_PHNOM_PENH',
  /** Pontianak */
  ASIA_PONTIANAK = 'ASIA_PONTIANAK',
  /** Pyongyang */
  ASIA_PYONGYANG = 'ASIA_PYONGYANG',
  /** Qatar */
  ASIA_QATAR = 'ASIA_QATAR',
  /** Qostanay */
  ASIA_QOSTANAY = 'ASIA_QOSTANAY',
  /** Qyzylorda */
  ASIA_QYZYLORDA = 'ASIA_QYZYLORDA',
  /** Riyadh */
  ASIA_RIYADH = 'ASIA_RIYADH',
  /** Sakhalin */
  ASIA_SAKHALIN = 'ASIA_SAKHALIN',
  /** Samarkand */
  ASIA_SAMARKAND = 'ASIA_SAMARKAND',
  /** Seoul */
  ASIA_SEOUL = 'ASIA_SEOUL',
  /** Shanghai */
  ASIA_SHANGHAI = 'ASIA_SHANGHAI',
  /** Singapore */
  ASIA_SINGAPORE = 'ASIA_SINGAPORE',
  /** Srednekolymsk */
  ASIA_SREDNEKOLYMSK = 'ASIA_SREDNEKOLYMSK',
  /** Taipei */
  ASIA_TAIPEI = 'ASIA_TAIPEI',
  /** Tashkent */
  ASIA_TASHKENT = 'ASIA_TASHKENT',
  /** Tbilisi */
  ASIA_TBILISI = 'ASIA_TBILISI',
  /** Tehran */
  ASIA_TEHRAN = 'ASIA_TEHRAN',
  /** Thimphu */
  ASIA_THIMPHU = 'ASIA_THIMPHU',
  /** Tokyo */
  ASIA_TOKYO = 'ASIA_TOKYO',
  /** Tomsk */
  ASIA_TOMSK = 'ASIA_TOMSK',
  /** Ulaanbaatar */
  ASIA_ULAANBAATAR = 'ASIA_ULAANBAATAR',
  /** Urumqi */
  ASIA_URUMQI = 'ASIA_URUMQI',
  /** Ust-Nera */
  ASIA_UST_NERA = 'ASIA_UST_NERA',
  /** Vientiane */
  ASIA_VIENTIANE = 'ASIA_VIENTIANE',
  /** Vladivostok */
  ASIA_VLADIVOSTOK = 'ASIA_VLADIVOSTOK',
  /** Yakutsk */
  ASIA_YAKUTSK = 'ASIA_YAKUTSK',
  /** Yangon */
  ASIA_YANGON = 'ASIA_YANGON',
  /** Yekaterinburg */
  ASIA_YEKATERINBURG = 'ASIA_YEKATERINBURG',
  /** Yerevan */
  ASIA_YEREVAN = 'ASIA_YEREVAN',
  /** Azores */
  ATLANTIC_AZORES = 'ATLANTIC_AZORES',
  /** Bermuda */
  ATLANTIC_BERMUDA = 'ATLANTIC_BERMUDA',
  /** Canary */
  ATLANTIC_CANARY = 'ATLANTIC_CANARY',
  /** Cape Verde */
  ATLANTIC_CAPE_VERDE = 'ATLANTIC_CAPE_VERDE',
  /** Faroe */
  ATLANTIC_FAROE = 'ATLANTIC_FAROE',
  /** Madeira */
  ATLANTIC_MADEIRA = 'ATLANTIC_MADEIRA',
  /** Reykjavik */
  ATLANTIC_REYKJAVIK = 'ATLANTIC_REYKJAVIK',
  /** South Georgia */
  ATLANTIC_SOUTH_GEORGIA = 'ATLANTIC_SOUTH_GEORGIA',
  /** Stanley */
  ATLANTIC_STANLEY = 'ATLANTIC_STANLEY',
  /** St Helena */
  ATLANTIC_ST_HELENA = 'ATLANTIC_ST_HELENA',
  /** Adelaide */
  AUSTRALIA_ADELAIDE = 'AUSTRALIA_ADELAIDE',
  /** Brisbane */
  AUSTRALIA_BRISBANE = 'AUSTRALIA_BRISBANE',
  /** Broken Hill */
  AUSTRALIA_BROKEN_HILL = 'AUSTRALIA_BROKEN_HILL',
  /** Currie */
  AUSTRALIA_CURRIE = 'AUSTRALIA_CURRIE',
  /** Darwin */
  AUSTRALIA_DARWIN = 'AUSTRALIA_DARWIN',
  /** Eucla */
  AUSTRALIA_EUCLA = 'AUSTRALIA_EUCLA',
  /** Hobart */
  AUSTRALIA_HOBART = 'AUSTRALIA_HOBART',
  /** Lindeman */
  AUSTRALIA_LINDEMAN = 'AUSTRALIA_LINDEMAN',
  /** Lord Howe */
  AUSTRALIA_LORD_HOWE = 'AUSTRALIA_LORD_HOWE',
  /** Melbourne */
  AUSTRALIA_MELBOURNE = 'AUSTRALIA_MELBOURNE',
  /** Perth */
  AUSTRALIA_PERTH = 'AUSTRALIA_PERTH',
  /** Sydney */
  AUSTRALIA_SYDNEY = 'AUSTRALIA_SYDNEY',
  /** Amsterdam */
  EUROPE_AMSTERDAM = 'EUROPE_AMSTERDAM',
  /** Andorra */
  EUROPE_ANDORRA = 'EUROPE_ANDORRA',
  /** Astrakhan */
  EUROPE_ASTRAKHAN = 'EUROPE_ASTRAKHAN',
  /** Athens */
  EUROPE_ATHENS = 'EUROPE_ATHENS',
  /** Belgrade */
  EUROPE_BELGRADE = 'EUROPE_BELGRADE',
  /** Berlin */
  EUROPE_BERLIN = 'EUROPE_BERLIN',
  /** Bratislava */
  EUROPE_BRATISLAVA = 'EUROPE_BRATISLAVA',
  /** Brussels */
  EUROPE_BRUSSELS = 'EUROPE_BRUSSELS',
  /** Bucharest */
  EUROPE_BUCHAREST = 'EUROPE_BUCHAREST',
  /** Budapest */
  EUROPE_BUDAPEST = 'EUROPE_BUDAPEST',
  /** Busingen */
  EUROPE_BUSINGEN = 'EUROPE_BUSINGEN',
  /** Chisinau */
  EUROPE_CHISINAU = 'EUROPE_CHISINAU',
  /** Copenhagen */
  EUROPE_COPENHAGEN = 'EUROPE_COPENHAGEN',
  /** Dublin */
  EUROPE_DUBLIN = 'EUROPE_DUBLIN',
  /** Gibraltar */
  EUROPE_GIBRALTAR = 'EUROPE_GIBRALTAR',
  /** Guernsey */
  EUROPE_GUERNSEY = 'EUROPE_GUERNSEY',
  /** Helsinki */
  EUROPE_HELSINKI = 'EUROPE_HELSINKI',
  /** Isle of Man */
  EUROPE_ISLE_OF_MAN = 'EUROPE_ISLE_OF_MAN',
  /** Istanbul */
  EUROPE_ISTANBUL = 'EUROPE_ISTANBUL',
  /** Jersey */
  EUROPE_JERSEY = 'EUROPE_JERSEY',
  /** Kaliningrad */
  EUROPE_KALININGRAD = 'EUROPE_KALININGRAD',
  /** Kiev */
  EUROPE_KIEV = 'EUROPE_KIEV',
  /** Kirov */
  EUROPE_KIROV = 'EUROPE_KIROV',
  /** Lisbon */
  EUROPE_LISBON = 'EUROPE_LISBON',
  /** Ljubljana */
  EUROPE_LJUBLJANA = 'EUROPE_LJUBLJANA',
  /** London */
  EUROPE_LONDON = 'EUROPE_LONDON',
  /** Luxembourg */
  EUROPE_LUXEMBOURG = 'EUROPE_LUXEMBOURG',
  /** Madrid */
  EUROPE_MADRID = 'EUROPE_MADRID',
  /** Malta */
  EUROPE_MALTA = 'EUROPE_MALTA',
  /** Mariehamn */
  EUROPE_MARIEHAMN = 'EUROPE_MARIEHAMN',
  /** Minsk */
  EUROPE_MINSK = 'EUROPE_MINSK',
  /** Monaco */
  EUROPE_MONACO = 'EUROPE_MONACO',
  /** Moscow */
  EUROPE_MOSCOW = 'EUROPE_MOSCOW',
  /** Oslo */
  EUROPE_OSLO = 'EUROPE_OSLO',
  /** Paris */
  EUROPE_PARIS = 'EUROPE_PARIS',
  /** Podgorica */
  EUROPE_PODGORICA = 'EUROPE_PODGORICA',
  /** Prague */
  EUROPE_PRAGUE = 'EUROPE_PRAGUE',
  /** Riga */
  EUROPE_RIGA = 'EUROPE_RIGA',
  /** Rome */
  EUROPE_ROME = 'EUROPE_ROME',
  /** Samara */
  EUROPE_SAMARA = 'EUROPE_SAMARA',
  /** San Marino */
  EUROPE_SAN_MARINO = 'EUROPE_SAN_MARINO',
  /** Sarajevo */
  EUROPE_SARAJEVO = 'EUROPE_SARAJEVO',
  /** Saratov */
  EUROPE_SARATOV = 'EUROPE_SARATOV',
  /** Simferopol */
  EUROPE_SIMFEROPOL = 'EUROPE_SIMFEROPOL',
  /** Skopje */
  EUROPE_SKOPJE = 'EUROPE_SKOPJE',
  /** Sofia */
  EUROPE_SOFIA = 'EUROPE_SOFIA',
  /** Stockholm */
  EUROPE_STOCKHOLM = 'EUROPE_STOCKHOLM',
  /** Tallinn */
  EUROPE_TALLINN = 'EUROPE_TALLINN',
  /** Tirane */
  EUROPE_TIRANE = 'EUROPE_TIRANE',
  /** Ulyanovsk */
  EUROPE_ULYANOVSK = 'EUROPE_ULYANOVSK',
  /** Uzhgorod */
  EUROPE_UZHGOROD = 'EUROPE_UZHGOROD',
  /** Vaduz */
  EUROPE_VADUZ = 'EUROPE_VADUZ',
  /** Vatican */
  EUROPE_VATICAN = 'EUROPE_VATICAN',
  /** Vienna */
  EUROPE_VIENNA = 'EUROPE_VIENNA',
  /** Vilnius */
  EUROPE_VILNIUS = 'EUROPE_VILNIUS',
  /** Volgograd */
  EUROPE_VOLGOGRAD = 'EUROPE_VOLGOGRAD',
  /** Warsaw */
  EUROPE_WARSAW = 'EUROPE_WARSAW',
  /** Zagreb */
  EUROPE_ZAGREB = 'EUROPE_ZAGREB',
  /** Zaporozhye */
  EUROPE_ZAPOROZHYE = 'EUROPE_ZAPOROZHYE',
  /** Zurich */
  EUROPE_ZURICH = 'EUROPE_ZURICH',
  /** Antananarivo */
  INDIAN_ANTANANARIVO = 'INDIAN_ANTANANARIVO',
  /** Chagos */
  INDIAN_CHAGOS = 'INDIAN_CHAGOS',
  /** Christmas */
  INDIAN_CHRISTMAS = 'INDIAN_CHRISTMAS',
  /** Cocos */
  INDIAN_COCOS = 'INDIAN_COCOS',
  /** Comoro */
  INDIAN_COMORO = 'INDIAN_COMORO',
  /** Kerguelen */
  INDIAN_KERGUELEN = 'INDIAN_KERGUELEN',
  /** Mahe */
  INDIAN_MAHE = 'INDIAN_MAHE',
  /** Maldives */
  INDIAN_MALDIVES = 'INDIAN_MALDIVES',
  /** Mauritius */
  INDIAN_MAURITIUS = 'INDIAN_MAURITIUS',
  /** Mayotte */
  INDIAN_MAYOTTE = 'INDIAN_MAYOTTE',
  /** Reunion */
  INDIAN_REUNION = 'INDIAN_REUNION',
  /** Apia */
  PACIFIC_APIA = 'PACIFIC_APIA',
  /** Auckland */
  PACIFIC_AUCKLAND = 'PACIFIC_AUCKLAND',
  /** Bougainville */
  PACIFIC_BOUGAINVILLE = 'PACIFIC_BOUGAINVILLE',
  /** Chatham */
  PACIFIC_CHATHAM = 'PACIFIC_CHATHAM',
  /** Chuuk */
  PACIFIC_CHUUK = 'PACIFIC_CHUUK',
  /** Easter */
  PACIFIC_EASTER = 'PACIFIC_EASTER',
  /** Efate */
  PACIFIC_EFATE = 'PACIFIC_EFATE',
  /** Enderbury */
  PACIFIC_ENDERBURY = 'PACIFIC_ENDERBURY',
  /** Fakaofo */
  PACIFIC_FAKAOFO = 'PACIFIC_FAKAOFO',
  /** Fiji */
  PACIFIC_FIJI = 'PACIFIC_FIJI',
  /** Funafuti */
  PACIFIC_FUNAFUTI = 'PACIFIC_FUNAFUTI',
  /** Galapagos */
  PACIFIC_GALAPAGOS = 'PACIFIC_GALAPAGOS',
  /** Gambier */
  PACIFIC_GAMBIER = 'PACIFIC_GAMBIER',
  /** Guadalcanal */
  PACIFIC_GUADALCANAL = 'PACIFIC_GUADALCANAL',
  /** Guam */
  PACIFIC_GUAM = 'PACIFIC_GUAM',
  /** Honolulu */
  PACIFIC_HONOLULU = 'PACIFIC_HONOLULU',
  /** Kiritimati */
  PACIFIC_KIRITIMATI = 'PACIFIC_KIRITIMATI',
  /** Kosrae */
  PACIFIC_KOSRAE = 'PACIFIC_KOSRAE',
  /** Kwajalein */
  PACIFIC_KWAJALEIN = 'PACIFIC_KWAJALEIN',
  /** Majuro */
  PACIFIC_MAJURO = 'PACIFIC_MAJURO',
  /** Marquesas */
  PACIFIC_MARQUESAS = 'PACIFIC_MARQUESAS',
  /** Midway */
  PACIFIC_MIDWAY = 'PACIFIC_MIDWAY',
  /** Nauru */
  PACIFIC_NAURU = 'PACIFIC_NAURU',
  /** Niue */
  PACIFIC_NIUE = 'PACIFIC_NIUE',
  /** Norfolk */
  PACIFIC_NORFOLK = 'PACIFIC_NORFOLK',
  /** Noumea */
  PACIFIC_NOUMEA = 'PACIFIC_NOUMEA',
  /** Pago Pago */
  PACIFIC_PAGO_PAGO = 'PACIFIC_PAGO_PAGO',
  /** Palau */
  PACIFIC_PALAU = 'PACIFIC_PALAU',
  /** Pitcairn */
  PACIFIC_PITCAIRN = 'PACIFIC_PITCAIRN',
  /** Pohnpei */
  PACIFIC_POHNPEI = 'PACIFIC_POHNPEI',
  /** Port Moresby */
  PACIFIC_PORT_MORESBY = 'PACIFIC_PORT_MORESBY',
  /** Rarotonga */
  PACIFIC_RAROTONGA = 'PACIFIC_RAROTONGA',
  /** Saipan */
  PACIFIC_SAIPAN = 'PACIFIC_SAIPAN',
  /** Tahiti */
  PACIFIC_TAHITI = 'PACIFIC_TAHITI',
  /** Tarawa */
  PACIFIC_TARAWA = 'PACIFIC_TARAWA',
  /** Tongatapu */
  PACIFIC_TONGATAPU = 'PACIFIC_TONGATAPU',
  /** Wake */
  PACIFIC_WAKE = 'PACIFIC_WAKE',
  /** Wallis */
  PACIFIC_WALLIS = 'PACIFIC_WALLIS',
  /** UTC offset: UTC+0 */
  UTC_0 = 'UTC_0',
  /** UTC offset: UTC+0:30 */
  UTC_0_30 = 'UTC_0_30',
  /** UTC offset: UTC+1 */
  UTC_1 = 'UTC_1',
  /** UTC offset: UTC+10 */
  UTC_10 = 'UTC_10',
  /** UTC offset: UTC+10:30 */
  UTC_10_30 = 'UTC_10_30',
  /** UTC offset: UTC+11 */
  UTC_11 = 'UTC_11',
  /** UTC offset: UTC+11:30 */
  UTC_11_30 = 'UTC_11_30',
  /** UTC offset: UTC+12 */
  UTC_12 = 'UTC_12',
  /** UTC offset: UTC+12:45 */
  UTC_12_45 = 'UTC_12_45',
  /** UTC offset: UTC+13 */
  UTC_13 = 'UTC_13',
  /** UTC offset: UTC+13:45 */
  UTC_13_45 = 'UTC_13_45',
  /** UTC offset: UTC+14 */
  UTC_14 = 'UTC_14',
  /** UTC offset: UTC+1:30 */
  UTC_1_30 = 'UTC_1_30',
  /** UTC offset: UTC+2 */
  UTC_2 = 'UTC_2',
  /** UTC offset: UTC+2:30 */
  UTC_2_30 = 'UTC_2_30',
  /** UTC offset: UTC+3 */
  UTC_3 = 'UTC_3',
  /** UTC offset: UTC+3:30 */
  UTC_3_30 = 'UTC_3_30',
  /** UTC offset: UTC+4 */
  UTC_4 = 'UTC_4',
  /** UTC offset: UTC+4:30 */
  UTC_4_30 = 'UTC_4_30',
  /** UTC offset: UTC+5 */
  UTC_5 = 'UTC_5',
  /** UTC offset: UTC+5:30 */
  UTC_5_30 = 'UTC_5_30',
  /** UTC offset: UTC+5:45 */
  UTC_5_45 = 'UTC_5_45',
  /** UTC offset: UTC+6 */
  UTC_6 = 'UTC_6',
  /** UTC offset: UTC+6:30 */
  UTC_6_30 = 'UTC_6_30',
  /** UTC offset: UTC+7 */
  UTC_7 = 'UTC_7',
  /** UTC offset: UTC+7:30 */
  UTC_7_30 = 'UTC_7_30',
  /** UTC offset: UTC+8 */
  UTC_8 = 'UTC_8',
  /** UTC offset: UTC+8:30 */
  UTC_8_30 = 'UTC_8_30',
  /** UTC offset: UTC+8:45 */
  UTC_8_45 = 'UTC_8_45',
  /** UTC offset: UTC+9 */
  UTC_9 = 'UTC_9',
  /** UTC offset: UTC+9:30 */
  UTC_9_30 = 'UTC_9_30'
}

/** Options for filtering the connection */
export type GQLMenuItemsWhereArgs = {
  /** The ID of the object */
  id?: Maybe<Scalars['Int']>;
  /** The menu location for the menu being queried */
  location?: Maybe<GQLMenuLocationEnum>;
};

export type GQLPostObjectUnion = GQLPost | GQLPage | GQLMediaItem | GQLSocialMediaIcon;

export type GQLTermObjectUnion = GQLCategory | GQLTag | GQLPostFormat;
