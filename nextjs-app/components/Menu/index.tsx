import {FC} from "react";
import Link from 'next/link';
import { GQLMenu, GQLMenuItem } from "../../gql.generated";
import { MenuItem, MenuLink, MenuList } from "./index.styled";
import convertToHierarchy from "./hooks/convertToHierarchy";

type MenuProps = {
  menu: GQLMenu;
}

const getMenuItems = (menu?: GQLMenu): GQLMenuItem[] => {
  return convertToHierarchy(menu.menuItems.nodes);
}

const Menu: FC<MenuProps> = ({menu}) => {
  if (typeof menu === 'undefined') {
    return <></>;
  }

  const menuItems = getMenuItems(menu);

  const topLevels = menuItems.map((menuItem: GQLMenuItem) => {
    let childItems = null;
    if (menuItem.childItems.nodes.length > 0 ) {
      childItems = menuItem.childItems.nodes.map((menuItem: GQLMenuItem) => {
        return (
          <MenuItem key={menuItem.id}>
            <Link href={menuItem.path} passHref>
              <MenuLink>{menuItem.label}</MenuLink>
            </Link>
          </MenuItem>
        )
      });
      childItems = <MenuList>{childItems}</MenuList>;
    }
    return (
      <MenuItem key={menuItem.id}>
        <Link href={menuItem.path} passHref>
          <MenuLink>{menuItem.label}</MenuLink>
        </Link>
        {childItems}
      </MenuItem>
    );
  });

  return (
    <MenuList>
      {topLevels}
    </MenuList>
  );
};

export default Menu;