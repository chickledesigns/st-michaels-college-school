import {GQLMenuItem} from "../../../gql.generated";

const flatListToHierarchical = (data:GQLMenuItem[] = []) => {
  const tree = [];
  const childrenOf = {};
  data.forEach((item) => {
    const newItem = {...item};
    const { id, parentId = 0 } = newItem;
    childrenOf[id] = childrenOf[id] || [];
    newItem.childItems = {};
    newItem.childItems.nodes = childrenOf[id];
    parentId
      ? (
        childrenOf[parentId] = childrenOf[parentId]|| []
      ).push(newItem)
      : tree.push(newItem);
  });
  return tree;
};

export default flatListToHierarchical;