import gql from 'graphql-tag';

const GET_MENU_BY_NAME = gql`
    query getMenuByName($id:ID!) {
      menu(id:$id,idType:NAME) {
        menuItems {
          nodes {
            ...nodes
            childItems {
              nodes {
                ...nodes
              }
            }
          }
        }
      }
    }
    
    fragment nodes on MenuItem {
      id
      databaseId
      title
      path
      cssClasses
      description
      label
      linkRelationship
      target
      parentId
    }
`;

export default GET_MENU_BY_NAME;