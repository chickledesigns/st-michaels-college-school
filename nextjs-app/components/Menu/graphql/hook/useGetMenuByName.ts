import { useQuery } from "@apollo/client";

import GET_MENU_BY_NAME from "../../queries/GET_MENU_BY_NAME";
import { GQLRootQuery } from "../../../../gql.generated";

const useGetMenuByName = (name: string) => useQuery<GQLRootQuery>(GET_MENU_BY_NAME, {
    variables: {
        id: name
    }
});

export default useGetMenuByName;