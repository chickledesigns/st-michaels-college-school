import styled from "styled-components";
import {ChevronDownIcon} from "../Icons/index.styled";
import breakpoint, {Breakpoint} from "../../lib/breakpoint";

export const MenuLink = styled.a`
  color: #fff;
`;

export const MenuList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`;

export const MenuItem = styled.li`
`;

export const MenuChevron = styled(ChevronDownIcon)`
  @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
    display: none;
  }
`