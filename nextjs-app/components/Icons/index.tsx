import React, {FC} from "react";
import {Burger, BurgerLine} from "./index.styled";

type HamburgerProps = {
  identifier: string;
}

export const Hamburger:FC<HamburgerProps> = ({identifier}) => {
  return (
    <Burger className="mobile-menu">
      <label htmlFor={identifier}>
        <BurgerLine />
        <BurgerLine />
        <BurgerLine />
      </label>
    </Burger>
  );
}