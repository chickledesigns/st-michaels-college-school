import styled from "styled-components";
import breakpoint, {Breakpoint} from "../../lib/breakpoint";
import colours from "../../styles/colours";

type IconProps = {
  color: string;
  fontSize: string;
}

const Icon = styled.i<IconProps>`
  font-family: 'Icomoon';
  font-style: normal;
  
  &::before {
    content: '';
    font-size: ${(p) => p.fontSize};
    color: ${(p) => p.color};
    font-weight: 100;
  }
`;

export const FileIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue908';
  }
`;

export const PhoneIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue906';
  }
`;

export const LocationIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue907';
  }
`;

export const InstagramIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue900';
  }
`;

export const LinkedInIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue901';
  }
`;

export const YoutubeIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue902';
  }
`;

export const TwitterIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue903';
  }
`;

export const FacebookIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue904';
  }
`;

export const EnvelopeIcon = styled(Icon)<IconProps>`
  &::before {
    content: '\ue905';
  }
`;

export const ChevronDownIcon = styled(Icon)<IconProps>`  
  &::before {
    content: '\ue909';
  }
`;

export const Burger = styled.div`
  display: block;
  cursor: pointer;
  width: 18px;
  position: absolute;
  right: 20px;
  top: 50%;
  transform: translateY(-50%);
  
  input[type="checkbox"] {
    position: absolute;
    left: -9999px;
    visibility: hidden;
  }
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
    display: none;
  }
`;

export const BurgerLine = styled.div`
  width: 18px;
  height: 2px;
  background-color: ${colours('white')};
  margin: 2px 0;
  border-radius: 2px;
`;