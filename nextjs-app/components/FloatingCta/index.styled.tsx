import styled from "styled-components";
import colours from "../../styles/colours";
import breakpoint, {Breakpoint} from "../../lib/breakpoint";

export const CtaButton = styled.a`
  display: block;
  margin-bottom: 8px;
  
  &:hover span {
    @media (hover) {
      padding-left: 30px;
      background-color: ${colours('quoteColour')};
    }
  }

  span {
    display: inline-block;
    padding: 8px 20px 8px 15px;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
    font-size: 1.4rem;
    color: ${colours('primary')};
    background-color: ${colours('secondary')};
    transition: all 0.3s ease;
    
    @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
      padding-right: 30px;
    }
  }
  
`;

export const FloatingCtaWrapper = styled.div`
  position: fixed;
  top: 100px;
  right: 0;
  text-align: right;
  transition: top 0.3s ease;
  z-index: 999;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
    top: 130px;
  }
`;