import React, { FC } from "react";
import {CtaButton, FloatingCtaWrapper} from "./index.styled";
import { Link } from "../../utils/types";

type FloatingCtaProps = {
  links: Link[]
}

const FloatingCta: FC<FloatingCtaProps>  = ({links}) => {
  return (
    <FloatingCtaWrapper>
      {
        links.map( (link) =>
          <CtaButton href={link.href} key={link.href + link.label}>
            <span>{link.label}</span>
          </CtaButton>
        )
      }
    </FloatingCtaWrapper>
  )
}

export default FloatingCta;