import styled from "styled-components";
import breakpoint, { Breakpoint } from "../../lib/breakpoint";

export const MainWrapper = styled.div``;

export const InnerWrapper = styled.div`
  max-width: 1750px;
  margin: 0 auto;
`;

export const Image = styled.img`
  display: block;
`;

type FlexProps = {
  justifyContent: string;
  flexWrap: string;
  alignItems: string;
}

export const Flex = styled.div<FlexProps>`
  display: flex;
  ${(p) => p.justifyContent ? 'justify-content:' + p.justifyContent + ';' : ''}
  ${(p) => p.flexWrap ? 'flex-wrap:' + p.flexWrap + ';': ''}
  ${(p) => p.alignItems ? 'align-items:' + p.alignItems : ''}
`