import { useQuery } from "@apollo/client";

import GET_PAGE_BY_URI from "../../queries/GET_PAGE_BY_URI";
import { GQLRootQuery } from "../../../../gql.generated";

const useGetPageByUri = (uri: string) => useQuery<GQLRootQuery>(GET_PAGE_BY_URI, {
  variables: {
    uri: uri
  }
});

export default useGetPageByUri;