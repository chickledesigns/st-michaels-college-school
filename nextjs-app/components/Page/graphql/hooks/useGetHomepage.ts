import { useQuery } from "@apollo/client";

import { GQLRootQuery } from "../../../../gql.generated";
import GET_HOMEPAGE from "../../queries/GET_HOMEPAGE";

const useGetHomepage = () => useQuery<GQLRootQuery>(GET_HOMEPAGE, {
  variables: {
    uri: 'home'
  }
});

export default useGetHomepage;