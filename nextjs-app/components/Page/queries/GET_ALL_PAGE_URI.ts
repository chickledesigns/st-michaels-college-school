import gql from 'graphql-tag';

const GET_ALL_PAGE_URI = gql`
  query getAllPageUris {
    pages(first: 0, last: 100) {
      nodes {
        uri
      }
    }
  }
`;

export default GET_ALL_PAGE_URI;