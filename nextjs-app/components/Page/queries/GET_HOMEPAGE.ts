import gql from "graphql-tag";

const GET_HOMEPAGE = gql`
  query getHomepage($uri: ID!) {
    menus {
      nodes {
        name
         menuItems(first: 100) {
          nodes {
            id
            databaseId
            title
            path
            cssClasses
            description
            label
            linkRelationship
            target
            parentId
          }
        }
      }
    }
    logo: mediaItem(id: "logo", idType: SLUG) {
      title
      altText
      sourceUrl
    }
    logoSmall: mediaItem(id: "logo-small", idType: SLUG) {
      title
      altText
      sourceUrl
    }
    heroImage: mediaItem(id: "be-belong-become", idType: SLUG) {
      title
      altText
      sourceUrl
    }
    socialMediaIcons {
      nodes {
        slug
        socialMediaLink {
          socialMediaLink
        }
      }
    }
    page(id: $uri, idType: URI) {
      id
      databaseId
      title
      uri
      content
      link
      featuredImage {
        node {
          altText
          srcSet
          title
        }
      }
    }
  }
`;

export default GET_HOMEPAGE;