import {FC, useEffect, useState} from 'react';
import {GQLMediaItem, GQLMenu, GQLMenuItem} from '../../gql.generated';
import Menu from '../Menu';
import Link from 'next/link';
import { Logo, StMichaelsHeader, Nav, TopNav } from "./index.styled";
import {Flex, Image, InnerWrapper} from "../Page/index.styled";
import {Hamburger} from "../Icons";

type HeaderProps = {
  primaryMenu?: GQLMenu;
  topNavMenu?: GQLMenu;
  logo: GQLMediaItem;
  logoSmall: GQLMediaItem;
}

const getScrollPosition = () => typeof window === 'undefined' ? 0 : window.scrollY;

const getHeaderClassName = () => getScrollPosition() > 0 ? 'collapsed' : 'docked';

const Header: FC<HeaderProps> = ({ primaryMenu, topNavMenu, logo, logoSmall}) => {
  const [headerClassName, setHeaderClassName] = useState(getHeaderClassName());
  const [logoUrl, setLogoUrl] = useState(getScrollPosition() > 0 ? logoSmall.sourceUrl : logo.sourceUrl);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('scroll', (e) => {
        setHeaderClassName(getHeaderClassName);
        setLogoUrl(getScrollPosition() > 0 ? logoSmall.sourceUrl : logo.sourceUrl);
      });
    }
  }, []);

  return (
    <StMichaelsHeader className={headerClassName}>
      <InnerWrapper>
        <Flex flexWrap="wrap">
          <Logo className="logo">
            <Link href="/" passHref>
              <a>
                <Image alt={logo.altText} src={logoUrl} />
              </a>
            </Link>
          </Logo>
          <Hamburger identifier="site-header"/>
          <div className="nav-wrapper">
            <input data-role="menu" type="checkbox" id="site-header"/>
            <Flex flexWrap="wrap" alignItems="center">
              <TopNav className={headerClassName}>
                {topNavMenu && <Menu menu={topNavMenu} />}
              </TopNav>
              <Nav className={headerClassName}>
                {primaryMenu && <Menu menu={primaryMenu} />}
              </Nav>
            </Flex>
          </div>
        </Flex>
      </InnerWrapper>
    </StMichaelsHeader>
  )
};

export default Header;
