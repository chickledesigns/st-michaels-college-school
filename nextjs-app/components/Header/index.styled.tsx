import styled from 'styled-components';
import colours from "../../styles/colours";
import breakpoint, { Breakpoint } from "../../lib/breakpoint";

export const Logo = styled.div`
  width: 75%;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.sm)}) {
    width: 30%;
  }
  
  a {
    display: block;
    
    img {
      width: 100%;
      max-width: 200px;
      
      @media only screen and (min-width: ${breakpoint(Breakpoint.sm)}) {
        max-width: 365px;
      }
    }
  }
`;

export const StMichaelsHeader = styled.header`
  background-color: #0C233F;
  font-size: 1.2rem;
  padding: 8px 20px; 
  position: fixed;
  top: 0;
  width: 100%;
  max-height: 100vh;
  overflow-y: auto;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
    padding: 8px 35px;
    max-height: 125px;
    overflow: hidden;
  }
  
  @media (hover) {
    &:hover {
      transition: max-height 0.6s ease;
      max-height: 500px !important;
    }
  }
  
  + .main-wrapper {
    padding-top: 72px;
    
    @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
      padding-top: 125px;
    }
  }
  
  &.collapsed {
    @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
      max-height: 86px;
    }
    
    .logo {
      img {
        max-width: 120px;
        
        @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
          max-width: 250px;
        }
      }
    }
    
    + .main-wrapper {
      padding-top: 42px;
      
      @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
        padding-top: 86px;
      }
    }
  }
    
  .nav-wrapper {
    width: 100%;
    
    @media only screen and (min-width: ${breakpoint(Breakpoint.sm)}) {
      width: ${100 - 30}%;
      padding-right: 40px;
    }
    
    > div {
      display: none;
      height: 100%;
      
      @media only screen and (min-width: ${breakpoint(Breakpoint.sm)}) {
        display: flex;
      }
    }
    
    [data-role=menu]:checked + div {
      display: flex;
    }
  }
`;

export const TopNav = styled.div`
  font-family: 'Proxima Nova';
  width: 100%;
  text-align: right;
  padding: 0;
  display: none;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.sm)}) {
    display: block;
  }
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
    padding: 30px 20px;
  }
  
  &.collapsed {
    @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
      padding: 20px 0 15px;
    }
  }
  
  ul {
    li {
      display: inline-block;
      padding-left: 20px;
      text-transform: uppercase;

      a {
        color: ${colours('secondary')};
      }
    }
  }
`

export const Nav = styled.nav`
  font-family: 'Proxima Nova Bold';
  width: 100%;
  font-weight: 600;
  font-size: 1.6rem;
  padding: 20px 10px;
  display: none;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
    font-size: 1.4rem;
    padding: 12px 0 20px;
    display: block;
  }
  
  &.collapsed {
    @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
      padding: 0;
    }
  }

  // Top level
  > ul {
    
    @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
      display: flex;
      flex-wrap: nowrap;
    }
    
    // Common styles
    li {
      text-align: left;
      width: 100%;
      
      @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
        text-align: center;
      }
      
      a {
        display: inline-block;
        padding-bottom: 25px;
        
        @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
          padding-bottom: 15px;
        }
        
        &:hover {
          color: ${colours('lightestBlue')};
        }
      }
    }
    
    // Top level link
    > li {
      position: relative;
      
      &::before {
        display: block;
        position: absolute;
        left: 0;
        width: 1px;
        background-color: ${colours('divider')};
        bottom: 0;
        height: 85%;
        
        @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
          content: '';
        }
      }
      
      &::after {
        content: '';
        border-left: 1px solid #fff;
        border-bottom: 1px solid #fff;
        transform: rotate(-45deg);
        height: 10px;
        width: 10px;
        display: inline-block;
        vertical-align: top;
        margin-left: 10px;
        
        @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
          display: none;
        }
      }
      
      > ul {
        display: none;
        
        @media only screen and (min-width: ${breakpoint(Breakpoint.lg)}) {
          display: block;
        }
        
        // Second level link
        > li {
          font-family: 'Proxima Nova';
          font-weight: 400;
          font-size: 1.1rem;
          padding: 0 20px;
          
          &:last-child a {
            padding-bottom: 5px;
          }
        }
      }
    }
  }
`;