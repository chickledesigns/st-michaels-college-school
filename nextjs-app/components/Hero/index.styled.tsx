import styled, {keyframes} from "styled-components";
import breakpoint, {Breakpoint} from "../../lib/breakpoint";

type HeroWrapperProps = {
  backgroundImage: string;
}

export const HeroWrapper = styled.div<HeroWrapperProps>`
  background-image: url('${(p) => p.backgroundImage}');
  background-size: cover;
  padding: 25vh 0;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
    padding: 20px 0;
  }
`;

const solidify = keyframes`
  0% {
    color: transparent;
  }
  
  10% {
    color: #fff;
  }
  
  20% {
    color: #fff;
  }
  
  30% {
    color: transparent;
  }
   
  100% {
    color: transparent;
  }
`

type HeroWordProps = {
  delay: number;
}

export const HeroWord = styled.div<HeroWordProps>`
  font-family: 'Proxima Nova Bold', sans-serif;
  font-size: 16vw;
  line-height: 1;
  padding: 0 10%;
  -webkit-text-stroke: 1px #fff;
  color: transparent;
  animation: ${solidify} 9s ease infinite ${(p) => p.delay * 3}s;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
    -webkit-text-stroke: 2px #fff;
  }
`;