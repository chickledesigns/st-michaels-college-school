import React, { FC } from "react";
import {GQLMediaItem} from "../../gql.generated";
import {HeroWord, HeroWrapper} from "./index.styled";

type HeroProps = {
  backgroundImage: GQLMediaItem;
  words?: string[]
}

const Hero:FC<HeroProps> = ({backgroundImage, words}) => {
  return (
    <HeroWrapper backgroundImage={backgroundImage.sourceUrl}>
      {words.map((word, index) => {
        return <HeroWord key={word} delay={index}>{word}</HeroWord>
      })}
    </HeroWrapper>
  )
};

export default Hero;