import React, { FC } from 'react';
import {GQLMenu} from "../../gql.generated";
import {Flex, InnerWrapper} from "../Page/index.styled";
import {ConnectWithUsBlock, ContactBlock, JoinUsBlock, QuickLinksBlock, Footer as FooterElement} from "./index.styled";
import Menu from "../Menu";
import {EnvelopeIcon, FileIcon, LocationIcon, PhoneIcon} from "../Icons/index.styled";

type FooterProps = {
  quickLinks?: GQLMenu;
}

const Footer: FC<FooterProps> = ({quickLinks}) => {
  return (
    <FooterElement>
      <InnerWrapper>
        <Flex justifyContent="center" flexWrap="wrap">
          <ContactBlock>
            <h2>Contact Us</h2>
            <Flex alignItems="center">
              <p>
                <LocationIcon />
                1515 Bathurst Street<br />
                Toronto, ON<br />
                Canada M5P 3H4
              </p>
            </Flex>
            <Flex alignItems="center">
              <p>
                <PhoneIcon />
                <a href="tel:4166533180">416-653-3180</a>
              </p>
            </Flex>
            <Flex alignItems="center">
              <p>
                <FileIcon />
                416-653-7704
              </p>
            </Flex>
            <Flex alignItems="center">
              <p>
                <EnvelopeIcon />
                <a href="mailto:info&commat;smcmail&period;com">info@smcmail.com</a>
              </p>
            </Flex>
          </ContactBlock>
          <QuickLinksBlock>
            <h2>Quick Links</h2>
            {quickLinks && <Menu menu={quickLinks} />}
          </QuickLinksBlock>
          <JoinUsBlock>
            <h2>Join Us</h2>
          </JoinUsBlock>
          <ConnectWithUsBlock>
            <h2>Connect With Us</h2>
          </ConnectWithUsBlock>
        </Flex>
      </InnerWrapper>
    </FooterElement>
  );
};

export default Footer;