import styled from "styled-components";
import colours from "../../styles/colours";
import breakpoint, { Breakpoint } from "../../lib/breakpoint";

const FooterItem = styled.div`
  width: 100%;
  padding: 15px;
  border-bottom: 1px solid ${colours('lightBlue')};
  font-size: 1.3rem;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
    width: 22%;
    border-bottom: none;
    font-size: 1.4rem;
  }
  
  i {
    margin-right: 5px;
  }
  
  a {
    display: inline-block;
    padding-bottom: 10px;
    
    &:hover {
      color: ${colours('lightestBlue')};
    }
  }
  
  h2 {
    font-size: 1.4rem;
    
    @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
      font-size: 1.8rem;
    }
  }
`;

export const Footer = styled.footer`
  background-color: ${colours('blue')};
  color: ${colours('white')};
  
  > div {
    padding: 10px;
  }
`;

export const ContactBlock = styled(FooterItem)`
  p {
    margin-top: 0;
    line-height: 2rem;
  }
`;

export const QuickLinksBlock = styled(FooterItem)`
  display: none;
  
  @media only screen and (min-width: ${breakpoint(Breakpoint.md)}) {
    display: block;
  }
`;

export const JoinUsBlock = styled(FooterItem)`
`;

export const ConnectWithUsBlock = styled(FooterItem)`
  border-bottom: none;
`;