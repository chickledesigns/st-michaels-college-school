import React, { FC, ComponentProps } from 'react';
import Header from '../Header';
import Footer from "../Footer";
import ErrorBoundary from "../ErrorBoundary";
import {MainWrapper} from "../Page/index.styled";
import FloatingCta from "../FloatingCta";
import {Link} from "../../utils/types";

type LayoutProps = {
  headerProps: ComponentProps<typeof Header>;
  footerProps: ComponentProps<typeof Footer>;
}

const getLinks = ():Link[] => {
  return [
    {
      href: '/inquire',
      label: 'Inquire',
    },
    {
      href: '/tour',
      label: 'Tour',
    },
    {
      href: '/apply',
      label: 'Apply',
    }
  ];
}

const Layout: FC<LayoutProps> = ({headerProps, children, footerProps}) => (
  <ErrorBoundary>
    <Header {...headerProps} />
    <MainWrapper className="main-wrapper">
      <FloatingCta links={getLinks()} />
      <main>{children}</main>
    </MainWrapper>
    <Footer {...footerProps} />
  </ErrorBoundary>
);

export default Layout;
