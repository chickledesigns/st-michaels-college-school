import {FC} from "react";
import Head from "next/head";
import Layout from "../../components/Layout";
import { PageActionsProps } from "./hooks/extractPageProps";
import {GQLMenu} from "../../gql.generated";

const Page: FC<PageActionsProps> = (props) => {
  const {
    title,
    content,
    menus,
    logo,
    logoSmall
  } = props;

  const headerProps = {
    primaryMenu: menus.find(( menu:GQLMenu ) => { return menu.name === 'primary' }),
    topNavMenu: menus.find((menu:GQLMenu) => { return menu.name === 'top-nav' }),
    logo: logo,
    logoSmall: logoSmall
  };

  const footerProps = {
    quickLinks: menus.find((menu:GQLMenu) => { return menu.name === 'quick-links'}),
  }

  return (
    <Layout headerProps={headerProps} footerProps={footerProps}>
      <Head>
        <title>{title}</title>
      </Head>
      <div className="cms">{content}</div>
    </Layout>
  )
}

export default Page;