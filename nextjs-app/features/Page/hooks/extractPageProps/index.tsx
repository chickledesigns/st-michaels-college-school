import HtmlParser from 'react-html-parser';

import {GQLMediaItem, GQLMenu, GQLPage, Maybe, Scalars} from '../../../../gql.generated';
import { ApolloError } from "@apollo/client";

export type PageActionsProps = {
  error?: ApolloError;
  loading?: boolean;
  title?: string;
  uri?: string;
  content?: string;
  featuredImage?: GQLMediaItem;
  menus?: GQLMenu[];
  logo: GQLMediaItem;
  logoSmall: GQLMediaItem;
}

const getMenus = (data?: Maybe<Scalars['JSON']>): GQLMenu[] => {
  return data?.menus.nodes;
}

const getPage = (data?: Maybe<Scalars['JSON']>): GQLPage => {
  return data?.page;
}

const getLogo = (data?: Maybe<Scalars['JSON']>): GQLMediaItem => {
  return data?.logo;
}

const getLogoSmall = (data?: Maybe<Scalars['JSON']>): GQLMediaItem => {
  return data?.logoSmall;
}

const extractPageProps = (data: Scalars['JSON']):PageActionsProps => {
  const page = getPage(data);
  const menus = getMenus(data);
  const logo = getLogo(data);
  const logoSmall = getLogoSmall(data);

  return {
    title: page?.title,
    uri: page?.uri,
    content: HtmlParser(page?.content),
    featuredImage: page?.featuredImage?.node,
    menus,
    logo,
    logoSmall
  }
}

export default extractPageProps;