import HtmlParser from 'react-html-parser';

import {GQLMediaItem, GQLMenu, GQLPage, GQLRootQuery, Maybe, Scalars} from '../../../../gql.generated';

export type PageActionsProps = {
  title?: string;
  uri?: string;
  content?: string;
  featuredImage?: GQLMediaItem;
  menus?: GQLMenu[];
  logo: GQLMediaItem;
  logoSmall: GQLMediaItem;
  heroImage: GQLMediaItem;
}

const getMenus = (data?: GQLRootQuery): GQLMenu[] => {
  return data?.menus.nodes;
}

const getPage = (data?: Maybe<Scalars['JSON']>): GQLPage => {
  return data?.page;
}

const getLogo = (data?: Maybe<Scalars['JSON']>): GQLMediaItem => {
  return data?.logo;
}

const getLogoSmall = (data?: Maybe<Scalars['JSON']>): GQLMediaItem => {
  return data?.logoSmall;
}

const getHeroImage = (data?: Maybe<Scalars['JSON']>): GQLMediaItem => {
  return data?.heroImage;
}

const extractHomepageProps = (data?: Maybe<Scalars['JSON']>):PageActionsProps => {
  const page = getPage(data);
  const menus = getMenus(data);
  const logo = getLogo(data);
  const logoSmall = getLogoSmall(data);
  const heroImage = getHeroImage(data);

  return {
    title: page?.title,
    uri: page?.uri,
    content: HtmlParser(page?.content),
    featuredImage: page?.featuredImage?.node,
    menus,
    logo,
    logoSmall,
    heroImage
  }
}

export default extractHomepageProps;