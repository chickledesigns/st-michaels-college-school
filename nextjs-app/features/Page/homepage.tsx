import {FC} from "react";
import Head from "next/head";
import Layout from "../../components/Layout";
import { PageActionsProps } from "./hooks/extractHomepageProps";
import {GQLMenu} from "../../gql.generated";
import Hero from "../../components/Hero";

const Home: FC<PageActionsProps> = (props) => {
  const {
    title,
    menus,
    logo,
    logoSmall,
    content,
    heroImage
  } = props;

  const headerProps = {
    primaryMenu: menus.find(( menu:GQLMenu ) => { return menu.name === 'primary' }),
    topNavMenu: menus.find((menu:GQLMenu) => { return menu.name === 'top-nav' }),
    logo: logo,
    logoSmall: logoSmall
  };

  const footerProps = {
    quickLinks: menus.find((menu:GQLMenu) => { return menu.name === 'quick-links'}),
  }

  return (
    <Layout headerProps={headerProps} footerProps={footerProps}>
      <Head>
        <title>{title}</title>
      </Head>
      <div className="cms">
        <Hero backgroundImage={heroImage} words={['Be','Belong','Become']} />
        {content}
      </div>
    </Layout>
  )
}

export default Home;