<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'stmichaels' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_mvGz4AJO#i=)FXd~[Q:M]LMYSQ-bFfp><aLv&F0_u2g&7(cIa3@c+{&?H044PX.' );
define( 'SECURE_AUTH_KEY',  'iFEHzW_ob+.yqCV5@Zj637jTF.%?lJRdlog(7&mfd -xn4 Dnkud<Bu5HZ!IEvpG' );
define( 'LOGGED_IN_KEY',    '3k#jV@nLnca6psDgmU@9j|fE}RbkIV}XJ]O!9-kU~[=d5`]y^5+MEz`=jLMTz.O?' );
define( 'NONCE_KEY',        './mG*bKNcy+A5-Z#pF!_!@Eq,J)Ug*wo`UDJNNbfK9 o1n+W=n_}U?OzWkc&e!VO' );
define( 'AUTH_SALT',        '@|@]I?1sfE?=.iO61OP,;*ayEE@&nl_e8n2QsJP2ry2pR^l9<bS/&W,PgCd23f7w' );
define( 'SECURE_AUTH_SALT', 'Rb$IeMxrA}V?dl?r%c|;TL430&,<.nUM}0P 6Tw}>I@K^)DHpqxuYfmh>m]hEmpv' );
define( 'LOGGED_IN_SALT',   '?lmVT^CZ[+qWiVHL<J~[=x]%OCi/I#14o;2-0YM<4i=~>-Vuyi9{_8|Q_/I,0e7!' );
define( 'NONCE_SALT',       '?*dgIVG2&>}fbzN/j.ZwxKe?))/7AW+sroRu^B}8Ht0}@N|4Vf<i]-+BawAJdgs]' );
define( 'GRAPHQL_JWT_AUTH_SECRET_KEY', '!UTk6pZizvSg12}WlR,BF+R0W&_V&^EQ+G1x+mJ92,rqlEW9oTF_rLgdkR{aR}zP' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
